## Features

- real-time collaborative learning
- connect with your peers
- broadcast current goals/projects
- collaborative contributions
- slack like chat client
- stackoverflow like q&a forum
- trello like management tools

## What is Mesh?

Mesh is a place to connect with people who want to learn new skills or sharpen existing ones.

## Requirements

- Redis
- Postgres
- Node >= 7.0.0

## Code style

*We're sticking to this!*

[![JavaScript Style Guide](https://cdn.rawgit.com/feross/standard/master/badge.svg)](https://github.com/feross/standard)

`npm run lint` to check code against standard

## Installation

*install redis*
- `brew install redis && brew services start redis`

*install postgres*
- `brew install postgresql && brew services start postgresql`

*create a db and add postgres user*
- `createdb mesh-dev`
- `createuser postgres`

*install packages and start server*
- `yarn install --ignore-engines`
- `npm start`

## Commit messages

We are using emoji to signiy current state of the project within
that commit. I would appreciate if you follow this convention..
- wip = :hammer:
- built = :heavy_check_mark:
- fix = :bug:
- done = :100:
- add = :heavy_plus_sign:
- remove = :scissors:
- change = :nut_and_bolt:
- refactor = :pencil:
- cleanup  = :wrench:
- documentation = :book:

## Workflow

> This workflow encourages collaboration among the group by giving each team member ownership of issues they assign. Thus the team member has a stake in the
completion of the chosen issue.

### Issue Assignment and Weighing 

1. For each feature, create an issue
  - For each issue, assign a weight
2. Each **team member** picks the issues most important to them
3. That **team member**, then chooses other team members to which to **assign** those chosen issues
4. Finally, team members may swap issues at will
