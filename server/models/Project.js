'use strict'

module.exports = (sequelize, DataTypes) => {
  const Project = sequelize.define('Project', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    tags: DataTypes.TEXT
  },
  {
    underscored: true
  })

  Project.associate = (models) => {
    Project.hasMany(models.Board, { as: 'boards' })
    Project.belongsTo(models.User, { as: 'owner' })
    Project.belongsTo(models.Team, { as: 'team' })
    Project.belongsTo(models.Rep, { as: 'rep' })
    Project.belongsToMany(models.User, { as: 'present', through: 'ProjectPresents' })
  }

  return Project
}
