'use strict'
const Sequelize = require('sequelize')

module.exports = (sequelize, DataTypes) => {
  const Question = sequelize.define('Question', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    title: DataTypes.STRING,
    description: DataTypes.TEXT,
    tags: DataTypes.TEXT
  },
  {
    underscored: true
  })

  Question.associate = ({ User, Team, Answer, Rep }) => {
    Question.belongsToMany(Answer, {
      through: 'QuestionAnswers',
      as: 'answers'
    })
    Question.belongsTo(User, { as: 'owner' })
    Question.belongsTo(Team, { as: 'team' })
    Question.belongsTo(Rep, { as: 'rep' })
  }

  return Question
}
