'use strict'

module.exports = (sequelize, DataTypes) => {
  const Card = sequelize.define('Card', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    text: DataTypes.TEXT,
    label: DataTypes.STRING,
    next: DataTypes.INTEGER,
    prev: DataTypes.INTEGER,
    editing: { type: DataTypes.BOOLEAN, defaultValue: true }
  },
  {
    underscored: true
  })

  Card.associate = (models) => {
    Card.belongsTo(models.User, { as: 'owner' })
    Card.belongsTo(models.Board, { as: 'board' })
  }

  Card.prototype.sort = function (next, prev) {
    if (this.next >= next && this.next < prev) {
      this.update({ prev: this.next })
        .then(card => card.increment('next'))
    } else if (this.next <= next && this.next > prev) {
      this.update({ prev: this.next })
        .then(card => card.decrement('next'))
    } else if (this.next === prev) {
      this.update({ prev: prev, next: next })
    }
  }
  Card.prototype.remove = function (prev) {
    if (this.next > prev) {
      this.update({ prev: this.next })
        .then(card => card.decrement('next'))
    }
  }
  Card.prototype.add = function (next) {
    if (this.next >= next) {
      this.update({ prev: this.next })
        .then(card => card.increment('next'))
    }
  }


  return Card
}
