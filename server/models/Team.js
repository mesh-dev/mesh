'use strict'

module.exports = (sequelize, DataTypes) => {
  const Team = sequelize.define('Team', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    name: DataTypes.STRING,
    description: DataTypes.TEXT,
    cover_url: DataTypes.STRING,
    tags: DataTypes.TEXT
  },
  {
    underscored: true
  })

  Team.associate = (models) => {
    Team.belongsToMany(models.User, { as: 'teammates', through: 'Teammates' })
  }

  return Team
}
