'use strict'

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')
const Utils = require('../utils')
const Sequelize = require('sequelize')
const SearchUtils = require('../utils/SearchUtils')

const db = {}
let logger = (log) => {
  console.log(chalk.blue(log))
}
const sequelize = new Sequelize(Utils.CONSTS.DB.URL, Utils.CONSTS.DB.OPTIONS.user, Utils.CONSTS.DB.OPTIONS.password, {
  host: 'localhost',
  dialect: 'postgres',
  pool: {
    max: 5,
    min: 0,
    idle: 10000
  },
  logging: false
})

db['sequelize'] = sequelize

fs.readdirSync(__dirname)
  .filter(file => file.indexOf('.') !== 0 && file !== 'index.js')
  .forEach(file => {
    const model = sequelize.import(path.join(__dirname, file))
    const search = new SearchUtils(db, model)
    db[model.name] = model
    db[model.name].addFullTextIndex = search.addFullTextIndex
    db[model.name].search = search.searchModel
  })

Object.keys(db).forEach(modelName => {
  if ('associate' in db[modelName]) {
    db[modelName].associate(db)
  }
})

module.exports = db
