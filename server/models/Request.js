'use strict'

module.exports = (sequelize, DataTypes) => {
  const Request = sequelize.define('Request', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    approved: { type: DataTypes.BOOLEAN, allowNull: true },
    kind: DataTypes.STRING
  },
  {
    underscored: true
  })

  Request.associate = (models) => {
    Request.belongsTo(models.User, { as: 'requester' })
    Request.belongsTo(models.User, { as: 'requested' })
    Request.belongsTo(models.Team, { as: 'team' })
  }

  return Request
}
