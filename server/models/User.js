'use strict'

const bcrypt = require('bcryptjs')

module.exports = (sequelize, DataTypes) => {
  const User = sequelize.define('User', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    email: DataTypes.STRING,
    handle: {
      type: DataTypes.STRING,
      allowNull: false
    },
    reputation: {
      type: DataTypes.INTEGER,
      defaultValue: 1
    },
    fb_id: DataTypes.STRING,
    gh_id: DataTypes.STRING,
    fb_token: DataTypes.STRING,
    gh_token: DataTypes.STRING,
    password: DataTypes.STRING,
    avatar_url: { type: DataTypes.STRING,
      get: function () {
        let handle = this.getDataValue('handle')
        let avatar_url = this.getDataValue('avatar_url')

        if (!avatar_url) {
          return `https://api.adorable.io/avatars/128/${handle}.png`
        } else {
          return avatar_url
        }
      }
    },
    cover_url: DataTypes.STRING,
    description: DataTypes.TEXT,
    location: DataTypes.STRING,
    birthday: DataTypes.DATE,
    website: DataTypes.STRING,
    active: DataTypes.BOOLEAN,
    role: { type: DataTypes.STRING, defaultValue: 'user' },
    tags: DataTypes.TEXT
  },
  {
    underscored: true,
    defaultScope: {
      attributes: { include: [ 'team_id' ], exclude: [ 'password' ] }
    }
  })

  User.generateHash = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null)
  }
  User.validPassword = (attempt, password) => {
    return bcrypt.compareSync(attempt, password)
  }
  User.associate = (models) => {
    User.belongsTo(models.Team, { as: 'team' })
  }

  User.prototype.sumRep = function (reputation) {
    this.update({ reputation: reputation })
  }

  return User
}
