'use strict'

module.exports = (sequelize, DataTypes) => {
  const Answer = sequelize.define('Answer', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    text: DataTypes.TEXT
  },
  {
    underscored: true
  })

  Answer.associate = (models) => {
    Answer.belongsToMany(models.Comment, {
      through: 'AnswerComments',
      as: 'comments'
    })
    Answer.belongsTo(models.Question, { as: 'question' })
    Answer.belongsTo(models.User, { as: 'owner' })
    Answer.belongsTo(models.Rep, { as: 'rep' })
  }

  return Answer
}
