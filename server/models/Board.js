'use strict'

module.exports = (sequelize, DataTypes) => {
  const Board = sequelize.define('Board', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    title: DataTypes.STRING,
    next: DataTypes.INTEGER,
    prev: DataTypes.INTEGER
  },
  {
    underscored: true,
  })

  Board.associate = (models) => {
    Board.belongsTo(models.User, { as: 'owner' })
    Board.belongsToMany(models.Card, { through: 'CardBoards', as: 'cards' })
  }

  Board.prototype.sort = function (next, prev) {
    if (this.next >= next && this.next < prev) {
      this.update({ prev: this.next })
        .then(board => board.increment('next'))
    } else if (this.next <= next && this.next > prev) {
      this.update({ prev: this.next })
        .then(board => board.decrement('next'))
    } else if (this.next === prev) {
      this.update({ prev: prev, next: next })
    }
  }

  return Board
}
