'use strict'

module.exports = (sequelize, DataTypes) => {
  const Comment = sequelize.define('Comment', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    text: DataTypes.TEXT
  },
  {
    underscored: true
  })

  Comment.associate = (models) => {
    Comment.belongsTo(models.Answer, { as: 'answer' })
    Comment.belongsTo(models.User, { as: 'owner' })
  }

  return Comment
}
