'use strict'

module.exports = (sequelize, DataTypes) => {
  const Mention = sequelize.define('Mention', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    seen: { type: DataTypes.BOOLEAN, defaultValue: false },
    text: DataTypes.TEXT,
    link: DataTypes.STRING
  },
  {
    underscored: true
  })

  Mention.associate = (models) => {
    Mention.belongsTo(models.User, { as: 'sender' })
    Mention.belongsTo(models.User, { as: 'mentioned' })
  }

  return Mention
}
