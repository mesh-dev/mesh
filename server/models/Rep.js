'use strict'

module.exports = (sequelize, DataTypes) => {
  const Rep = sequelize.define('Rep', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    reputation: { type: DataTypes.INTEGER, defaultValue: 6 }
  },
  {
    underscored: true
  })

  Rep.associate = (models) => {
    Rep.belongsTo(models.User, { as: 'owner' })
    Rep.belongsToMany(models.User, { as: 'upvoters', through: 'Upvotes' })
    Rep.belongsToMany(models.User, { as: 'downvoters', through: 'Downvotes' })
  }

  return Rep
}
