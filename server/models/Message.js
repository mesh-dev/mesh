'use strict'

module.exports = (sequelize, DataTypes) => {
  let Message = sequelize.define('Message', {
    id: { type: DataTypes.INTEGER, primaryKey: true, autoIncrement: true },
    kind: DataTypes.STRING,
    text: DataTypes.TEXT,
    seen: { type: DataTypes.BOOLEAN, defaultValue: false },
    edited: { type: DataTypes.BOOLEAN, defaultValue: false },
    attachment: DataTypes.JSON
  },
  {
    underscored: true
  })

  Message.associate = (models) => {
    Message.belongsTo(models.User, { as: 'receipent' })
    Message.belongsTo(models.User, { as: 'sender' })
    Message.belongsTo(models.Project, { as: 'project' })
  }

  return Message
}
