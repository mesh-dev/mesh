'use strict'

const { Project } = require('../models')
const ConversationUtils = require('./ConversationUtils')

class ProjectUtils {
  // Initialize project associations
  static async initProject ({ user, body }) {
    const project = await Project.create(body)
    const owner = await project.setOwner(user)

    if (user.team_id) {
      const team = await project.setTeam(user.team_id)
    }

    //const titleScore = await ConversationUtils
      //.evaluateScore(project.title, 10)
    //const descriptionScore = await ConversationUtils
      //.evaluateScore(project.description, 10)

    const created = await project.createRep({ reputation: 1, owner_id: user.id })
    const rep = await created.getRep()
    const upvoter = await rep.addUpvoter(user)

    return project
  }
  static async updateTeam (user) {
    let projects
    try {
      projects = await Project
        .findAll({
          where: { owner_id: user.id }
        })
    } catch (err) {
      throw new Error(err)
    }

    const team = await user.getTeam()
    projects.forEach(project => project.setTeam(team))

    return projects
  }
  // User in project presnce
  // @TODO account for when user directly disconnects
  // it currently on works when user navigates away manually
  static handlePresence (status, data, user) {
    const projectId = data.split(':')[1] // the project's id occurs after the first colon
    if (status === 'join') {
      Project.findById(projectId).then(project => {
        project.addPresent(user.id)
      })
    } else {
      Project.findById(projectId).then(project => {
        project.removePresent(user.id)
      })
    }
  }

  // Protect group chat from non team members
  static async ensureTeammate ({ user, body }) {
    const project = await Project.findById(body.project_id)
    const owner = await project.getOwner()

    if (owner.team_id !== user.team_id) {
      const err = new Error()
      err.status = 403
      throw err
    } else {
      return owner
    }
  }
}

module.exports = ProjectUtils
