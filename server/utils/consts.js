'use strict'

const path = require('path')

const isProd = process.env.NODE_ENV === 'production'

const options = {
  user: 'postgres',
  password: process.env.NODE_ENV === 'production' ? process.env.DB_PASS : 'postgres'
}

const server = {
  PORT: process.env.PORT || 9000,
  PROTOCOL: process.env.NODE_ENV !== 'production' ? 'http://' : 'https://',
  DOMAIN: process.env.DOMAIN || 'localhost'
}

const facebook = {
  APP_ID: '900740376616481',
  APP_SECRET: '8adfa40d95f98e8a78fa63b2637da79b',
  CALLBACK_URL: (server.PROTOCOL + server.DOMAIN + (process.env.NODE_ENV !== 'production' ? (':' + server.PORT) : '')) + '/auth/facebook/callback'
}
const github = {
  CLIENT_ID: '9b246c618d543728ea67',
  CLIENT_SECRET: '38e9aa3d523826eb00ea639ccf1191d351602637',
  CALLBACK_URL: (server.PROTOCOL + server.DOMAIN + (process.env.NODE_ENV !== 'production' ? (':' + server.PORT) : '')) + '/auth/github/callback'
}

const mailgun = {
  MAILGUN_KEY: 'key-b4c113492579782232e4a90146ec8496',
  MAILGUN_DOMAIN: 'meshtogether.io'
}

module.exports = {
  PUBLIC_PATH: path.join(__dirname, '../../public/'),
  API_URL: '/api',
  FACEBOOK: facebook,
  GITHUB: github,
  MAILGUN: mailgun,
  SLACK_TOKEN: 'xoxp-4415257202-4415257212-177571465815-62490e7df7328dc3ad727a90a753317a',
  RECAPTCHA_SECRET: '6LcVQAcUAAAAAL4EN4nPA9v2om4lsxywM_EfnnyI',
  DB: {
    URL: isProd ? 'mesh-prod' : 'mesh-dev',
    OPTIONS: options
  },
  SERVER: server,
  MESH_SECRECT: 'winston-ftw',
  S3: {
    key: 'AKIAIJAF7WND33T7KFJA',
    secret: '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'
  }
}
