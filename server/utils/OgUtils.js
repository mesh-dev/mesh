'use strict'

const opengraph = require('open-graph-scraper')

class OgUtils {
  static scrapeOgData (url) {
    return new Promise((resolve, reject) => {
      opengraph({ url: url, timeout: 4000 }, (err, og) => {
        if (err) reject(err)
        resolve(og.data)
      })
    })
  }
  static async unfurlLink (message) {
    const urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig

    if (urlRegex.test(message.text)) {
      const url = message.text.match(urlRegex)[0]
      const data = await this.scrapeOgData(url)

      message.attachment = data
      const updated = message.save()

      return updated
    } else {
      return message
    }
  }
}

module.exports = OgUtils
