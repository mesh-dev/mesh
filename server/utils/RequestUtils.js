'use strict'

const TeamUtils = require('./TeamUtils')

class RequestUtils {
  static async joinTeam (request) {
    const requester = await request.getRequester()
    const requested = await request.getRequested()
    const team = await request.getTeam()

    if (requester.team_id) {
      try {
        const leaveTeam = await TeamUtils.leaveTeam(requester, team)
      } catch (err) {
        throw new Error(err)
      }
    } else {
      const set = await requester.setTeam(team)
      const add = await team.addTeammate(requester)
    }

    return request
  }
  static async addTeam (request) {
    const requester = await request.getRequester()
    const requested = await request.getRequested()
    const team = await request.getTeam()

    if (requested.team_id) {
      try {
        const leaveTeam = await TeamUtils.leaveTeam(requested, team)
      } catch (err) {
        console.error(err)
      }
    }

    const set = await requested.setTeam(team)
    const add = await team.addTeammate(requested)

    return request
  }
}

module.exports = RequestUtils
