'use strict'

const sharp = require('sharp')

class ImageUtils {
  constructor () {
  }
  static handleImageCrop (image) {
    let img = sharp(image)

    return new Promise((resolve, reject) => {
      img.metadata()
      .then((metadata) => {
        return img
          .resize(400, 400)
          .crop(sharp.strategy.entropy)
          .toBuffer()
      })
      .then((data) => {
        resolve(data)
      })
    })
  }
  static handleImageOptimize (image) {
    let img = sharp(image)

    return new Promise((resolve, reject) => {
      img.metadata()
      .then((metadata) => {
        return img
          .resize(Math.round(metadata.width / 2))
          .toBuffer()
      })
      .then((data) => {
        resolve(data)
      })
    })
  }
}

module.exports = ImageUtils
