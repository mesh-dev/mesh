'use strict'

const db = require('../models')
const _ = require('lodash')

// @TODO consider change to observable structure
// versus chained promises
class SearchUtils {
  constructor (db, model) {
    this.searchModel = this.searchModel.bind(this)
    this.addFullTextIndex = this.addFullTextIndex.bind(this)

      // Can be undefined when SearchUtils is used as a static class
    this.db = db
    this.model = model
    this.sequelize = db.sequelize
  }
  static searchModels (db, term) {
    const unsearchableModels = [
      'sequelize',
      'Comment',
      'CardBoard',
      'Downvote',
      'QuestionAnswer',
      'Rep',
      'Request',
      'Session',
      'Upvote'
    ]

    return new Promise((resolve, reject) => {
      const actions = Object.keys(db)
        .filter(key => unsearchableModels.indexOf(key) === -1)
        .map(key => db[key])
        .map(model => model.search(term))

      const results = Promise.all(actions)
      results.then(results => {
        // @TODO try to steer away from these hardcoded indices
        // without too many loops
        const res = {
          building: results[5],
          questions: results[6],
          teams: results[7],
          users: results[8]
        }

        resolve(res)
      }).catch(err => reject(err))
    })
  }
  addFullTextIndex () {
    const Model = this.model
    const searchFields = Object.keys(Model.rawAttributes).filter(attr => {
      return attr.match(/title|label|description|name|email|handle|location|text|tags/)
    })
    const vectorName = 'PostText'

      // Raw SQL statements
    const alter = `ALTER TABLE "${Model.tableName}" ADD COLUMN "${vectorName}" TSVECTOR`
    const update = `UPDATE "${Model.tableName}" SET "${vectorName}" = to_tsvector('english', '${searchFields.join('\' || \'')}')`
    const index = `CREATE INDEX post_search_idx_${Model.tableName} ON "${Model.tableName}" USING gin("${vectorName}")`
    const trigger = `CREATE TRIGGER post_vector_update BEFORE INSERT OR UPDATE ON "${Model.tableName}" FOR EACH ROW EXECUTE PROCEDURE tsvector_update_trigger("${vectorName}", 'pg_catalog.english', ${searchFields.join(', ')})`

    this.sequelize.transaction(t => {
      return this.sequelize.query(alter, {transaction: t})
          .then(() => this.sequelize.query(update, { transaction: t }))
          .then(() => this.sequelize.query(index, { transaction: t }))
          .then(() => this.sequelize.query(trigger, { transaction: t }))
    }).catch(err => err)
  }
  searchModel (query) {
    const Model = this.model
    const escapedQuery = this.sequelize.getQueryInterface()
        .escape(`(${query.replace(' ', '\\ ')}:*)`)

    return this.sequelize
        .query(`SELECT * FROM "${Model.tableName}" WHERE "PostText" @@ to_tsquery('english', ${escapedQuery})`, {
          type: this.sequelize.Sequelize.QueryTypes.SELECT,
          include: [{ all: true, nested: true }]
        })
  }
}

module.exports = SearchUtils
