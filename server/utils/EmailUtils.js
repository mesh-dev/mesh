'use strict'

const { CONSTS } = require('./')
const mailgun = require('mailgun-js')({
  apiKey: CONSTS.MAILGUN.MAILGUN_KEY,
  domain: CONSTS.MAILGUN.MAILGUN_DOMAIN
})

class EmailUtils {
  static buildMessage (data, user) {
    const message = {
      from: 'Mesh <no-reply@meshtogether.io>',
      to: user.email,
      subject: 'New Activity on Mesh',
      html: `
        <html>
          <img src="http://i.imgur.com/kdvZW26.jpg" style="width: 50%;"/>
          <h2>Hello there ${user.handle}!</h2>
          <h3>${data.sender.handle} said,</h3>
          <p>${data.text}</p>
          <a href="https://meshtogether.io">
            <p style="display: inline-block; background: #39393A; color: #FFFCF9; padding: 16px; border-radius: 4px;">
              View On Mesh
            </p>
          </a>
        </html>
      `
    }

    return message
  }
  static notify (data, user) {
    const message = this.buildMessage(data, user)
    return new Promise((resolve, reject) => {
      mailgun.messages().send(message, (err, body) => {
        if (err) reject(err)
        resolve(body)
      })

    })
  } 
}

module.exports = EmailUtils
