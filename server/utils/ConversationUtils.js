'use strict'

const adapt = require('ugly-adapter')
const googleapis = require('googleapis')

const API_KEY = 'AIzaSyA1qZQ3grHVJW9Q60NHoLr5w6QFFLancrg'
const COMMENT_ANALYZER_DISCOVERY_URL = 'https://commentanalyzer.googleapis.com/$discovery/rest?version=v1alpha1'

class ConversationUtils {
  static async evaluateScore (comment, scale) {
    const request = {
      comment: { text: comment },
      requestedAttributes: {
        "TOXICITY": {},
        "UNSUBSTANTIAL": {},
        "OBSCENE": {},
        "INFLAMMATORY": {},
        "LIKELY_TO_REJECT": {}
      },
      languages: [ 'en' ]
    }
    const response = await this.analyzeComment(request)
    const scores = response.attributeScores

    const values = []
    for(let score in scores) {
      const value = (1 - scores[score].summaryScore.value) * scale
      values.push(value)
    }
    
    // A summation of those variables results in a MESH score, rewarding good behavior, but not punishing bad behavior.
    const rep = values.reduce((a, b) => a + b, 0)
    return Math.round(rep)
  }
  static async analyzeComment (request) {
    const client = await this.createClient()

    let response
    try {
      response = await adapt(client.comments.analyze, {
        key: API_KEY,
        resource: request
      })
    } catch (err) {
      throw new Error(err)
    }

    return response
  }
  static createClient () {
    return new Promise((resolve, reject) => {
      googleapis.discoverAPI(COMMENT_ANALYZER_DISCOVERY_URL, (err, client) => {
        if (err) reject(err)
        resolve(client)
      })
    })
  }
}

module.exports = ConversationUtils
