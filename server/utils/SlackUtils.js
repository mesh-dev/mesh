'use strict'

const slack = require('slack-invite')
const CONSTS = require('./consts')

class SlackUtils {
  static invite (email) {
    slack({
      email: email,
      team: 'meshtogether',
      token: CONSTS.SLACK_TOKEN
    }, (res) => {
      return res
    })
  }
}

module.exports = SlackUtils
