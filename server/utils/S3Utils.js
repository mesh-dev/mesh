'use strict'

const AWS = require('aws-sdk')
const Utils = require('./')
const { CONSTS } = Utils

AWS.config = {
  accessKeyId: CONSTS.S3.key,
  secretAccessKey: CONSTS.S3.secret
}

class S3Utils {
  constructor (params, file) {
    this.params = params

    const type = file.match(/^data:(\w+)\/(.+);/, '')[1]
    this.ext = file.match(/^data:(\w+)\/(.+);/, '')[2]
    const re = new RegExp(`^data:${type}/${this.ext};base64,`)

    this.base64Data = file.replace(re, '')
    this.buffer = Buffer.from(this.base64Data, 'base64')

    this.params.Key = params.Key + `.${this.ext}`
    this.params.ContentType = `${type}/${this.ext}`
    this.s3 = new AWS.S3({ params: this.params })
  }
  cropAndOptimize () {
    const { ImageUtils } = Utils

    return new Promise((resolve, reject) => {
      ImageUtils
        .handleImageCrop(this.buffer)
        .then(buffer => {
          resolve(this.handleUpload(buffer))
        })
        .catch(err => reject(err))
    })
  }
  optimize () {
    const { ImageUtils } = Utils

    return new Promise((resolve, reject) => {
      if (this.ext !== 'gif') {
        ImageUtils
          .handleImageOptimize(this.buffer)
          .then(buffer => {
            resolve(this.handleUpload(buffer))
          })
          .catch(err => reject(err))
      } else {
        resolve(this.handleUpload())
      }
    })
  }
  handleUpload (buffer) {
    const opts = { queueSize: 4, partSize: 1024 * 1024 * 5 }

    return new Promise((resolve, reject) => {
      this.s3
        .upload({ Body: buffer || this.buffer }, opts)
        .send((err, data) => {
          if (err) reject(err)
          resolve(data)
        })
    })
  }
}

module.exports = S3Utils
