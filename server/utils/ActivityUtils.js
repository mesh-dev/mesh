'use strict'

const { Project, Question, Rep, User } = require('../models')

const includes = [
  {
    model: Rep,
    as: 'rep',
    include: [{
      model: User,
      as: 'upvoters'
    },
    {
      model: User,
      as: 'downvoters'
    }]
  }
]

class ActivityUtils {
  static async gatherActivity (user) {
    const projects = await Project
      .findAll({
        where: { owner_id: user.id },
        include: includes,
        order: [[
          { model: Rep, as: 'rep' },
          'reputation',
          'DESC'
        ]]
      })
    const questions = await Question
      .findAll({
        where: { owner_id: user.id },
        include: includes,
        order: [[
          { model: Rep, as: 'rep' },
          'reputation',
          'DESC'
        ]]
      })

    const activity = { projects: projects, questions: questions }
    user.setDataValue('activity', activity)

    return user
  }
}

module.exports = ActivityUtils
