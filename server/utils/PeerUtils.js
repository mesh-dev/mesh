'use strict'

const _ = require('lodash')

class PeerUtils {
  static addListeners (socket, sockets) {
    socket.on('call:join', (data) => {
      console.log(`${socket.request.user.handle} joined a call`)

      socket.to(`project:${data.project.id}`).broadcast.emit('call:join', {
        peer: socket.request.user.toJSON(),
        initiator: true
      })
    })
    socket.on('call:leave', (data) => {
      console.log(`${socket.request.user.handle} left the call`)

      socket.to(`project:${data.project.id}`).broadcast.emit('call:leave', {
        peer: socket.request.user.toJSON()
      })
    })
    socket.on('call:signal', (call) => {
      console.log(`${socket.request.user.handle} signalled an ${call.type} user ID ${call.to.handle}`)

      sockets.to(`user:${call.to.id}`).emit('call:signal', call)
    })
  }
}

module.exports = PeerUtils
