'use strict'

const _ = require('lodash')

class CardUtils {
  static handleOrderSameBoard (req, board) {
    board.getCards()
      .then(cards => {
        for (let card of cards) {
          card.sort(req.body.next, req.body.prev)
        }
      })
  }
  static handleOrderNewBoard (req, board, card) {
    board.getCards()
      .then(cards => {
        board.removeCard(card)
        for (let card of cards) {
          card.remove(req.body.prev)
        }
      })
    card.setBoard(req.body.board_id)
      .then(card => {
        card.getBoard().then(board => {
          board.addCard(card)
          board.getCards().then(cards => {
            for (let card of cards) {
              card.add(req.body.next)
            }
          })
        })
      })
    card.update(req.body)
  }
}

module.exports = CardUtils
