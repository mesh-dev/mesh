'use strict'

const { User } = require('../models')
const _ = require('lodash')

class UserUtils {
  static async handlePresence ({ id }, active) {
    let user
    try {
      user = await User.findById(id)
    } catch (err) {
      throw new Error(err)
    }
    user.active = active

    const updated = await user.save()

    return updated
  }
  static async saveAvatar ({ id }, url) {
    const user = await User.findById(id)
    user.avatar_url = url

    const updated = await user.save()

    return updated
  }
  static async saveCover ({ id }, url) {
    const user = await User.findById(id)
    user.cover_url = url

    const updated = await user.save()

    return updated
  }
}

module.exports = UserUtils
