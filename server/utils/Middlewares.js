'use strict'

const { CONSTS } = require('./index')
const ProjectUtils = require('./ProjectUtils')
const Recaptcha = require('recaptcha-verify')

const recaptcha = new Recaptcha({
  secret: CONSTS.RECAPTCHA_SECRET
})

class Middlewares {
  static ensureAuthenticated () {
    return (req, res, next) => {
      if (req.isAuthenticated()) {
        return next()
      }
      res.status(401).redirect('/login')
    }
  }
  static handleAuthenticated () {
    return (req, res, next) => {
      if (req.isAuthenticated()) {
        return res.redirect(`/D`)
      }
      next()
    }
  }
  static protectEndpoint () {
    return (req, res, next) => {
      if (req.isAuthenticated() && req.user.role === 'admin') {
        return next()
      }
      res.sendStatus(401)
    }
  }
  static protectProject () {
    return (req, res, next) => {
      if (!req.body.project_id) return next()

      ProjectUtils
        .ensureTeammate(req)
        .then(owner => next())
        .catch(({ status }) => res.sendStatus(status))
    }
  }
  static verifyRecaptcha () {
    return (req, res, next) => {
      recaptcha.checkResponse(req.body.recaptcha_token, (err, response) => {
        if (err) {
          return res.sendStatus(500)
        }

        if (response.success) {
          next()
        } else {
          res.status(400).send('Never gonna give you up!')
        }
      })
    }
  }
}

module.exports = Middlewares
