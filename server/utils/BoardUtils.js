'use strict'

const _ = require('lodash')
const Board = require('../models').Board

class BoardUtils {
  static handleOrder (req, boards) {
    for (let board of boards) {
      board.sort(req.body.next, req.body.prev)
    }
    return boards
  }
}

module.exports = BoardUtils
