'use strict'

const { Team } = require('../models')
const ProjectUtils = require('./ProjectUtils')
const QuestionUtils = require('./QuestionUtils')

class TeamUtils {
  static async leaveTeam (user, team = null) {
    const oldTeam = await user.getTeam()
    const removed = await oldTeam.removeTeammate(user)

    const teammates = await oldTeam.getTeammates()
    if (teammates.length === 0) {
      const destroyed = await oldTeam.destroy()
    }

    if (team) {
      const teammate = await team.addTeammate(user)
    } 

    const set = await user.setTeam(team)
    let json
    if (set.team_id) {
      json = team.toJSON()
      json.teammates = await team.getTeammates()
    }

    const updated = await Promise.all([ ProjectUtils.updateTeam(user), QuestionUtils.updateTeam(user) ])

    return json || {}
  }
  static async saveCover (id, url) {
    const team = await Team.findById(id)
    team.cover_url = url

    const updated = await team.save()

    return updated
  }
}

module.exports = TeamUtils
