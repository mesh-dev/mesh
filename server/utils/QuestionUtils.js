'use strict'

const { Question } = require('../models')
const ConversationUtils = require('./ConversationUtils')

class QuestionUtils {
  // Initialize question associations
  static async initQuestion ({ user, body }) {
    const question = await Question.create(body)

    //const titleScore = await ConversationUtils
      //.evaluateScore(question.title, 10)
    //const descriptionScore = await ConversationUtils
      //.evaluateScore(question.description, 10)

    const created = await question.createRep({ reputation: 1, owner_id: user.id })
    const rep = await created.getRep()
    const upvoter = await rep.addUpvoter(user)

    if (user.team_id) {
      const team = await question.setTeam(user.team_id)
    }

    return question
  }
  static async updateTeam (user) {
    let questions
    try {
      questions = await Question
        .findAll({
          where: { owner_id: user.id }
        })
    } catch (err) {
      throw new Error(err)
    }

    const team = await user.getTeam()
    questions.forEach(question => question.setTeam(team))

    return questions
  }
}

module.exports = QuestionUtils
