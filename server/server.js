'use strict'

const chalk = require('chalk')
const app = require('./app')
const http = require('http')
const sockets = require('./sockets')

let port = process.env.PORT || 9000

app.set('port', port)

let httpServer = http.createServer(app)

httpServer.on('error', (err) => {
  console.log(err)
})

httpServer.on('listening', () => {
  if (process.env.NODE_ENV !== 'production') {
    require('figlet')
      .text('MESH', {
        font: 'speed',
        horizontalLayout: 'universal smushing',
        verticalLayout: 'universal smushing'
      }, (err, result) => console.log(chalk.red(result))
      )
  }
  console.log(chalk.blue(`Express server is listening on ${httpServer.address().port}`))
})

httpServer.listen(port, () => {
  sockets.connectTo(httpServer)
})
