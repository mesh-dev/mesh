'use strict'

const path = require('path')
const router = require('express').Router()
const { CONSTS, Middlewares } = require('./utils')

const { ensureAuthenticated, handleAuthenticated } = Middlewares
const pathToBundle = path.join(CONSTS.PUBLIC_PATH, 'index.html')

// Unauthed routes
const unuathedRoutes = [ '/', '/login' ]
router.get(unuathedRoutes, handleAuthenticated(), (req, res) => {
  res.sendFile(pathToBundle)
})

// Authed routes
const authedRoutes = [ '/D*', '/Q*', '/P*' ]
router.get(authedRoutes, ensureAuthenticated(), (req, res) => {
  res.sendFile(pathToBundle)
})

// Endpoints
require('./endpoints/upload')(router)
require('./endpoints/email')(router)
require('./endpoints/user')(router)
require('./endpoints/team')(router)
require('./endpoints/request')(router)
require('./endpoints/message')(router)
require('./endpoints/rep')(router)
require('./endpoints/question')(router)
require('./endpoints/answer')(router)
require('./endpoints/project')(router)
require('./endpoints/board')(router)
require('./endpoints/card')(router)
require('./endpoints/search')(router)
require('./endpoints/mention')(router)
require('./endpoints/comment')(router)

module.exports = router
