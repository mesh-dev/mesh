'use strict'

const _ = require('lodash')
const { Answer } = require('../../models')
const Utils = require('../../utils')

class AnswerController {
  // Create a new answer
  static create (req, res) {
    Answer.create(req.body).then(answer => {
      // @TODO move into a Util
      // with proper error handling
      answer.setQuestion(req.params.id).then(answer => {
        answer.getQuestion().then(question => {
          question.addAnswer(answer)
          answer.createRep({
            reputation: 1,
            owner_id: req.user.id
          }).then(answer => {
            answer.getRep().then(rep => {
              rep.addUpvoter(req.user)
              return res.json(answer)
            })
          })
        })
      })
    }).catch(err => res.send(err))
  }

  // Update an existing answer
  static update (req, res) {
    Answer.findById(req.params.id)
      .then(answer => {
        if (answer.owner_id !== req.user.id) {
          return res.sendStatus(401)
        }

        const updated = _.merge(answer, req.body)
        updated.save()
          .then(answer => res.json(answer))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err))
  }
}

module.exports = AnswerController
