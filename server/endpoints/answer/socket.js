'use strict'

const { Answer, Rep, User } = require('../../models')

module.exports = (sockets) => {

  Answer.hook('afterUpdate', (answer, options) => {
    if (!answer.rep_id || !answer.owner_id) return

    Answer.findOne({
      where: { id: answer.id },
      include: [{
        model: Rep,
        as: 'rep',
        include: [{
          model: User,
          as: 'upvoters'
        },
        {
          model: User,
          as: 'downvoters'
        }]
      },
      {
        model: User,
        as: 'owner'
      }]
    }).then(answer => {
      sockets.to(`question:${answer.question_id}`).emit('answer:save', answer.toJSON())
    })
  })

}
