'use strict'

const controller = require('./AnswerController')
const { Middlewares, CONSTS } = require('../../utils')
const { ensureAuthenticated } = Middlewares
const { API_URL } = CONSTS

module.exports = (router) => {

  router.post(`${API_URL}/answers/:id`, ensureAuthenticated(), controller.create)
  router.put(`${API_URL}/answers/:id`, ensureAuthenticated(), controller.update)

}
