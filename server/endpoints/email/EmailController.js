'use strict'

const _ = require('lodash')
const Utils = require('../../utils')
const mailgun = require('mailgun-js')({
  apiKey: Utils.CONSTS.MAILGUN.MAILGUN_KEY,
  domain: Utils.CONSTS.MAILGUN.MAILGUN_DOMAIN
})

class EmailController {
  // subscribe a user to the mailing list and send welcome email
  static subscribe (req, res) {
    var member = {
      name: req.body.name,
      address: req.body.email,
      subscribed: true
    }
    var message = {
      from: 'Mesh <welcome@mesh-app.io>',
      to: req.body.email,
      subject: 'Hello and Welcome to Mesh',
      html: `<html>
              <img src="http://i.imgur.com/cwvrUME.png" style="width: 100%;"/>
              <p>Hello there ${req.body.name}!</p>
              <p>First of all thanks for signing up for Mesh. We are working hard on getting Mesh polished up for alpha version and we are gonna let you know as soon as it’s ready, we promise!
              <br><br>Get ready to boost your productivity on a whole nother level.</p><p>Yours truly,<br>The Mesh Team</p>
            </html>`
    }
    mailgun.lists('mesh@sandbox7626287abc2d424788b2279cb1808c09.mailgun.org')
    .members().create(member, (err, body) => {
      if (err) return res.send(err)
      mailgun.messages().send(message, (err, body) => {
        if (err) return res.send(err)

        if (req.body.invite) {
          Utils.SlackUtils.invite(req.body.email).then(response => {
            res.json(response)
          })
        } else {
          return res.json({})
        }
      })
    })
  }
}

module.exports = EmailController
