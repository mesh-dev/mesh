'use strict'

const EmailController = require('./EmailController')
const Utils = require('../../utils')
const apiURL = Utils.CONSTS.API_URL

let controller = EmailController

module.exports = (router) => {
  router.post(`${apiURL}/emails`, controller.subscribe)
}
