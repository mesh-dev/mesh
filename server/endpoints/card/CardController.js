'use strict'

const _ = require('lodash')
const models = require('../../models')
const Utils = require('../../utils')
const Card = models.Card

class CardController {
  // Create a new card
  static create (req, res) {
    Card.create(req.body).then(card => {
      card.getBoard().then(board => {
        board.addCard(card)
        return res.json(card)
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }

  // Update an existing card
  static update (req, res) {
    Card.findOne({ where: {
      id: req.params.id
    }}).then(card => {
      if (!card) return res.sendStatus(404)

      let updated = _.merge(card, req.body)
      updated.save().then(card => {
        return res.json(card)
      })
    }).catch(err => res.send(err))
  }

  static sort (req, res) {
    Card.findOne({ where: {
      id: req.params.id
    }}).then(card => {
      if (!card) return res.sendStatus(404)

      card.getBoard().then(board => {
        if (req.body.board_id) {
          Utils.CardUtils.handleOrderNewBoard(req, board, card)
          return res.sendStatus(200)
        } else {
          Utils.CardUtils.handleOrderSameBoard(req, board)
          return res.sendStatus(200)
        }
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }
}

module.exports = CardController
