'use strict'

const CardController = require('./CardController')
const Utils = require('../../utils')
const API_URL = Utils.CONSTS.API_URL
const Mw = Utils.Middlewares

let controller = CardController

module.exports = (router) => {
  router.post(`${API_URL}/cards`, Mw.ensureAuthenticated(), controller.create)
  router.put(`${API_URL}/cards/:id`, Mw.ensureAuthenticated(), controller.update)
  router.put(`${API_URL}/cards/sort/:id`, Mw.ensureAuthenticated(), controller.sort)
}
