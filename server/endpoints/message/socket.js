'use strict'

const { Message } = require('../../models')
const { EmailUtils } = require('../../utils')

async function getIncluded (message) {
  const sender = await message.getSender({ raw: true })

  if (message.kind === 'direct') {
    const receipent = await message.getReceipent({ raw: true })
    if (!receipent.active && receipent.email) {
      EmailUtils.notify(message, receipent)
    }
  }


  const json = message.toJSON()

  message = { ...json, sender }

  return message
}

module.exports = (sockets, socket) => {
  Message.hook('afterCreate', (message, options) => {
    getIncluded(message)
      .then(message => {
        if (message.kind === 'direct') {
          sockets.to(`user:${message.receipent_id}`)
            .emit(`user:message`, message)
          sockets.to(`user:${message.receipent_id}`)
            .emit(`message:${message.sender_id}`, message)
        } else {
          sockets.to(`project:${message.project_id}`)
            .emit('message:group', message)
        }
      })
  })

  Message.hook('afterUpdate', (message, options) => {
    if (message.kind === 'direct') {
      if (message.seen && !message.edited) {
        return sockets.to(`user:${message.sender_id}`)
          .emit(`message:${message.receipent_id}:seen`, message.toJSON())
      }
      sockets.to(`user:${message.receipent_id}`)
        .emit(`message:${message.sender_id}:updated`, message.toJSON())
    } else {
      message.getSender().then(sender => {
        let populatedMessage = message.toJSON()
        populatedMessage.sender = sender.toJSON()
        sockets.to(`project:${message.project_id}`)
          .emit('message:group:updated', populatedMessage)
      })
    }
  })
}
