'use strict'

const controller = require('./MessageController')
const { CONSTS, Middlewares } = require('../../utils')
const { ensureAuthenticated, protectProject } = Middlewares
const { API_URL } = CONSTS

module.exports = (router) => {
  router.post(`${API_URL}/messages`, ensureAuthenticated(), protectProject(), controller.create)

  router.get(`${API_URL}/messages/direct/:sender`, ensureAuthenticated(), controller.direct)
  router.get(`${API_URL}/messages/group/:project`, ensureAuthenticated(), controller.group)
  router.get(`${API_URL}/messages/unseen`, ensureAuthenticated(), controller.unseen)

  router.put(`${API_URL}/messages/:id`, ensureAuthenticated(), controller.update)
  router.put(`${API_URL}/messages/seen/:id`, ensureAuthenticated(), controller.seen)
  router.put(`${API_URL}/messages/see/:id`, ensureAuthenticated(), controller.see)
}
