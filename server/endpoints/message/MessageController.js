'use strict'

const _ = require('lodash')
const { Message, User } = require('../../models')
const { OgUtils, ProjectUtils, ConversationUtils } = require('../../utils')

class MessageController {
  // Create a new message
  static create (req, res) {
    Message.create(req.body)
      .then(message => {
        OgUtils
          .unfurlLink(message)
          .then(message => res.json(message))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err).status(500))
  }

  // Get message for user and sender user paginated
  static direct (req, res) {
    const itemsPerPage = 10
    Message.findAndCountAll({
      where: { $or: [{
        receipent_id: req.user.id,
        sender_id: req.params.sender
      },
      {
        sender_id: req.user.id,
        receipent_id: req.params.sender
      }]},
      include: [{
        model: User,
        as: 'receipent'
      },
      {
        model: User,
        as: 'sender'
      }],
      offset: itemsPerPage * (req.query.page - 1),
      limit: itemsPerPage,
      order: [['created_at', 'DESC']]
    })
      .then(results => {
        const pages = Math.ceil(results.count / itemsPerPage)
        res.json({ pages: pages, docs: results.rows.reverse() })
      })
      .catch(err => res.send(err))
  }

  // Get message for user and sender user paginated
  static group (req, res) {
    const itemsPerPage = 25
    Message.findAndCountAll({
      where: { project_id: req.params.project },
      include: [{
        model: User,
        as: 'sender'
      }],
      offset: itemsPerPage * (req.query.page - 1),
      limit: itemsPerPage,
      order: [['created_at', 'DESC']]
    })
      .then(results => {
        const pages = Math.ceil(results.count / itemsPerPage)
        res.json({ pages: pages, docs: results.rows.reverse() })
      })
      .catch(err => res.send(err))
  }

  // Update an existing message
  static update (req, res) {
    Message.findById(req.params.id).then(message => {
      if (!message) return res.sendStatus(404)

      var updated = _.merge(message, req.body)
      updated.save().then(message => {
        return res.json(message)
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }

  // Get all messages unseen
  static unseen (req, res) {
    Message.findAll({
      where: {
        receipent_id: req.user.id,
        seen: false
      },
      include: [{ all: true, nested: true }],
      order: [['created_at', 'DESC']]
    }).then(messages => {
      return res.json(messages)
    }).catch(err => res.send(err))
  }

  // Update one message seen
  static seen (req, res) {
    Message.findById(req.params.id).then(message => {
      let updated = _.merge(message, req.body)
      updated.save().then(message => {
        return res.json(message)
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }

  // Update all messages seen
  static see (req, res) {
    Message.findAll({ where: {
      receipent_id: req.user.id,
      sender_id: req.params.id,
      seen: false
    }}).then(messages => {
      for (var message of messages) {
        let updated = _.merge(message, req.body)
        updated.save()
      }

      return res.json(messages)
    }).catch(err => res.send(err))
  }
}

module.exports = MessageController
