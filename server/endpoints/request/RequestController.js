'use strict'

const _ = require('lodash')
const models = require('../../models')
const Utils = require('../../utils')

const { Request } = models
const { RequestUtils } = Utils

class RequestController {
  // Create a new request
  static create (req, res) {
    Request.create(req.body).then(request => {
      return res.json(request)
    }).catch(err => res.send(err))
  }

  // Get my requests pending
  static me (req, res) {
    Request.findAll({
      where: {
        requested_id: req.user.id,
        approved: null
      },
      include: [{ all: true, nested: true }]
    }).then(requests => {
      return res.json(requests)
    }).catch(err => res.send(err))
  }

  // Get recent requests
  static limited (req, res) {
    Request.findAll({
      where: {
        requested_id: req.user.id,
        approved: false
      },
      include: [{
        model: models.User,
        as: 'requester'
      }],
      limit: 9
    }).then(requests => {
      return res.json(requests)
    }).catch(err => res.send(err))
  }

  // Get all of a user's requests
  static query (req, res) {
    Request.findAll({
      where: {
        $or: [{
          requested_id: req.params.id
        },
        {
          requester_id: req.params.id
        }]
      },
      include: [{
        model: models.User,
        as: 'to'
      },
      {
        model: models.User,
        as: 'requester'
      }]
    }).then(requests => {
      if (!requests) return res.sendStatus(404)

      return res.json(requests)
    }).catch(err => res.send(err))
  }

  // Get one request that matches
  // each of the users in the request
  static match (req, res) {
    Request.findOne({
      where: {
        $or: [{
          requested_id: req.user.id,
          requester_id: req.params.id,
          approved: null
        },
        {
          requested_id: req.params.id,
          requester_id: req.user.id,
          approved: null
        }]
      }
    })
      .then(request => res.json(request))
      .catch(err => res.send(err))
  }

  // Update an existing request
  static update (req, res) {
    Request.findById(req.params.id)
      .then(request => {
        if (!request) return res.sendStatus(404)
        
        const updated = _.merge(request, req.body)
        if (updated.approved && updated.kind === 'join') {

          RequestUtils
            .joinTeam(updated)
            .then(request => {
              updated.save()
                .then(request => res.json(request))
                .catch(err => res.send(err).status(500))
            })
            .catch(err => res.send(err).status(500))
        } else if (updated.approved) {
          req.user.role = 'user'
          req.user.save()

          RequestUtils
            .addTeam(updated)
            .then(requet => {
              updated.save()
                .then(request => res.json(request))
                .catch(err => res.send(err).status(500))
            })
            .catch(err => res.send(err).status(500))
        } else {
          updated.save()
            .then(request => res.json(request))
            .catch(err => res.send(err).status(500))
        }
      })
      .catch(err => res.send(err).status(500))
  }
}

module.exports = RequestController
