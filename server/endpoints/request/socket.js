'use strict'

const { Request } = require('../../models')

async function getIncluded (request) {
  const included = await Request
    .findOne({
      where: { id: request.id },
      include: [{ all: true, nested: true }]
    })

  return included
}

module.exports = (sockets) => {
  Request.hook('afterCreate', (request, options) => {
    getIncluded(request)
      .then(request => {
        sockets.to(`user:${request.requested.id}`)
          .emit('user:requested', request.toJSON())
      })
  })

  Request.hook('afterUpdate', (request, options) => {
    getIncluded(request)
      .then(request => {
        if (!request.approved) {
          return sockets.to(`user:${request.requester.id}`)
            .emit('request:declined', request.toJSON())
        }

        sockets.to(`user:${request.requested_id}`)
          .emit('request:approved', request.toJSON())

        sockets.to(`user:${request.requester_id}`)
          .emit('request:approved', request.toJSON())
      })
  })
}
