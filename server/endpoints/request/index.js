'use strict'

const RequestController = require('./RequestController')
const Utils = require('../../utils')
const API_URL = Utils.CONSTS.API_URL
const Mw = Utils.Middlewares

let controller = RequestController

module.exports = (router) => {
  router.post(`${API_URL}/requests`, Mw.ensureAuthenticated(), controller.create)

  router.get(`${API_URL}/requests/me`, Mw.ensureAuthenticated(), controller.me)
  router.get(`${API_URL}/requests/:id`, Mw.ensureAuthenticated(), controller.query)
  router.get(`${API_URL}/requests/pending/:id`, Mw.ensureAuthenticated(), controller.match)

  router.put(`${API_URL}/requests/:id`, Mw.ensureAuthenticated(), controller.update)
}
