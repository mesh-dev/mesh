'use strict'

const controller = require('./TeamController')
const { Middlewares, CONSTS } = require('../../utils')
const API_URL = CONSTS.API_URL
const Mw = Middlewares

module.exports = (router) => {
  router.post(`${API_URL}/teams`, Mw.ensureAuthenticated(), Mw.verifyRecaptcha(), controller.create)

  router.get(`${API_URL}/teams/me`, Mw.ensureAuthenticated(), controller.me)
  router.get(`${API_URL}/teams/:id`, Mw.ensureAuthenticated(), controller.show)
  router.get(`${API_URL}/teams/validate/:name`, Mw.ensureAuthenticated(), controller.validate)

  router.put(`${API_URL}/teams/:id`, Mw.ensureAuthenticated(), controller.update)
  router.delete(`${API_URL}/teams/teammate/:id`, Mw.ensureAuthenticated(), controller.remove)
}
