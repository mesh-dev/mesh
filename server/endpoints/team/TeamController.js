'use strict'

const _ = require('lodash')
const { Team, User } = require('../../models')
const { TeamUtils } = require('../../utils')

class TeamController {
  // Validate team name not taken
  static validate (req, res) {
    Team.findOne({ where: {
      name: req.params.name
    }})
      .then(team => {
        if (team) return res.sendStatus(422)

        return res.json({}).status(200)
      })
      .catch(err => res.send(err))
  }

  // Create a new team
  static create (req, res) {
    Team.create(req.body)
      .then(team => {
        req.user.role = 'owner'
        req.user.save()

        if (req.user.team_id) {
          TeamUtils
            .leaveTeam(req.user, team)
            .then(team => res.json(team))
            .catch(err => res.send(err))
        } else {
          team.addTeammate(req.user)
          req.user.setTeam(team)

          res.json(team)
        }
      })
      .catch(err => res.send(err))
  }

  // Get my team
  static me (req, res) {
    Team.findOne({
      where: {
        id: req.user.team_id
      },
      include: [{
        model: User,
        as: 'teammates'
      }],
      order: [[
        { model: User, as: 'teammates' },
        'updated_at',
        'DESC'
      ]]
    })
      .then(team =>  res.json(team))
      .catch(err => res.send(err))
  }

  // Get a single team
  static show (req, res) {
    Team.findOne({
      where: {
        id: req.params.id
      },
      include: [{
        model: User,
        as: 'teammates'
      }]
    })
      .then(team => {
        if (!team) return res.sendStatus(404)

        return res.json(team)
      })
      .catch(err => res.send(err))
  }

  // Update an existing team
  static update (req, res) {
    Team.findOne({ include: [{
      model: User,
      as: 'owner',
      where: { id: req.user.id }
    }]})
      .then(team => {
        if (!team) return res.sendStatus(404)

        const updated = _.merge(team, req.body)
        updated.save()
          .then(team => res.json(team))
          .catch(err => res.send(err))
      })
      .catch(err => res.send(err))
  }

  // Remove a teammate from team
  static remove (req, res) {
    User.findOne({
      where: { id: req.params.id }
    })
      .then(user => {
        console.log(user)
        if (!user) return res.sendStatus(404)
        if (req.user.role !== 'owner') return res.sendStatus(403)

        TeamUtils
          .leaveTeam(user)
          .then(() => res.sendStatus(204))
          .catch(err => console.error(err))
      })
      .catch(err => res.send(err))
  }
}

module.exports = TeamController
