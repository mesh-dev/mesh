'use strict'

const { Team } = require('../../models')

async function getIncluded (team) {
  const included = await Team
    .findOne({
      where: { id: team.id },
      include: [{ all: true, nested: true }]
    })

  return included
}

module.exports = (sockets) => {
  Team.hook('afterUpdate', (team, options) => {
    getIncluded(team)
      .then(team => sockets.to(`team:${team.id}`).emit('team:save', team.toJSON()))
  })
}
