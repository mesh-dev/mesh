'use strict'

const BoardController = require('./BoardController')
const Utils = require('../../utils')
const API_URL = Utils.CONSTS.API_URL
const Mw = Utils.Middlewares

let controller = BoardController

module.exports = (router) => {
  router.post(`${API_URL}/boards`, Mw.ensureAuthenticated(), controller.create)
  router.put(`${API_URL}/boards/:id`, Mw.ensureAuthenticated(), controller.update)
  router.put(`${API_URL}/boards/sort/:id`, Mw.ensureAuthenticated(), controller.sort)
}
