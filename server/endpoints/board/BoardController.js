'use strict'

const _ = require('lodash')
const models = require('../../models')
const Utils = require('../../utils')
const Board = models.Board

class BoardController {
  // Create a new board
  static create (req, res) {
    Board.create(req.body).then(board => {
      return res.json(board)
    }).catch(err => res.send(err))
  }

  // Update an existing board
  static update (req, res) {
    Board.findOne({ where: {
      id: req.params.id
    }}).then(board => {
      if (!board) return res.sendStatus(404)

      let updated = _.merge(board, req.body)
      updated.save().then(board => {
        return res.json(board)
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }

  // Sort a dropped board
  static sort (req, res) {
    Board.findAll({ where: {
      project_id: req.params.id
    }}).then(boards => {
      if (!boards) return res.sendStatus(404)
      let updated = Utils.BoardUtils.handleOrder(req, boards)
      return res.json(updated)
    }).catch(err => res.send(err))
  }
}

module.exports = BoardController
