'use strict'

const uuid = require('node-uuid')
const { S3Utils, UserUtils, TeamUtils } = require('../../utils')

class UploadController {
  // Upload avatar to s3
  static avatar (req, res) {
    const keyName = uuid.v4()
    const bucketName = 'mesh-avatars'

    const file = JSON.parse(req.body.toString())
    const s3Utils = new S3Utils({ Bucket: bucketName, Key: keyName }, file)
    s3Utils.cropAndOptimize()
      .then(({ Location }) => {
        UserUtils
          .saveAvatar(req.user, Location)
          .then(({ avatar_url }) => res.json({ path: avatar_url }))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err).status(500))
  }

  // Upload cover to s3
  static cover (req, res) {
    const keyName = uuid.v4()
    const bucketName = 'mesh-covers'
    const file = JSON.parse(req.body.toString())

    const s3Utils = new S3Utils({ Bucket: bucketName, Key: keyName }, file)
    s3Utils.optimize()
      .then(({ Location }) => {
        UserUtils
          .saveCover(req.user, Location)
          .then(({ cover_url }) => res.json({ path: cover_url }))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err).status(500))
  }

  // Upload team cover
  static teamCover (req, res) {
    const keyName = uuid.v4()
    const bucketName = 'mesh-covers'
    const file = JSON.parse(req.body.toString())

    const s3Utils = new S3Utils({ Bucket: bucketName, Key: keyName }, file)
    s3Utils.optimize()
      .then(({ Location }) => {
        TeamUtils
          .saveCover(req.user.team_id, Location)
          .then(({ cover_url }) => res.json({ path: cover_url }))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err))
  }

  // Upload file to s3
  static file (req, res) {
    const keyName = uuid.v4()
    const bucketName = 'group-files'
    const file = JSON.parse(req.body.toString())

    const s3Utils = new S3Utils({ Bucket: bucketName, Key: keyName }, file)
    s3handleUpload()
      .then(data => {
        const path = {
          path: data.Location
        }
        return res.json(path)
      }).catch(err => res.send(err))
  }
}

module.exports = UploadController
