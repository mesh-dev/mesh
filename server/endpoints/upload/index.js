'use strict'

const controller = require('./UploadController')
const { Middlewares, CONSTS } = require('../../utils')
const { ensureAuthenticated } = Middlewares
const { API_URL } = CONSTS

module.exports = (router) => {
  router.post(`${API_URL}/uploads/avatar`, ensureAuthenticated(), controller.avatar)
  router.post(`${API_URL}/uploads/cover`, ensureAuthenticated(), controller.cover)
  router.post(`${API_URL}/uploads/team`, ensureAuthenticated(), controller.teamCover)
  router.post(`${API_URL}/uploads/file`, ensureAuthenticated(), controller.file)
}
