'use strict'

let Request = require('../../models/Request')

module.exports = (sockets) => {
  Request.schema.post('save', (doc) => {
    // @TODO find a way around querying the db again to populate
    Request.findById(doc.id).populate('from_user for_user', '-password')
    .exec((err, request) => {
      if (!request.approved) {
        return sockets.to(`user:${request.for_user.id}`).emit('user:request', request)
      }
      sockets.to(`user:${request.from_user.id}`).emit('user:approve', request)
    })
  })
}
