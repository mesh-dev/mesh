'use strict'

const controller = require('./UserController')
const { Middlewares, CONSTS } = require('../../utils')
const { ensureAuthenticated } = Middlewares
const { API_URL } = CONSTS

module.exports = (router) => {
  router.get(`${API_URL}/users/validate/:handle`, controller.validate)
  router.get(`${API_URL}/users/me`, ensureAuthenticated(), controller.me)
  router.get(`${API_URL}/users/:handle`, ensureAuthenticated(), controller.show)
  router.get(`${API_URL}/users/:handle/activity`, ensureAuthenticated(), controller.activity)

  router.put(`${API_URL}/users/:id`, ensureAuthenticated(), controller.update)
}
