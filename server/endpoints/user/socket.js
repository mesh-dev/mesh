'use strict'

const { User, Team } = require('../../models')

async function getIncluded ({ id }) {
  const included = await User
    .findOne({
      where: { id: id },
      include: [{
        model: Team,
        as: 'team'
      }]
    })

  return included
}

module.exports = (sockets) => {
  User.hook('afterUpdate', (user, options) => {
    if (!user.team_id) {
      sockets.emit(`user:${user.id}:save`, user.toJSON())
      return 
    } 

    getIncluded(user)
      .then(user =>
        sockets.emit(`user:${user.id}:save`, user.toJSON()))
  })
}
