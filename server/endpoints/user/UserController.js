'use strict'

const _ = require('lodash')
const { User, Team } = require('../../models')
const { ActivityUtils } = require('../../utils')

class UserController {
  // Validate user handle not taken
  static validate (req, res) {
    User.findOne({ where: {
      handle: req.params.handle
    }})
      .then(user => {
        if (user && req.user.id !== user.id) return res.sendStatus(422)

        return res.json(user)
      })
      .catch(err => res.send(err))
  }

  // Get my info
  static me (req, res) {
    User.findOne({ where: {
      id: req.user.id
    },
      include: [{
        model: Team,
        as: 'team',
        include: [{
          model: User,
          as: 'teammates'
        }]
      }]})
        .then(user => {
          if (!user) return res.sendStatus(401)

          return res.json(user)
        })
        .catch(err => res.send(err))
  }

  // Get single user
  static show (req, res) {
    User.findOne({
      include: [{
        model: Team,
        as: 'team'
      }],
      where: { handle: req.params.handle }
    })
      .then(user => {
        if (!user) return res.sendStatus(404)

        ActivityUtils
          .gatherActivity(user)
          .then(user => res.json(user))
          .catch(err => res.send(err))
      })
      .catch(err => res.send(err))
  }

  // Update an existing user
  static update (req, res) {
    User.findById(req.user.id)
      .then(user => {
        if (!user) return res.sendStatus(404)
        if (req.params.id != user.id) return res.sendStatus(401) // eslint-disable-line
        if (req.body.reputation !== user.reputation) {
          // Block any request with a manually set reputation as this
          // would be updated atomically
          return res.sendStatus(400)
        }

        const updated = _.merge(user, req.body)
        updated.save()
          .then(user => res.json(user))
          .catch(err => res.send(err))
      })
      .catch(err => res.send(err).status(500))
  }

  // Get a users activity
  static activity (req, res) {
    ActivityUtils
      .gatherActivity(req.user)
      .then(activity => res.json(activity))
      .catch(err => res.send(err))
  }
}

module.exports = UserController
