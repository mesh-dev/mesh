'use strict'

const RepController = require('./RepController')
const Utils = require('../../utils')
const API_URL = Utils.CONSTS.API_URL
const Mw = Utils.Middlewares

let controller = RepController

module.exports = (router) => {
  router.get(`${API_URL}/reps/:id`, Mw.ensureAuthenticated(), controller.show)
  router.put(`${API_URL}/reps/:id`, Mw.ensureAuthenticated(), controller.update)
}
