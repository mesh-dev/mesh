'use strict'

const _ = require('lodash')
const { Rep } = require('../../models')

class RepController {
  constructor () {
    Rep.hook('afterCreate', (rep, options) => {
      Rep.sum('reputation', { where: {
        owner_id: rep.owner_id
      }}).then(sum => {
        rep.getOwner().then(user => {
          user.sumRep(sum)
        })
      })
    })
    Rep.hook('afterUpdate', (rep, options) => {
      Rep.sum('reputation', { where: {
        owner_id: rep.owner_id
      }}).then(sum => {
        rep.getOwner().then(user => {
          user.sumRep(sum)
        })
      })
    })
  }

  // Get a single rep
  static show (req, res) {
    Rep.findById(req.params.id).then(rep => {
      if (!rep) return res.sendStatus(404)

      return res.json(rep)
    }).catch(err => res.send(err))
  }

  // Update an existing rep
  static update (req, res) {
    Rep.findById(req.params.id).then(rep => {
      if (!rep) return res.sendStatus(404)

      // check for proper reputation increments
      const { reputation, upVoters } = rep.toJSON()
      if (reputation + 5 !== req.body.reputation &&
         reputation - 2 !== req.body.reputation &&
         upVoters.length !== 0) {
        return res.sendStatus(400)
      }

      const voters = Promise.all([rep.getUpvoters(), rep.getDownvoters()])
      voters.then(([ upVoters, downVoters ]) => {
        if (_.some(upVoters, (v) => v.id === req.user.id) &&
           _.some(downVoters, (v) => v.id === req.user.id)) {
          return res.sendStatus(400)
        }

        if (rep.reputation < req.body.reputation) {
          rep.addUpvoter(req.user)
        } else {
          rep.addDownvoter(req.user)
        }

        const updated = _.merge(rep, req.body)
        updated.save().then(rep => {
          return res.json(rep)
        }).catch(err => res.send(err))
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }
}

let sum = new RepController()

module.exports = RepController
