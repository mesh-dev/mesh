'use strict'

const models = require('../../models')
const Rep = models.Rep

module.exports = (sockets) => {
  Rep.hook('afterUpdate', (rep, options) => {
    sockets.emit('rep:save', rep.toJSON())
  })
}
