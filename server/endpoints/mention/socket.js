'use strict'

const { Mention } = require('../../models')
const { EmailUtils } = require('../../utils')

async function getIncluded (mention) {
  const sender = await mention.getSender({ raw: true })
  const mentioned = await mention.getMentioned({ raw: true })

  mention.sender = sender

  if (!mentioned.active && mentioned.email) {
    EmailUtils.notify(mention, mentioned)
  }

  const json = mention.toJSON()
  mention = { ...json, sender }

  return mention
}

module.exports = (sockets) => {
  Mention.hook('afterCreate', (mention, options) => {
    getIncluded(mention)
      .then(mention => {
        sockets.to(`user:${mention.mentioned_id}`).emit('user:mentioned', mention)
      })
  })
}
