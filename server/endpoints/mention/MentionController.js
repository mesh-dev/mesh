'use strict'

const _ = require('lodash')
const models = require('../../models')
const Utils = require('../../utils')
const Mention = models.Mention

class MentionController {
  // Create a mention
  static create (req, res) {
    Mention.create(req.body).then(mention => {
      if (!mention) return res.sendStatus(404)

      return res.json(mention)
    }).catch(err => res.send(err))
  }

  // Get a single mention
  static show (req, res) {
    Mention.findById(req.params.id).then(mention => {
      if (!mention) return res.sendStatus(404)

      return res.json(mention)
    }).catch(err => res.send(err))
  }

  // Get my mentions
  static me (req, res) {
    Mention.findAll({
      where: { mentioned_id: req.user.id },
      include: [{ all: true, nested: true }]
    }).then(mentions => {
      return res.json(mentions)
    }).catch(err => res.send(err))
  }

  // Get my unseen mentions
  static unseen (req, res) {
    Mention.findAll({
      where: {
        mentioned_id: req.user.id,
        seen: false
      },
      include: [{ all: true, nested: true }]
    }).then(mentions => {
      return res.json(mentions)
    }).catch(err => res.send(err))
  }

  // Update an existing mention
  static update (req, res) {
    Mention.findById(req.params.id).then(mention => {
      if (!mention) return res.sendStatus(404)

      var updated = _.merge(mention, req.body)
      updated.save().then(mention => {
        return res.json(mention)
      }).catch(err => res.send(err))
    }).catch(err => res.send(err))
  }
}

module.exports = MentionController
