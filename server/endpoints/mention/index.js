'use strict'

const MentionController = require('./MentionController')
const Utils = require('../../utils')
const API_URL = Utils.CONSTS.API_URL
const Mw = Utils.Middlewares

let controller = MentionController

module.exports = (router) => {
  router.post(`${API_URL}/mentions`, Mw.ensureAuthenticated(), controller.create)
  router.get(`${API_URL}/mentions/me`, Mw.ensureAuthenticated(), controller.me)
  router.get(`${API_URL}/mentions/me/unseen`, Mw.ensureAuthenticated(), controller.unseen)
  router.get(`${API_URL}/mentions/:id`, Mw.ensureAuthenticated(), controller.show)
  router.put(`${API_URL}/mentions/:id`, Mw.ensureAuthenticated(), controller.update)
}
