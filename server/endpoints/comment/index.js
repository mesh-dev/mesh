'use strict'

const controller = require('./CommentController')
const { Middlewares, CONSTS } = require('../../utils')
const { ensureAuthenticated } = Middlewares
const { API_URL } = CONSTS

module.exports = (router) => {

  router.post(`${API_URL}/comments/:id`, ensureAuthenticated(), controller.create)
  router.put(`${API_URL}/comments/:id`, ensureAuthenticated(), controller.update)

}
