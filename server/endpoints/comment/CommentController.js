'use strict'

const _ = require('lodash')
const { Comment } = require('../../models')
const Utils = require('../../utils')

class CommentController {
  // Create a new comment
  static create (req, res) {
    Comment.create(req.body).then(comment => {
      // @TODO move into a Util
      // with proper error handling
      comment.setAnswer(req.params.id).then(answer => {
        comment.getAnswer().then(answer => {
          answer.addComment(comment)
          return res.json(comment)
        })
      })
    }).catch(err => res.send(err))
  }

  // Update an existing comment
  static update (req, res) {
    Comment.findById(req.params.id)
      .then(comment => {
        if (comment.owner_id !== req.user.id) {
          return res.sendStatus(401)
        }

        const updated = _.merge(comment, req.body)
        updated.save()
          .then(comment => res.json(answer))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err))
  }
}

module.exports = CommentController
