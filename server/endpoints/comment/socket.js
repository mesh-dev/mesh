'use strict'

const { Comment, User } = require('../../models')

async function getIncluded ({ id }) {
  const comment = await Comment.findOne({
    where: { id: id },
    include: [{ all: true, nested: true }]
  })

  const answer = await comment.getAnswer()
  const question = await answer.getQuestion()

  return { comment, question }
}

module.exports = (sockets) => {

  Comment.hook('afterUpdate', (comment, options) => {

    getIncluded(comment).then(({ comment, question }) => {
      sockets.to(`question:${question.id}`).emit('comment:save', comment.toJSON())
    })
  })

}
