'use strict'

const models = require('../../models')
const Question = models.Question

module.exports = (sockets) => {
  Question.hook('afterUpdate', (question, options) => {
    sockets.to(`question:${question.id}`).emit('question:save', question.toJSON())
  })
}
