const _ = require('lodash')
const { Question, Answer, Comment, User, Rep } = require('../../models')
const { QuestionUtils } = require('../../utils')

class QuestionController {
  // Create a new question
  static create (req, res) {
    QuestionUtils
      .initQuestion(req)
      .then(question => res.json(question))
      .catch(err => console.log(err))
  }

  // Gets a single question
  static show (req, res) {
    Question.findOne({ where: { id: req.params.id },
      include: [
        { model: User, as: 'owner' },
        {
          model: Rep,
          as: 'rep',
          include: [{
            model: User,
            as: 'upvoters'
          },
          {
            model: User,
            as: 'downvoters'
          }]
        },
        {
          model: Answer,
          as: 'answers',
          order: [[
            { model: Rep, as: 'rep' },
            'reputation',
            'DESC'
          ]],
          include: [
            { model: User, as: 'owner' },
            {
              model: Rep,
              as: 'rep',
              include: [{
                model: User,
                as: 'upvoters'
              },
                {
                  model: User,
                  as: 'downvoters'
                }]
            },
            {
              model: Comment,
              as: 'comments',
              include: [
                { model: User, as: 'owner' }
              ],
              order: [
                [ 'created_at', 'DESC' ]
              ]
            }
          ]
        }
      ]
    })
      .then(question => res.json(question))
      .catch(err => res.send(err).status(500))
  }

  // Paginate all participating teammates
  static paginate (req, res) {
    const itemsPerPage = 15
    const whereClause = req.user.team_id ? { team_id: req.user.team_id } : { owner_id: req.user.id }
    Question.findAndCountAll({
      where: whereClause,
      include: [
        { model: User, as: 'owner' },
        {
          model: Rep,
          as: 'rep',
          include: [{
            model: User,
            as: 'upvoters'
          },
          {
            model: User,
            as: 'downvoters'
          }]
        }
      ],
      offset: itemsPerPage * (req.query.page - 1),
      limit: itemsPerPage,
      order: [[
        { model: Rep, as: 'rep' },
        'reputation',
        'DESC'
      ]]
    })
      .then(({ count, rows }) => {
        const pages = Math.ceil(count / itemsPerPage)
        res.json({ pages: pages, docs: rows })
      })
      .catch(err => console.log(err))
  }

  // Update an existing question
  static update (req, res) {
    Question.findById(req.body.id)
      .then(question => {
        if (question.owner_id !== req.user.id) {
          return res.sendStatus(401)
        }

        const updated = _.merge(question, req.body)
        updated.save()
          .then(question => res.json(question))
          .catch(err => res.send(err).status(500))
      })
      .catch(err => res.send(err))
  }
}

module.exports = QuestionController
