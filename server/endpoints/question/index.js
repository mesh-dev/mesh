'use strict'

const controller = require('./QuestionController')
const { CONSTS, Middlewares } = require('../../utils')

const { API_URL } = CONSTS
const { ensureAuthenticated, verifyRecaptcha } = Middlewares

module.exports = (router) => {
  router.post(`${API_URL}/questions`, ensureAuthenticated(), verifyRecaptcha(), controller.create)

  router.get(`${API_URL}/questions/paginate`, ensureAuthenticated(), controller.paginate)
  router.get(`${API_URL}/questions/:id`, ensureAuthenticated(), controller.show)

  router.put(`${API_URL}/questions/:id`, ensureAuthenticated(), controller.update)
}
