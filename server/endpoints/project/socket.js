'use strict'

let Learning = require('../../models/Learning')

module.exports = (sockets) => {
  Learning.schema.post('save', (doc) => {
    // @TODO find a way around querying the db again to populate
    Learning.findById(doc.id)
    .populate({
      path: 'question answers',
      populate: { path: 'rep owner' }
    })
    .exec((err, objective) => {
      sockets.to(`objective:${objective.id}`).emit('objective:save', objective)
    })
  })
}
