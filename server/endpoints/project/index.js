'use strict'

const controller = require('./ProjectController')
const { CONSTS, Middlewares } = require('../../utils')

const { API_URL } = CONSTS
const { ensureAuthenticated, verifyRecaptcha } = Middlewares

module.exports = (router) => {
  router.post(`${API_URL}/projects`, ensureAuthenticated(), verifyRecaptcha(), controller.create)

  router.get(`${API_URL}/projects/paginate`, ensureAuthenticated(), controller.paginate)
  router.get(`${API_URL}/projects/:id`, ensureAuthenticated(), controller.show)

  router.put(`${API_URL}/projects/:id`, ensureAuthenticated(), controller.update)
}
