'use strict'

const merge = require('lodash/merge')
const { Project, User, Rep, Board, Card } = require('../../models')
const { ProjectUtils } = require('../../utils')

class ProjectController {
  // Create a new project
  static create (req, res) {
    ProjectUtils
      .initProject(req)
      .then(project => res.json(project))
      .catch(err => res.send(err).status(500))
  }

  // Get a single project
  static show (req, res) {
    Project.findOne({ where: { id: req.params.id },
      include: [{ all: true, nested: true }],
      order: [
        [
          { model: Board, as: 'boards' },
          'next',
          'ASC'
        ],
        [
          { model: Board, as: 'boards' },
          { model: Card, as: 'cards' },
          'next',
          'ASC'
        ]
      ]
    })
      .then(project => {
        if (!project) return res.sendStatus(404)
        res.json(project)
      })
      .catch(err => res.send(err).status(500))
  }

  // Get projects for team or owner
  static paginate (req, res) {
    const itemsPerPage = 15
    const whereClause = req.user.team_id ? { team_id: req.user.team_id } : { owner_id: req.user.id }

    Project.findAndCountAll({
      where: whereClause,
      include: [
        { model: User, as: 'owner' },
        {
          model: Rep,
          as: 'rep',
          include: [{
            model: User,
            as: 'upvoters'
          },
          {
            model: User,
            as: 'downvoters'
          }]
        }
      ],
      offset: itemsPerPage * (req.query.page - 1),
      limit: itemsPerPage,
      order: [[
        { model: Rep, as: 'rep' },
        'reputation',
        'DESC'
      ]]
    })
      .then(({ count, rows }) => {
        const pages = Math.ceil(count / itemsPerPage)
        res.json({ pages: pages, docs: rows })
      })
      .catch(err => res.send(err))
  }

  // Update an existing project
  static update (req, res) {
    Project.findById(req.params.id)
      .then(project => {
        if (project.owner_id !== req.user.id) {
          return res.sendStatus(403)
        }

        const updated = merge(project, req.body)
        updated.save()
          .then(project => res.json(project))
          .catch(err => res.send(err))
      })
      .catch(err => res.send(err))
  }
}

module.exports = ProjectController
