'use strict'

const _ = require('lodash')
const Utils = require('../../utils')
const db = require('../../models')

class SearchController {
  // Get global search results
  static elastic (req, res) {
    Utils.SearchUtils.searchModels(db, req.query.term)
    .then(results => {
      return res.json(results)
    }).catch(err => res.send(err))
  }

  static handleErrors (err) {
    return res.status(500).send(err)
  }
}

module.exports = SearchController
