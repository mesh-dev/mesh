'use strict'

const SearchController = require('./SearchController')
const Utils = require('../../utils')
const apiURL = Utils.CONSTS.API_URL

let controller = SearchController

module.exports = (router) => {
  router.get(`${apiURL}/search`, Utils.Middlewares.ensureAuthenticated(), controller.elastic)
}
