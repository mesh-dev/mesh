'use strict'

// Main modules
const path = require('path')
const chalk = require('chalk')
const express = require('express')
const cors = require('cors')
const helmet = require('helmet')
const favicon = require('serve-favicon')
const session = require('express-session')
const passport = require('passport')
const morgan = require('morgan')
const bodyParser = require('body-parser')
const SequelizeStore = require('connect-session-sequelize')(session.Store)
const { CONSTS } = require('./utils')

// App modules
const app = express()
const db = require('./models')
const router = require('./router')

// Sync the DB
const store = new SequelizeStore({
  db: db.sequelize
})
store.sync()
  .then(() => db.sequelize.sync()
    .then(() => {
      const unSearchableModels = [
        'sequelize',
        'CardBoard',
        'Comment',
        'Downvote',
        'QuestionAnswer',
        'Rep',
        'Request',
        'Session',
        'Upvote'
      ]
      Object.keys(db)
        .filter(key => unSearchableModels.indexOf(key) === -1)
        .map(model => db[model].addFullTextIndex())
    }))

app.set('views', __dirname + '/views')
app.engine('html', require('ejs').renderFile)

// Middlewares
if (process.env.NODE_ENV !== 'production') {
  require('../webpack').bindTo(app)
  app.use(morgan('dev'))
}
app.use(helmet())
app.use(bodyParser.json())
app.use(bodyParser.raw({ limit: '400mb', type: 'application/octect-stream' }))
app.use(bodyParser.urlencoded({ limit: '50mb', extended: true }))
app.use(session({
  key: 'mesh.sid',
  secret: 'mesh together',
  resave: true,
  saveUninitialized: false,
  store: store,
  cookie: { expires: new Date(Date.now() + 7 * 24 * 60 * 60 * 1000) }
  // proxy: true // if you do SSL outside of node.
}))
app.use(passport.initialize())
app.use(passport.session())
app.use(favicon(path.join(CONSTS.PUBLIC_PATH, '/favicon.ico')))

// Authentication
require('./auth')(app)

// Catch all undefined routes
router.get('*', (req, res, next) => {
  const err = new Error()
  err.status = 404
  next(err)
})

// Serve static files
app.use('/assets', express.static(path.join(CONSTS.PUBLIC_PATH, '/assets')))
app.use('/js', express.static(path.join(CONSTS.PUBLIC_PATH, '/js')))

// Router
app.use('/', router)

// Handle unmatched routes
const handle404 = (err, req, res, next) => {
  if (err.status !== 404) {
    return next()
  }

  res.render('error.page.html')
}
app.use(handle404)

module.exports = app