'use strict'

const sockets = require('socket.io')
const redis = require('socket.io-redis')
const passportSocketIo = require('passport.socketio')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const chalk = require('chalk')
const db = require('./models')
const SequelizeStore = require('connect-session-sequelize')(session.Store)
const { PeerUtils, UserUtils, ProjectUtils } = require('./utils')

const bindListeners = (io) => {
  io.adapter(redis({ host: 'localhost', port: 6379 }))

  // Auth and attach req.user to the socket
  io.use(passportSocketIo.authorize({
    cookieParser: cookieParser,
    key: 'mesh.sid',
    secret: 'mesh together',
    store: new SequelizeStore({ db: db.sequelize })
  }))

  io.on('connection', (socket) => {
    if (!socket.request.user) return
    console.log(chalk.yellow(`socket-io: ${socket.request.user.handle} connected`))

    // Handle online presence
    UserUtils.handlePresence(socket.request.user, true)

    // Add peer listeners
    PeerUtils.addListeners(socket, io)

    // Join a user's own room and team room
    socket.join(`user:${socket.request.user.id}`)

    // If user has team join team's room
    if (socket.request.user.team_id) {
      socket.join(`team:${socket.request.user.team_id}`)
    }

    // Handle rooms
    socket.on('join', (room) => {
      console.log(chalk.yellow(`socket-io: ${socket.request.user.handle} joined room ${room}`))

      // If it's a project room update present users
      if (/project/g.test(room)) {
        ProjectUtils.handlePresence('join', room, socket.request.user)
        socket.to(room).broadcast.emit('project:join', socket.request.user.toJSON())
      }

      socket.join(room)
    })
    socket.on('leave', (room) => {
      console.log(chalk.yellow(`socket-io: ${socket.request.user.handle} left room ${room}`))

      // If it's a project room update present users
      if (/project/g.test(room)) {
        ProjectUtils.handlePresence('leave', room, socket.request.user)
        socket.to(room).broadcast.emit('project:leave', socket.request.user.toJSON())
      }

      socket.leave(room)
    })

    socket.on('disconnecting', () => {
      Object.keys(socket.rooms)
        .forEach(room => {
          if (/project/g.test(room)) {
            ProjectUtils.handlePresence('leave', room, socket.request.user)
            socket.to(room).broadcast.emit('project:leave', socket.request.user.toJSON())
          }
        })
    })

    socket.on('disconnect', () => {
      console.log(chalk.yellow('socket-io:', `${socket.request.user.handle} disconnected`))
      // Handle offline presence
      UserUtils.handlePresence(socket.request.user, false)
    })

    socket.on(`user:${socket.request.user.id}:typing`, data => {
      io.to(`user:${data.to}`).emit(`user:${socket.request.user.id}:typing`, data.isTyping)
    })
    socket.on(`message:group:typing`, data => {
      socket.to(`project:${data.to}`).broadcast.emit(`message:group:typing`, data)
    })
  })

   // Endpoint hooks
  require('./endpoints/user/socket')(io)
  require('./endpoints/request/socket')(io)
  require('./endpoints/team/socket')(io)
  require('./endpoints/message/socket')(io)
  require('./endpoints/question/socket')(io)
  require('./endpoints/answer/socket')(io)
  require('./endpoints/rep/socket')(io)
  require('./endpoints/mention/socket')(io)
  require('./endpoints/comment/socket')(io)

  // Expose global.io var that can be used everywhere in the app.
  global.io = io
}

exports.connectTo = (server) => {
  const io = sockets(server, { transports: ['websocket', 'polling'] })
  bindListeners(io)
}
