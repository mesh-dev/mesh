'use strict'

const FacebookStrategy = require('passport-facebook').Strategy
const { CONSTS } = require('../../utils')
const { User } = require('../../models')

module.exports = (passport) => {
  passport.use(new FacebookStrategy({
    clientID: CONSTS.FACEBOOK.APP_ID,
    clientSecret: CONSTS.FACEBOOK.APP_SECRET,
    callbackURL: CONSTS.FACEBOOK.CALLBACK_URL,
    profileFields: ['id', 'email', 'displayName']
  }, (accessToken, refreshToken, profile, done) => {
    const newUser = {
      fb_id: profile.id,
      handle: profile.displayName.split(' ').join('').toLowerCase() + Math.floor(Math.random() * 90 + 10),
      email: profile.emails ? profile.emails[0].value : null,
      fb_token: accessToken
    }

    User.findCreateFind({
      where: { fb_id: profile.id },
      defaults: newUser
    })
        .then(([ user ]) => {
          if (newUser.access_token === user.access_token) {
            return done(null, user)
          }

          user.access_token = accessToken
          user.save()
            .then(user => done(null, user))
        })
        .catch(err => done(err, null))
  }
  ))
}
