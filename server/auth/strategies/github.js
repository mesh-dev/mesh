'use strict'

const GithubStrategy = require('passport-github').Strategy
const { CONSTS } = require('../../utils')
const { User } = require('../../models')

module.exports = (passport) => {
  passport.use(new GithubStrategy({
    clientID: CONSTS.GITHUB.CLIENT_ID,
    clientSecret: CONSTS.GITHUB.CLIENT_SECRET,
    callbackURL: CONSTS.GITHUB.CALLBACK_URL,
    profileFields: ['id', 'email', 'displayName']
  }, (accessToken, refreshToken, profile, done) => {
    const newUser = {
      gh_id: profile.id,
      handle: profile.username.toLowerCase() + Math.floor(Math.random() * 90 + 10),
      email: profile.emails ? profile.emails[0].value : null,
      gh_token: accessToken
    }

    User.findCreateFind({
      where: { gh_id: profile.id },
      defaults: newUser
    })
        .then(([ user ]) => {
          if (newUser.access_token === user.access_token) {
            return done(null, user)
          }

          user.access_token = accessToken
          user.save()
            .then(user => done(null, user))
        })
        .catch(err => done(err, null))
  }
  ))
}
