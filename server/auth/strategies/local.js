'use strict'

const LocalStrategy = require('passport-local').Strategy
const Utils = require('../../utils')

let User = require('../../models').User

module.exports = (passport) => {
  passport.use('local-signup', new LocalStrategy({
    usernameField: 'email',
    passReqToCallback: true
  }, (req, email, password, done) => {
    User.findOne({ where: { email: email } }).then(user => {
      if (user) return done(null, false)

      var user = User.build({
        email: email,
        handle: req.body.handle,
        password: User.generateHash(password)
      })
      user.save().then(user => {
        return done(null, user)
      }).catch(err => done(err, null))
    }).catch(err => done(err, null))
  }))

  passport.use('local-login', new LocalStrategy({
    usernameField: 'email',
    failureRedirect: '/login'
  }, (email, attempt, done) => {
    User.findOne({
      attributes: { include: ['password'] },
      where: { email: email }
    }).then(user => {
      if (!user) return done(null, false)

      if (!User.validPassword(attempt, user.password)) {
        return done(null, false)
      }

      done(null, user)
    }).catch(err => done(err, null))
  }))
}
