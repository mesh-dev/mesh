'use strict'

const passport = require('passport')
const { SlackUtils } = require('../utils')
const { User } = require('../models')

module.exports = (app) => {
  // serialize and deserialize
  passport.serializeUser((user, done) => {
    done(null, user.id)
  })
  passport.deserializeUser((id, done) => {
    User.findById(id).then(user => {
      done(null, user)
    }).catch(err => done(err, null))
  })

  // bring in strategies
  require('./strategies/local')(passport)
  require('./strategies/facebook')(passport)
  require('./strategies/github')(passport)

  // signup a new user
  app.post('/signup', passport.authenticate('local-signup'), (req, res) => {
    if (req.body.invite) {
      SlackUtils.invite(req.body.email)
    }

    res.json(req.user)
  })

  // login an existing user
  app.post('/login', passport.authenticate('local-login'), (req, res) => {
    if (req.user) {
      res.json(req.user)
    }
  })

  // logout
  app.get('/logout', (req, res) => {
    if (req.user) {
      req.logout()
    }
    res.redirect('/login')
  })

  // oAuth facebook
  app.get('/facebook', passport.authenticate('facebook', {
    scope: ['public_profile', 'email']
  }), (req, res) => {
    res.sendStatus(200)
  })
  app.get('/auth/facebook/callback', passport.authenticate('facebook', {
    failureRedirect: '/'
  }), (req, res) => {
    res.redirect(`/D`)
  })

  // oAuth github
  app.get('/github', passport.authenticate('github', {
    scope: ['user']
  }), (req, res) => {
    res.sendStatus(200)
  })
  app.get('/auth/github/callback', passport.authenticate('github', {
    failureRedirect: '/'
  }), (req, res) => {
    res.redirect(`/D`)
  })
}
