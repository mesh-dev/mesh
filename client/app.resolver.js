// Services
import { AuthService } from './services/AuthService'
import { ApiService } from './services/ApiService'

// Resolvers
import { CurrentUserResolver } from './resolvers/CurrentUserResolver'
import { RequestsResolver } from './resolvers/RequestsResolver'
import { TeamResolver } from './resolvers/TeamResolver'
import { UserResolver } from './resolvers/UserResolver'
import { ProjectResolver } from './resolvers/ProjectResolver'
import { QuestionResolver } from './resolvers/QuestionResolver'
import { MentionsResolver } from './resolvers/MentionsResolver'
import { DirectMessagesResolver } from './resolvers/DirectMessagesResolver'

export const APP_ROUTE_RESOLVERS = [
  AuthService,
  ApiService,
  CurrentUserResolver,
  RequestsResolver,
  TeamResolver,
  UserResolver,
  ProjectResolver,
  QuestionResolver,
  MentionsResolver,
  DirectMessagesResolver
]
