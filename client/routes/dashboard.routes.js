// Resolvers
import { UserResolver } from '../resolvers/user.resolver'
import { TeamResolver } from '../resolvers/team.resolver'

// Child pages
import { TeamPage } from '../pages/team/team.page'
import { LearningPage } from '../pages/learning/learning.page'
import { BuildingPage } from '../pages/building/building.page'
import { ProfilePage } from '../pages/profile/profile.page'
import { CreatePage } from '../pages/create/create.page'
import { StartPage } from '../pages/start/start.page'
import { ErrorPage } from '../pages/error/error.page'

import { createPageRoutes } from './create.routes'

export const dashboardPageRoutes = [
  {
    path: 'team/:id',
    component: TeamPage
    resolve: {
      team: TeamResolver
    }
  },
  {
    path: 'learning',
    component: LearningPage
  },
  {
    path: 'building',
    component: BuildingPage
  },
  {
    path: 'create',
    component: CreatePage,
    children: [...createPageRoutes]
  },
  {
    path: '',
    component: StartPage
  },
  {
    path: 'profile/:handle',
    component: ProfilePage,
    resolve: {
      user: UserResolver
    }
  },
  {
    path: '*',
    component: ErrorPage
  }
]
