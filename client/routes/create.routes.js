import { RouterConfig } from '@angular/router'
import { CurrentUserResolver } from '../resolvers/currentuser.resolver'

// Child Views
import { TeamCreateComponent } from '../components/create/team.component'
import { QuestionCreateComponent } from '../components/create/question.component'
import { ProjectCreateComponent } from '../components/create/project.component'
import { ErrorPage } from '../pages/error/error.page'

export const createPageRoutes = [
  {
    path: 'team',
    component: TeamCreateComponent
  },
  {
    path: 'question',
    component: QuestionCreateComponent
  },
  {
    path: 'project',
    component: ProjectCreateComponent
  },
  {
    path: '*',
    component: ErrorPage
  }
]
