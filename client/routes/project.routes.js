import { BoardComponent } from '../components/project/board.component'
import { GroupMessageComponent } from '../components/messages/group.component'
import { ErrorPage } from '../pages/error/error.page'

export const projectPageRoutes = [
  {
    path: 'communication',
    component: GroupMessageComponent
  },
  {
    path: 'tracking',
    component: BoardComponent
  }
  {
    path: '*',
      component: ErrorPage
  }
]
