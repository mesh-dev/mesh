'use strict'

import {Pipe, PipeTransform} from '@angular/core'

@Pipe({name: 'sort'})
export class SortPipe implements PipeTransform {
  transform (values) {
    return values.sort((a, b) => {
      return b.rep.reputation - a.rep.reputation
    })
  }
}
