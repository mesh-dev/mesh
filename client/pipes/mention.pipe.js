'use strict'

import { DomSanitizer } from '@angular/platform-browser'
import { Pipe, PipeTransform } from '@angular/core'

@Pipe({name: 'mention'})
export class MentionPipe implements PipeTransform {
  constructor (sanitizer: DomSanitizer) {
    this.sanitizer = sanitizer
  }
  transform (values) {
    values = values || ''
    let pattern = /\@(\w*)\s/gim
    let mentions = values.replace(pattern, `<a class="mention" routerLink="/D/profile$1">@$1 </a>`)
    return this.sanitizer.bypassSecurityTrustHtml(mentions)
  }
}
