'use strict'

import { Pipe, PipeTransform } from '@angular/core'

@Pipe({name: 'status'})
export class StatusPipe implements PipeTransform {
  transform (values, reverse) {
    if (reverse) {
      return values.sort((a, b) => {
        if (a.active || b.active) {
          return b.active - a.active
        } else {
          return b.updated_at > a.updated_at
        }
      }).reverse()
    }
    return values.sort((a, b) => {
      if (a.active || b.active) {
        return b.active - a.active
      } else {
        return b.updated_at > a.updated_at
      }
    })
  }
}

