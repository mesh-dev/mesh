'use strict'

import {Pipe, PipeTransform} from '@angular/core'
import * as moment from 'moment'

@Pipe({name: 'moment'})
export class MomentPipe implements PipeTransform {
  transform (value) {
    let date = moment(value).format('MMM DD h:mmA')
    return date
  }
}
