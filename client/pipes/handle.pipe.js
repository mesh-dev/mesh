'use strict'

import { Pipe, PipeTransform } from '@angular/core'

@Pipe({name: 'handle'})
export class HandlePipe implements PipeTransform {
  transform (items: any[], termHandle) {
    if (termHandle.length === 0) return items
    return items.filter(item => item.handle.indexOf(termHandle) !== -1)
  }
}
