'use strict'

import {Pipe, PipeTransform} from '@angular/core'
import * as Autolinker from 'autolinker'

@Pipe({name: 'linkify'})
export class LinkPipe implements PipeTransform {
  transform (value) {
    let urlRegex = /(\b(https?|ftp|file):\/\/[-A-Z0-9+&@#\/%?=~_|!:,.;]*[-A-Z0-9+&@#\/%=~_|])/ig
    if (urlRegex.test(value)) {
      let match = value.match(urlRegex)[0]
      return Autolinker.link(value)
    }
    return value
  }
}
