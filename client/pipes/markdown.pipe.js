'use strict'

import { Pipe, PipeTransform } from '@angular/core'
import { highlightAuto } from 'highlight.js'
import * as marked from 'marked'

const renderer = new marked.Renderer()
renderer.link = (href, title, text) => {
  return `<a href="${href}" target="_blank" title="${title}">${text}</a>`
}

marked.setOptions({
  renderer: renderer,
  sanitize: true,
  highlight: (code, language) => {
    return highlightAuto(code).value
  }
})

@Pipe({name: 'markdown'})
export class MarkdownPipe implements PipeTransform {
  transform (value) {
    if (!value) return
    return marked(value)
  }
}
