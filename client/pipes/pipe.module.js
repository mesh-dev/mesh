import { NgModule } from '@angular/core'
import { LinkPipe } from './link.pipe'
import { MarkdownPipe } from './markdown.pipe'
import { MentionPipe } from './mention.pipe'
import { MomentPipe } from './moment.pipe'
import { SortPipe } from './sort.pipe'
import { StatusPipe } from './status.pipe'
import { HandlePipe } from './handle.pipe'

@NgModule({
  declarations: [
    LinkPipe,
    MarkdownPipe,
    MentionPipe,
    MomentPipe,
    SortPipe,
    StatusPipe,
    HandlePipe
  ],
  exports: [
    LinkPipe,
    MarkdownPipe,
    MentionPipe,
    MomentPipe,
    SortPipe,
    StatusPipe,
    HandlePipe
  ]
})
export class PipeModule { }
