'use strict'

import { Component, Input } from '@angular/core'
import { NgIf, NgFor } from '@angular/common'
import { SocketService } from '../../services/socket.service'
import { ApiService } from '../../services/api.service'
import { MentionService } from '../../services/mention.service'

@Component({
  selector: 'comments',
  template: `
    <ul class="comments">
      <li *ngFor="let comment of comments; let i = index">
      <div class="comment">
        <edit [item]="comment"
              key="text"
              [textarea]="true"
              [hidden] ="!comment.editing"
              (handler)="comment.text = $event.text">
        </edit>
        <div class="markdown-body"
             [hidden]="comment.editing"
             [innerHTML]="comment.text | markdown | mention"></div>
        <button [hidden]="!comment.editing"
                (click)="handleEdit(comment, i)"
                class="send" type="submit">
          Save
        </button>
        <owner [item]="comment">
        </owner>
        <p class="edit" [hidden]="currentUser.id !== comment.owner_id || comment.editing"
           (click)="comment.editing = !comment.editing">
          edit
        </p>
      </div>
     </li>
    </ul>
    <form name="comment">
      <label>Reply</label>
      <div class="form-control">
        <textarea name="comment" [hidden]="editing"
                  placeholder="Discuss..."
                  [(ngModel)]="comment.text">
        </textarea>
      </div>
      <div class="live-preview markdown-body" [innerHTML]="comment.text | markdown"></div>
      <button type="submit" class="send" [disabled]="!comment.text" (click)="onSubmit()">Comment</button>
    </form>
  `
})
export class CommentsComponent {
  @Input() currentUser
  @Input() answer

  constructor(socket: SocketService, api: ApiService, mention: MentionService) {
    this.socket = socket
    this.api = api
    this.mention = mention
  }

  ngOnInit () {
    this.comment = {}
    this.comments = this.answer.comments

    this.socket.sync('comment:save', (comment) => {
      if (_.some(this.comments, { id: comment.id })) {
        const index = _.findIndex(this.comments, (c) => c.id === comment.id)
        this.comments[index] = comment
        return
      }

      this.comments = [ ...this.comments, comment ]
    })
  }

  handleRep (rep, index) {
    this.comments[index].rep = rep
    const updatedComments = [ ...this.comments ]

    this.comments = updatedComments
  }
  handleEdit (comment, index) {
    comment.editing = false
    this.api.update(`comments/${comment.id}`, comment)
      .then(comment => this.comments[index].text = comment.text)
      .catch(err => console.log(err))
  }
  onSubmit () {
    this.comment.owner_id = this.currentUser.id
    this.api.create(`comments/${this.answer.id}`, this.comment)
      .then(comment => this.comment = {})
      .catch(err => console.log(err))
  }
  handleMentions (user) {
    const splitMessage = this.comment.text.split('@')
    splitMessage[splitMessage.length - 1] = `${user.handle} `
    this.comment.text = splitMessage.join('@')
  }
}
