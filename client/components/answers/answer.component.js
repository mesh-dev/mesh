'use strict'

import { Component, Input, OnInit } from '@angular/core'
import { NgIf } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { MentionService } from '../../services/mention.service'

@Component({
  selector: 'answer',
  template: `
    <form name="answer">
      <label>Answer this question</label>
      <div class="form-control">
        <auto-complete [trigger]="'@'" [model]="answer.text" [candidates]="question.particpants"
                       (completion)="handleMentions($event)"></auto-complete>
        <textarea name="answer" [hidden]="editing"
                  placeholder="Be descriptive and provide resources. Supports markdown.."
                  [(ngModel)]="answer.text">
        </textarea>
      </div>
      <div class="live-preview markdown-body" [innerHTML]="answer.text | markdown"></div>
      <button type="submit" class="send" [disabled]="!answer.text" (click)="onSubmit()">Submit</button>
    </form>
  `
})
export class AnswerComponent {
  @Input() question
  @Input() currentUser

  constructor (api: ApiService, mention: MentionService) {
    this.api = api
    this.mention = mention
  }
  ngOnInit () {
    this.answer = {}
  }
  onSubmit () {
    this.answer.owner_id = this.currentUser.id
    this.api.create(`answers/${this.question.id}`, this.answer)
      .then(answer => {
        this.mention.findMentions(this.currentUser, answer.text)
        this.answer = {}
      })
      .catch(err => console.log(err))
  }
  handleMentions (user) {
    const splitMessage = this.answer.text.split('@')
    splitMessage[splitMessage.length - 1] = `${user.handle} `
    this.answer.text = splitMessage.join('@')
  }
}
