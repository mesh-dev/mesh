'use strict'

import { Component, Input } from '@angular/core'
import { NgIf, NgFor } from '@angular/common'
import { ApiService } from '../../services/api.service'

@Component({
  selector: 'answers',
  template: `
    <ul class="answers">
      <li *ngFor="let answer of answersArray | sort; let i = index">
        <reputation [currentUser]="currentUser"
                    [rep]="answer.rep" (newRep)="handleRep($event, i)">
        </reputation>
      <div class="answer">
        <edit [item]="answer"
              key="text"
              [textarea]="true"
              [hidden] ="!answer.editing"
              (handler)="answer.text = $event.text">
        </edit>
        <div class="markdown-body" [innerHTML]="answer.text | markdown | mention"></div>
        <svg-icon [icon]="'/assets/icons/edit.svg'"
                  [hidden]="currentUser.id !== answer.owner_id || answer.editing"
                  (click)="answer.editing = !answer.editing">
        </svg-icon>
        <button [hidden]="!answer.editing"
                (click)="handleEdit(answer, i)"
                class="send" type="submit">
          Save
        </button>
        <owner [item]="answer">
        </owner>
        <comments [answer]="answer" [currentUser]="currentUser"></comments>
      </div>
     </li>
    </ul>
  `
})
export class AnswersComponent {
  @Input() currentUser
  @Input() answersArray

  constructor(api: ApiService) { this.api = api }

  handleRep (rep, index) {
    this.answersArray[index].rep = rep
    const updatedAnswers = [...this.answersArray]

    this.answersArray = updatedAnswers
  }
  handleEdit (answer, index) {
    answer.editing = false
    this.api.update(`answers/${answer.id}`, answer)
      .then(answer => this.answersArray[index].text = answer.text)
      .catch(err => console.log(err))
  }
}
