'use strict'

import { Component } from '@angular/core'
import { NgIf } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { ToggleSwitchComponent } from '../common/toggleswitch.component'

@Component({
  selector: 'register',
  template: `
    <form name="register">
      <h1>MESH</h1>
      <p class="tagline">Learn and build, together</p>
      <div class="headline">
        <h5>Register now for early access</h5>
        <p>Come on you know you wanna</p>
      </div>
      <div class="name-email">
        <div class="name">
          <label>Name</label>
          <input type="text" name="name"
                 [class]="name.dirty && !name.valid && !thanks ? 'warn' : ''"
                 [(ngModel)]="model.name" #name="ngModel" />
        </div>
        <div class="email">
          <label>Email</label>
          <input type="email" name="email"
                 [class]="email.dirty && !email.valid && !thanks ? 'warn' : ''"
                 pattern="[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,6}"
                 [(ngModel)]="model.email" #email="ngModel" />
        </div>
      </div>
      <div class="warning" *ngIf="name.dirty && email.dirty && !thanks &&
           (!name.valid || !email.valid)">
        <p *ngIf="name.errors && name.errors.required">A name is required</p>
        <p *ngIf="email.errors && email.errors.required">An email is required</p>
        <p *ngIf="email.errors && email.errors.pattern">Please use a valid email</p>
      </div>
      <div class="slack" (click)="onToggle()">
        <toggle-switch [state]="model.invite" [lock]="false"></toggle-switch>
        <p [class]="!model.invite ? 'opted-out' : ''">Also get an invite to our Slack group</p>
      </div>
      <button type="submit" (click)="onSubmit()" [disabled]="!name.valid || !email.valid">Register</button>
      <p class="thanks" *ngIf="thanks">{{ thanks }}</p>
      <p class="try-it">Or <a routerLink="/login">login</a> with the test user and try out the demo</p>
    </form>
  `
})
export class RegisterComponent {
  constructor (api: ApiService) {
    this.api = api

    this.model = {}
    this.errors = {}
    this.model.invite = true
  }
  onToggle () {
    this.model.invite = !this.model.invite
  }
  onSubmit () {
    this.api.create('emails', this.model)
      .then(() => {
        this.thanks = 'Thanks for your interest! You will be hearing from us soon :)'
        this.model.name = ''
        this.model.email = ''
      })
      .catch(err => {
        this.errors.server = `Something went wrong.. Kindly try again :/ Code: ${err.status}`
      })
  }
}
