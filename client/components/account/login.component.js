'use strict'

import { Component } from '@angular/core'
import { Router } from '@angular/router'
import { NgIf } from '@angular/common'
import { AuthService } from '../../services/auth.service'
import { OAuthComponent } from './oauth.component'

@Component({
  selector: 'login',
  template: `
    <form name="login" (ngSubmit)="onSubmit()">
      <h1>MESH</h1>
      <p>Learn and build, together</p>
      <o-auth></o-auth>
      <p class="or">Or</p>
      <label>Email</label>
      <input type="email" name="email" [(ngModel)]="model.email" />
      <label>Password</label>
      <input type="password" name="password" [(ngModel)]="model.password" />
      <p *ngIf="errorMessage">{{ errorMessage }}</p>
      <button type="submit">Login</button>
    </form>
  `
})
export class LoginComponent {
  constructor (router: Router, auth: AuthService) {
    this.router = router
    this.auth = auth
    this.model = {}
  }
  onSubmit () {
    this.auth.login(this.model)
      .then(user => {
        this.router.navigate(['D'])
      })
      .catch(err => {
        if (err.status === 401) {
          return this.errorMessage = 'That email is not registered..'
        }
        this.errorMessage = `Something went wrong.. Kindly try again :/ Code: ${err}`
      })
  }
}
