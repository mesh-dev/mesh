'use strict'

import { Component } from '@angular/core'

@Component({
  selector: 'o-auth',
  template: `
    <a href="/facebook">
      <img src="/assets/icons/facebook.svg" />
    </a>
    <a href="/github">
      <img src="/assets/icons/github.svg" />
    </a>
  `
})
export class OAuthComponent {
}
