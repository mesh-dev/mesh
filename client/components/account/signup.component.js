import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { FormControl } from '@angular/forms'
import { NgIf } from '@angular/common'
import { OAuthComponent } from './oauth.component'
import { AuthService } from '../../services/auth.service'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'

@Component({
  selector: 'signup',
  template: `
    <form name="signup">
      <h1>MESH</h1>
      <p>Learn and build, together</p>
      <o-auth></o-auth>
      <p class="or">Or</p>
      <div class="name-email">
        <div class="name">
          <label>@Username</label>
          <input type="text" [class]="errors.handle ? 'warn' : ''"
                 name="handle" [formControl]="handle" [(ngModel)]="model.handle" />
        </div>
        <div class="email">
          <label>Email</label>
          <input type="email" [class]="errors.email ? 'warn' : ''"
                 name="email" [(ngModel)]="model.email" />
        </div>
      </div>
      <label>Password</label>
      <input type="password" name="password"
             [(ngModel)]="model.password" />
      <p *ngIf="errors.handle">{{ errors.handle }}</p>
      <p *ngIf="errors.email">{{ errors.email }}</p>
      <p *ngIf="errors.server">{{ errors.server }}</p>
      <div class="slack" (click)="onToggle()">
        <toggle-switch [state]="model.invite" [lock]="false"></toggle-switch>
        <p [class]="!model.invite ? 'opted-out' : ''">Also get an invite to our Slack community</p>
      </div>
      <button type="submit" (click)="onSubmit()" [disabled]="errors.handle">Signup</button>
    </form>
  `
})
export class SignupComponent {
  constructor (router: Router, auth: AuthService) {
    this.router = router
    this.auth = auth
  }
  ngOnInit () {
    this.model = {}
    this.model.invite = true

    this.errors = {}

    this.handle = new FormControl()
    this.handle.valueChanges
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe(handle => {
        if (!handle) return this.errors.handle = null

        this.auth.validate(handle)
          .then(() => {
            this.errors.handle = null
          })
          .catch(err => {
            this.errors.handle = 'That username is taken ¯\\_(ツ)_/¯'
          })
      })
  }
  onToggle () {
    this.model.invite = !this.model.invite
  }
  onSubmit () {
    this.auth.signup(this.model)
      .then(user => {
        this.router.navigate(['D'])
      })
      .catch(err => {
        if (err.status === 401) {
          return this.errors.email = 'That email is already registered..'
        }

        this.errors.server = `Something went wrong.. Kindly try again :/ Code: ${err.status}`
      })
  }
}
