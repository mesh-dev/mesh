import {
  Component,
  Input,
  Output,
  EventEmitter,
  OnInit,
  OnChanges,
  ViewChild,
  AfterViewInit,
  ChangeDetectorRef
} from '@angular/core'
import { NgIf } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { EventService } from '../../services/event.service'
import { SocketService } from '../../services/socket.service'
import { InfiniteScrollService } from '../../services/infinitescroll.service'
import { MessageBase } from './message.base'
import * as _map from 'lodash/map'
import * as _findIndex from 'lodash/findIndex'

@Component({
  selector: 'direct-message',
  providers: [ InfiniteScrollService ],
  template: `
    <div [class]="'direct-message'">
      <div class="tool-bar">
        <figure class="close" (click)="event.closeThread(user)">
          &times;
        </figure>
        <a routerLink="/D/profile/{{ user.handle }}">
          {{ user.handle }}
        </a>
      </div>
      <div class="messages" #directMessages
           infiniteScroll
           [scrollWindow]="false"
           [infiniteScrollUpDistance]="0"
           [infiniteScrollThrottle]="10"
           (scrolledUp)="onScrolled()">
        <figure class="loader" *ngIf="infiniteScroll.busy"></figure>
        <p class="done" *ngIf="done">
          You have reached the end of your direct message history with {{ user.handle }}
        </p>
        <div class="dm" *ngFor="let message of messages">
          <div class="message-container" *ngIf="message">
            <div class="user">
              <img class="avatar" [src]="message.sender.avatar_url" />
              <span>{{ message.seen && message.receipent_id !== currentUser.id ? '&#x2713;' : '' }}</span>
            </div>
            <div class="message">
              <span>{{ message.created_at | moment }}</span>
              <div class="message-text markdown-body" [hidden]="message.editing"
                 [innerHTML]="message.text | markdown | linkify | mention">
              </div>
              <edit *ngIf="message.editing" [item]="message"
                    [key]="'text'" [textarea]="false"
                    (handler)="handleMessage($event)"></edit>
              <attachment *ngIf="message.attachment"
                          [message]="message">
              </attachment>
            </div>
          </div>
          <message-tools *ngIf="currentUser.id == message.sender.id"
                  [hidden]="message.editing || false"
                  (tool)="handleTools($event, message)">
          </message-tools>
        </div>
        <p class="err" [hidden]="!err" (click)="retryMessage(message)">
          <em>Failed to send... Click to retry :p</em>
        </p>
      </div>
      <form name="dm" class="send-dm">
        <div class="user-typing" [hidden]="!typing">
          <em>{{ user.handle }} is typing</em>
        </div>
        <textarea #textarea name="message" [(ngModel)]="message.text"
                  (ngModelChange)="handleTyping($event)"
                  (keydown)="handleMessage($event)">
        </textarea>
        <button type="submit" class="send" (click)="sendMessage()"
                [disabled]="message.text === null || sending || err">
          <svg-icon [icon]="'/assets/icons/outline-send.svg'">
          </svg-icon>
        </button>
      </form>
    </div>
  `
})
export class DirectMessageComponent extends MessageBase {
  @Input() currentUser
  @Input() user

  @ViewChild('directMessages') messagesEl
  @ViewChild('textarea') textareaEl

  constructor (
    api: ApiService,
    socket: SocketService,
    event: EventService,
    cdr: ChangeDetectorRef,
    infiniteScroll: InfiniteScrollService
  ) {
    this.api = api
    this.socket = socket
    this.cdr = cdr
    this.event = event
    this.infiniteScroll = infiniteScroll
  }
  ngOnInit () {
    super()

    this.infiniteScroll
      .loadFirstPage(`messages/direct/${this.user.id}`)
      .subscribe(messages => {
        this.messages = messages
        this.handleScroll()
      })

    this.message = {}
    this.isEditMessage = {}
    this.typing = false

    this.api.update(`messages/see/${this.user.id}`, { seen: true })
      .catch(err => console.log(err))

    this.socket.sync(`message:${this.user.id}`, (message) => {
      this.alert.play()
      message.sender = this.user
      this.messages = [ ...this.messages, ...message ]
      this.handleScroll()
      this.typing = false

      this.api.update(`messages/seen/${message.id}`, { seen: true })
        .catch(err => console.log(err))
    })
    this.socket.sync(`message:${this.user.id}:seen`, (message) => {
      const index = _findIndex(this.messages, (m) => m.id === message.id)
      Object.assign(this.messages[index], message)
    })
    this.socket.sync(`message:${this.user.id}:updated`, (message) => {
      message.sender = this.user
      const index = _findIndex(this.messages, (m) => m.id == message.id)
      Object.assign(this.messages[index], message)
    })

    this.socket.sync(`user:${this.user.id}:typing`, isTyping => {
      this.typing = isTyping
    })

    this.socket.sync(`user:${this.user.id}:save`, user => {
      if (!user.active) {
        this.typing = false
      }
    })
  }
  handleTyping (event) {
    if (event.length > 1) return

    this.socket.emit(`user:${this.currentUser.id}:typing`, { 
      to: this.user.id,
      isTyping: event.length > 0 
    })
  }
  ngAfterViewInit () {
    this.textareaEl.nativeElement.focus()
  }
  ngOnDestroy () {
    this.socket.unsync(`message:${this.user.id}`)
    this.socket.unsync(`message:${this.user.id}:seen`)
    this.socket.unsync(`message:${this.user.id}:updated`)
    this.socket.unsync(`user:${this.user.id}:typing`)
    this.event.closeThread(this.user)
  }
  sendMessage () {
    this.sending = true

    const message = {
      receipent_id: this.user.id,
      sender_id: this.currentUser.id,
      kind: 'direct',
      text: this.message.text
    }
    message.sender = this.currentUser

    this.messages.push(message)
    this.handleScroll()

    const index = this.messages.indexOf(message)
    this.api.create(`messages`, message)
      .then(message => {
        Object.assign(this.messages[index], message)
        this.isEditMessage = this.messages[index]

        this.sending = false
        this.message.text = null
        this.err = null
      })
      .catch(err => this.err = err)
  }
  retryMessage (message) {
    this.sending = true

    const index = this.messages.indexOf(message)
    message.sender_id = this.currentUser.id
    message.receipent_id = this.user.id

    this.api.create(`messages`, message)
      .then(message => {
        Object.assign(this.messages[index], message)
        this.sending = false
        this.message.text = null
        this.err = null
      })
      .catch(err => this.err = err)
  }
  saveEdit (message) {
    const index = this.messages.indexOf(message)
    message.edited = true
    Object.assign(this.messages[index], message)

    this.api.update(`messages/${message.id}`, message)
      .then(message => {
        this.messages[index].editing = false
        this.textareaEl.nativeElement.focus()
        this.err = null
      })
      .catch(err => this.err = err)
  }
  onScrolled () {
    const { scrollHeight } = this.messagesEl.nativeElement
    this.previousHeight = scrollHeight

    this.infiniteScroll
      .fetchNextPage()
      .subscribe(messages => {
        if (!messages) {
          this.done = true
          return
        }

        this.infiniteScroll.busy = false
        this.messages = [ ...messages, ...this.messages ]

        // Fire change detection to pick up diff in elem scroll height
        this.cdr.detectChanges()
        const { scrollHeight } = this.messagesEl.nativeElement
        const newScrollTop = scrollHeight - this.previousHeight

        this.messagesEl.nativeElement.scrollTop = newScrollTop
      })
  }
}
