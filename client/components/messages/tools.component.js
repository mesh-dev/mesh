'use strict'

import { Component, Output, EventEmitter } from '@angular/core'
import { SvgIconComponent } from '../common/svgicon.component'

@Component({
  selector: 'message-tools',
  template: `
    <ul class="tools">
      <li class="tool">
        <svg-icon [icon]="'/assets/icons/edit.svg'"
                  (click)="onEdit()">
        </svg-icon>
      </li>
    </ul>
  `
})
export class MessageToolsComponent {
  @Output() tool = new EventEmitter();

  onEdit () {
    this.tool.emit('editor')
  }
}
