'use strict'

import { Component, Input, OnDestroy } from '@angular/core'
import { NgFor, NgIf } from '@angular/router'
import { DirectMessageComponent } from './direct.component'

@Component({
  selector: 'direct-messenger',
  template: `
    <div class="direct-messages">
      <direct-message *ngFor="let user of users"
                      [currentUser]="currentUser"
                      [user]="user">
      </direct-message>
    </div>
  `
})
export class MessengerComponent {
  @Input() currentUser;
  @Input() users;
}
