'use strict'

import {Component, Input} from '@angular/core'

@Component({
  selector: 'attachment',
  template: `
    <div class="attachment">
      <a [href]="message.attachment.ogUrl" target="_blank">
        <h6>{{ message.attachment.ogTitle }}</h6>
      </a>
      <a *ngIf="message.attachment.ogImage"
         [href]="message.attachment.ogUrl"
         target="_blank">
        <img [src]="message.attachment.ogImage.url" target="_blank" />
      </a>
      <p>{{ message.attachment.ogDescription }}
    </div>
  `
})
export class MessageAttachmentComponent {
  @Input() message;
}
