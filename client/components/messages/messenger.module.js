import { NgModule } from '@angular/core'
import { SharedModule } from '../common/shared.module'
import { MessengerComponent } from './messenger.component'
import { DirectMessageComponent } from './direct.component'
import { GroupMessageComponent } from './group.component'
import { MessageToolsComponent } from './tools.component'
import { MessageAttachmentComponent } from './attachment.component'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    MessengerComponent,
    DirectMessageComponent,
    GroupMessageComponent,
    MessageToolsComponent,
    MessageAttachmentComponent
  ],
  exports: [
    MessengerComponent,
    DirectMessageComponent,
    GroupMessageComponent,
    MessageToolsComponent,
    MessageAttachmentComponent
  ]
})
export class MessengerModule { }
