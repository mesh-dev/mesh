import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ChangeDetectorRef } from '@angular/core'
import { Router, RouterLink, ActivatedRoute } from '@angular/router'
import { NgIf, NgClass } from '@angular/common'

import { ApiService } from '../../services/api.service'
import { EventService } from '../../services/event.service'
import { SocketService } from '../../services/socket.service'
import { MentionService } from '../../services/mention.service'
import { InfiniteScrollService } from '../../services/infinitescroll.service'
import { MessageBase } from './message.base'
import * as _forEach from 'lodash/forEach'
import * as _findIndex from 'lodash/findIndex'

@Component({
  selector: 'group-message',
  providers: [ InfiniteScrollService ],
  template: `
    <div class="group-messages">
      <div class="messages"
           #groupMessages
           infiniteScroll
           [scrollWindow]="false"
           [infiniteScrollUpDistance]="0"
           [infiniteScrollThrottle]="10"
           (scrolledUp)="onScrolled()">
        <figure class="loader" *ngIf="infiniteScroll.busy"></figure>
        <p class="done" *ngIf="done">
          You  have reached the end of the message histroy for this project
        </p>
        <div class="gm" *ngFor="let message of messages">
          <div class="message-container" *ngIf="message">
            <div class="user">
              <img [src]="message.sender.avatar_url" />
            </div>
            <div class="message">
              <a class="username" routerLink="/D/{{ message.sender.handle }}">
                {{ message.sender.handle }}
              </a>
              <span>{{ message.created_at | moment }}</span>
              <div class="message-text markdown-body" [hidden]="message.editing" [innerHTML]="message.text | markdown | linkify | mention"></div>
              <edit *ngIf="message.editing"
                    [item]="message"
                    [key]="'text'"
                    [textarea]="true"
                    (handler)="handleMessage($event)">
              </edit>
              <attachment *ngIf="message.attachment" [message]="message">
              </attachment>
            </div>
            <message-tools *ngIf="currentUser.id == message.sender.id && !message.attachment"
                    [hidden]="message.editing || false"
                    (tool)="handleTools($event, message)">
            </message-tools>
          </div>
        </div>
        <p class="err" [hidden]="!err" (click)="retryMessage(message)">
          <em>Failed to send... Click to retry :p</em>
        </p>
      </div>
      <form name="gm" class="send-gm" (ngSubmit)="sendMessage()">
        <div class="user-typing" *ngIf="typingUsers.length > 0">
          <em *ngFor="let user of typingUsers">{{ user.handle }} is typing</em>
        </div>
        <upload [hidden]="loading" [allowed]="'*'"
                [kind]="'file'" (path)="handleFileUpload($event)">
        </upload>
        <div class="loading-container" *ngIf="loading">
          <figure class="loader"></figure>
        </div>
        <auto-complete [trigger]="'@'" [model]="message.text"
                       [candidates]="currentUser.team ? currentUser.team.teammates : []"
                       (completion)="handleMentions($event)">
        </auto-complete>
        <textarea #textarea name="message"
                  [(ngModel)]="message.text"
                  (ngModelChange)="handleTyping($event)"
                  (keydown)="handleMessage($event)"
                  (focus)="isFocused = true"
                  (blur)="isFocused = false">
        </textarea>
        <button type="submit" class="send" [disabled]="!message.text || sending || err">
          <svg-icon [icon]="'/assets/icons/outline-send.svg'"></svg-icon>
        </button>
      </form>
    </div>
  `
})
export class GroupMessageComponent extends MessageBase {
  @ViewChild('groupMessages') messagesEl
  @ViewChild('textarea') textareaEl

  constructor (
    router: Router,
    route: ActivatedRoute,
    api: ApiService,
    socket: SocketService,
    event: EventService,
    cdr: ChangeDetectorRef,
    mention: MentionService,
    infiniteScroll: InfiniteScrollService
  ) {
    this.route = route
    this.router = router
    this.api = api
    this.socket = socket
    this.cdr = cdr
    this.event = event
    this.mention = mention
    this.infiniteScroll = infiniteScroll
  }
  ngOnInit () {
    super()

    this.message = {}
    this.isEditMessage = {}
    this.typingUsers = []

    this.sub = this.route.data
      .subscribe(({ currentUser, project }) => {
        this.currentUser = currentUser
        this.project = project
        this.loadChatPageOne()
      })

    this.router.events
      .subscribe(({ url }) => /\/P/g.test(url) ? this.loadChatPageOne() : null)

    this.socket.sync('message:group', (message) => {
      if (this.project.id === message.project_id &&
        this.currentUser.id !== message.sender_id) {
        this.messages.push(message)
        this.handleScroll()
      }
    })
    this.socket.sync('message:group:updated', (message) => {
      if (this.project.id === message.project_id && this.currentUser.id !== message.sender_id) {
        _forEach(this.messages, (m, i) => {
          if (m.id === message.id) {
            return this.messages[i] = message
          }
        })
      }
    })
    this.socket.sync('message:group:typing', ({ isTyping, from }) => {
      if (isTyping && _findIndex(this.typingUsers, (u) => u.id === from.id) === -1) {
        this.typingUsers.push(from)
      } else {
        const index = this.typingUsers.indexOf(from)
        this.typingUsers.splice(index, 1)
      }
    })
  }
  ngAfterViewInit () {
    this.textareaEl.nativeElement.focus()
  }
  ngOnDestroy () {
    this.socket.emit(`message:group:typing`, {
      to: this.project.id,
      from: this.currentUser,
      isTyping: false 
    })
    this.socket.unsync('message:group')
    this.socket.unsync('message:updated')
    this.socket.unsync('message:group:typing')
  }
  loadChatPageOne () {
    this.infiniteScroll
      .loadFirstPage(`messages/group/${this.project.id}`)
      .subscribe(messages => {
        this.messages = messages
        this.handleScroll()
      })
  }
  handleTyping (evt) {
    if (evt.length > 1) return

    this.socket.emit(`message:group:typing`, {
      to: this.project.id,
      from: this.currentUser,
      isTyping: evt.length > 0
    })
  }
  sendMessage () {
    this.sending = true
    this.socket.emit(`message:group:typing`, {
      to: this.project.id,
      from: this.currentUser,
      isTyping: false 
    })
    const message = {
      sender_id: this.currentUser.id,
      project_id: this.project.id,
      kind: 'group',
      text: this.message.text
    }
    message.sender = this.currentUser

    this.messages.push(message)
    this.handleScroll()

    const index = this.messages.indexOf(message)
    this.api.create('messages', message)
      .then(message => {
        Object.assign(this.messages[index], message)
        this.isEditMessage = this.messages[index]

        this.sending = false
        this.mention.findMentions(this.currentUser, message.text)
        this.message.text = null
      })
      .catch(err => {
        const errMessage = {
          sender: {
            handle: 'meshbot',
            avatar_url: '/assets/images/mesh-logo-large.jpg'
          },
          text: '**403 Forbidden**: You are not a member of this team... Join them to ineteract with this project 💯'
        }

        if (err.status === 403) {
          this.messages.push(errMessage)
          this.message.text = null
          this.handleScroll()
          return
        }

        this.err = err
      })
  }
  retryMessage (message) {
    this.sending = true

    const index = this.messages.indexOf(message)
    message.sender_id = this.currentUser.id
    message.project_id = this.project.id

    this.api.create(`messages`, message)
      .then(message => {
        this.sending = false
        this.message.text = null
        this.err = null

        if(message.attachment) {
          this.messages[index].attachment = message.attachment
        }
      })
      .catch(err => this.err = err)
  }
  saveEdit (message) {
    const index = this.messages.indexOf(message)
    message.edited = true
    this.messages[index] = message
    this.api.update(`messages/${message.id}`, message)
      .then(message => {
        this.messages[index].editing = false
        this.textareaEl.nativeElement.focus()
      })
      .catch(err => this.err = err)
  }
  handleMentions (user) {
    if (this.message.editing) { return }

    let splitMessage = this.message.text.split('@')
    splitMessage[splitMessage.length - 1] = `${user.handle} `
    this.message.text = splitMessage.join('@')
    this.textareaEl.nativeElement.focus()
  }
  handleFileUpload (evt) {
    if (!evt) { return this.loading = true }

    const typeSplit = evt.split('.')
    const nameSplit = evt.split('/')
    const fileType = typeSplit[typeSplit.length - 1]
    const fileName = nameSplit[nameSplit.length - 1]

    const message = {
      sender_id: this.currentUser.id,
      project_id: this.project.id,
      kind: 'group',
      text: `uploaded a file ${fileName}`,
      attachment: {
        ogTitle: 'File Upload',
        ogUrl: evt,
        ogDescription: `A ${fileType} has been uploaded`,
        ogImage: fileType === 'jpeg' || fileType === 'gif' || fileType === 'png'
          ? { url: evt } : null
      }
    }

    this.api.create('messages', message)
    .then(message => {
      this.loading = false
      message.sender = this.currentUser
      this.message.text = null
      this.messages.push(message)

      this.handleScroll()
    }).catch(err => console.log(err))
  }
  onScrolled () {
    const { scrollHeight } = this.messagesEl.nativeElement
    this.previousHeight = scrollHeight

    this.infiniteScroll
      .fetchNextPage()
      .subscribe(messages => {
        if (!messages) {
          this.done = true
          return
        }

        this.infiniteScroll.busy = false
        this.messages = [ ...messages, ...this.messages ]

        // Fire change detection to pick up diff in elem scroll height
        this.cdr.detectChanges()
        const { scrollHeight } = this.messagesEl.nativeElement
        const newScrollTop = scrollHeight - this.previousHeight

        this.messagesEl.nativeElement.scrollTop = newScrollTop
      })
  }
}
