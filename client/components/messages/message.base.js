'use strict'

import * as Buzz from 'node-buzz'

export class MessageBase {
  constructor () {
    this.alert = new Buzz.sound(
      '/assets/sounds/direct.mp3',
      { preload: true }
    )
  }
  handleMessage (evt) {
    const e = evt.e || evt
    const message = evt || {}

    switch (true) {
      case e.keyCode === 38 && e.metaKey:
        e.preventDefault()
        this.isEditMessage.editing = true
        break
      case e.keyCode === 13 && message.editing:
        e.preventDefault()
        if (message.text) {
          this.saveEdit(message)
        }
        break
      case e.keyCode === 13 && !e.shiftKey && this.message.text != null:
        e.preventDefault()
        this.sendMessage()
        break
      case e.keyCode === 27:
        e.preventDefault()
        this.event.closeThread(this.user)
    }
  }
  handleTools (evt, message) {
    let index = this.messages.indexOf(message)
    if (evt === 'editor') {
      this.messages[index].editing = true
    }
  }
  handleScroll () {
    if (!this.cdr.destroyed) {
      this.cdr.detectChanges()
    }
    this.messagesEl.nativeElement.scrollTop =
    this.messagesEl.nativeElement.scrollHeight
  }
}
