'use strict'

import { Component, Input } from '@angular/core'
import { RouterLink } from '@angular/router'
import { NgIf } from '@angular/common'
import { SvgIconComponent } from '../common/svgicon.component'
import { MarkdownPipe } from '../../pipes/markdown.pipe'

@Component({
  selector: 'search-results',
  template: `
    <div class="results">
      <h4 class="title" [hidden]="results.length < 1">{{ title }}</h4>
      <div class="result" *ngFor="let result of results">
        <div class="rep" *ngIf="result.reputation || result.rep">
          <svg-icon [icon]="'/assets/icons/flask-purp.svg'"></svg-icon>
          <p>{{ result.reputation || result.rep.reputation }}</p>
        </div>
        <a class="name" [routerLink]="getRouterLink(path, result)">
          <img *ngIf="result.avatar_url" [src]="result.avatar_url" />
          <p>{{ result.name || result.title || result.handle }}</p>
        </a>
        <div class="info">
          <div class="markdown-body" [innerHTML]="result.description | markdown"></div>
          <span>{{ result.tags }}</span>
        </div>
      </div>
    </div>
  `
})
export class SearchResultsComponent {
  @Input() path;
  @Input() title;
  @Input() results;
  @Input() currentUser;

  getRouterLink (path, result) {
    if (path === '/P') {
      return [`${path}/${result.id}`]
    } else if (path === '/D') {
      return [`${path}/profile/${result.handle}`]
    } else {
      return [`${path}/${result.id}`]
    }
  }
}
