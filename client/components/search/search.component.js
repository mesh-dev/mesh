import { Component, OnInit, Input } from '@angular/core'
import { NgIf } from '@angular/common'
import { FormControl } from '@angular/forms'
import { ApiService } from '../../services/api.service'
import { SearchResultsComponent } from './results.component'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/distinctUntilChanged'

@Component({
  selector: 'search',
  template: `
    <form name="search-bar">
      <input type="search" autocomplete="off" name="search" placeholder="Search"
             (focus)="isFocused = true" (blur)="isFocused = false"
             [formControl]="term" [(ngModel)]="model.term" />
      <svg-icon class="magnify" [hidden]="isFocused" [icon]="'/assets/icons/search.svg'"></svg-icon>
    </form>
    <div class="elastic-dropdown" (click)="model = {}" *ngIf="model.term">
      <div class="fetching" *ngIf="fetching">
        <figure class="loader"></figure>
        <p>Meshing bits of knowledge...</p>
      </div>
      <search-results *ngIf="results && results.teams" [currentUser]="currentUser"
                      [results]="results.teams" path="/D/team"
                      title="Teams">
      </search-results>
      <search-results *ngIf="results && results.users" [currentUser]="currentUser"
                      [results]="results.users" path="/D"
                      title="People">
      </search-results>
      <search-results *ngIf="results && results.questions" [currentUser]="currentUser"
                      [results]="results.questions" path="/Q"
                      title="Questions">
      </search-results>
      <search-results *ngIf="results && results.building" [currentUser]="currentUser"
                      [results]="results.building" path="/P"
                      title="Projects">
      </search-results>
    </div>
  `
})
export class SearchComponent {
  @Input() currentUser;
  term = new FormControl();

  constructor (api: ApiService) {
    this.api = api
  }
  ngOnInit () {
    this.model = {}

    this.term.valueChanges
      .do(() => this.results = null)
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe(term => {
        if (!term) { return this.results = null }

        this.fetching = true
        this.api.fetch(`search?term=${term}`)
          .subscribe(results => {
            this.fetching = false
            this.results = results
          }, err => console.log(err))
      })
  }
}
