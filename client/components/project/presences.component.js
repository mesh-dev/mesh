'use strict'

import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { NgFor } from '@angular/common'
import { SocketService } from '../../services/socket.service'

@Component({
  selector: 'present-users',
  template: `
    <h6>People here now</h6>
    <span *ngIf="project.present.length < 2">
      You're the only one here @mention someone to get their attention
    </span>
    <ul class="present-users">
      <li *ngFor="let user of project.present">
        <img [src]="user.avatar_url" />
        <p>{{ user.handle }}</p>
      </li>
    </ul>
  `
})
export class PresentUsersComponent {
  @Input() currentUser;
  @Input() project;

  constructor (socket: SocketService) {
    this.socket = socket
  }
  ngOnInit () {
    if (!_.some(this.project.present, { id: this.currentUser.id })) {
      this.project.present.push(this.currentUser)
    }

    this.socket.sync('project:join', (user) => {
      if (!_.some(this.project.present, { id: user.id })) {
        this.project.present.push(user)
      }
    })
    this.socket.sync('project:leave', (user) => {
      _.remove(this.project.present, (u) => u.id === user.id)
    })
  }
  ngOnDestroy () {
    this.socket.unsync('project:join')
    this.socket.unsync('project:leave')
  }
}
