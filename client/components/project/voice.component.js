'use strict'

import { DomSanitizer } from '@angular/platform-browser'
import { Component, ChangeDetectorRef, Input, ViewChild, ViewChildren, OnDestroy } from '@angular/core'
import { NgIf, NgFor } from '@angular/common'
import { SvgIconComponent } from '../common/svgicon.component'
import { WebRTCService } from '../../services/webrtc.service'
import { SocketService } from '../../services/socket.service'
import * as getUserMedia from 'getusermedia'
import * as _ from 'lodash'

@Component({
  selector: 'voice-chat',
  template: `
    <div class="voice-chat">
      <div class="phone" (click)="onCall()">
        <svg-icon [icon]="isInCall ? '/assets/icons/calling.svg' :
                  '/assets/icons/call.svg'">
        </svg-icon>
        <span>{{ !isInCall ? 'Launch or join a call' : 'Leave the call' }}</span>
      </div>
      <ul class="peers">
        <li *ngFor="let peer of peers">
          <img #img [src]="peer.avatar_url" />
        </li>
      </ul>
      <audio #player autoplay crossOrigin="anonymous">
        <source *ngFor="let stream of streams" [src]="sanitizer.bypassSecurityTrustUrl(stream)">
      </audio>
    </div>
  `
})
export class VoiceChatComponent {
  @Input() currentUser
  @Input() project

  @ViewChildren('img') imgEls
  @ViewChild('player') playerEl

  peerCount = 0
  streams = []
  peers = []
  node = {}

  audioContext = new AudioContext()

  constructor (
    cdr: ChangeDetectorRef,
    sanitizer: DomSanitizer,
    webRTC: WebRTCService,
    socket: SocketService
  ) {
    this.cdr = cdr
    this.sanitizer = sanitizer
    this.webRTC = webRTC
    this.socket = socket
  }
  ngOnInit () {
    window.onbeforeunload = () => {
      if (this.isInCall) {
        this.socket.emit('call:leave', {
          project: this.project
        })
      }
    }

    this.webRTC.peerStream
      .subscribe(({ stream, peer }) => {
        this.streams.push(window.URL.createObjectURL(stream))
        this.peers.push(peer)
        this.peerCount++

        this.cdr.detectChanges()

        this.handleVisuals(stream, peer.id)
      })

    this.webRTC.peerHangup
      .subscribe(({ id }) => {
          _.forEach(this.peers, (peer, index) => {
            if (peer && peer.id === id) {
              // cleanup audio nodes
              this.node[id].onaudioprocess = null
              this.node[id] = null

              this.streams.splice(index, 1)
              this.peers.splice(index, 1)

              this.cdr.detectChanges()

              this.peerCount--
            }
          })
        })
  }
  ngOnDestroy () {
    if (this.isInCall) {
      this.socket.emit('call:leave', {
        project: this.project
      })
    }
  }
  onCall () {
    if (this.currentUser.team_id !== this.project.team_id) return

    if (this.isInCall) {
      this.webRTC.hangup()
      this.playerEl.nativeElement.volume = 0

      this.peers = []
      this.streams = []
      for (let k in this.node) {
        if (this.node[k]) {
          this.node[k].onaudioprocess = null
        }
      }
      this.node = {}

      for (let track of this.stream.getTracks()) {
        track.stop()
      }
      this.isInCall = false

      return
    }

    this.isInCall = true
    this.peers.push(this.currentUser)
    this.playerEl.nativeElement.volume = 1

    getUserMedia({ audio: true, video: false }, (err, stream) => {
      this.stream = stream
      this.handleVisuals(stream, this.currentUser.id)
      this.webRTC.init(this.currentUser, this.project, stream)
    })
  }
  handleVisuals (stream, peerId) {
    const analyser = this.audioContext.createAnalyser()
    const microphone = this.audioContext.createMediaStreamSource(stream)
    const newPeerIndex = _.findIndex(this.peers, (p) => p.id === peerId)

    analyser.smoothingTimeConstant = 0.3
    analyser.fftSize = 1024

    this.node[peerId] = this.audioContext.createScriptProcessor(1024, 1, 1)
    const node = this.node[peerId]

    microphone.connect(analyser)
    analyser.connect(node)
    node.connect(this.audioContext.destination)

    node.onaudioprocess = () => {
      const array = new Uint8Array(analyser.frequencyBinCount)
      analyser.getByteFrequencyData(array)

      const length = array.length
      let values = 0
      for (let a of array) {
        values += a
      }
      const average = values / length
      const imgEls = this.imgEls.toArray()
      const { nativeElement } = imgEls[newPeerIndex] || null

      if (nativeElement) {
        // scale the users avatar according to mic levels and constrain between 1 and 2
        nativeElement.style.transform = `scale(${Math.max(1, Math.min(2, average * 0.1))})`
      }
    }
  }
}

