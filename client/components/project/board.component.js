import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { EditFieldComponent } from '../common/editfield.component'
import { SortableDirective } from '../../directives/sortable.directive'
import { MarkdownPipe } from '../../pipes/markdown.pipe'

@Component({
  selector: 'board',
  template: `
    <ul class="boards" sortable [group]="'boards'"
        (drop)="handleBoardDrop($event, boards)">
      <li class="board" *ngFor="let board of boards; let i = index">
        <h6 *ngIf="!board.editing" (click)="board.editing = !board.editing">
          {{ board.title }}
        </h6>
        <edit *ngIf="board.editing"
              [item]="board"
              key="title"
              (handler)="handleTitle($event.e, board)"></edit>
        <ul class="cards" sortable [group]="'cards'"
            (add)="target = board" (drop)="handleCardDrop($event, board)">
          <li class="card" *ngFor="let card of board.cards">
            <p *ngIf="!card.editing" class="markdown-body"
               (click)="card.editing = !card.editing"
               [innerHTML]="card.text | markdown">
            </p>
            <edit *ngIf="card.editing"
                  [item]="card"
                  key="text"
                  [textarea]="true"
                  (handler)="handleText($event.e, card, i)">
            </edit>
          </li>
          <li class="card new" (click)="onNewCard(board)">
            <p>New Card</p>
          </li>
        </ul>
      </li>
      <li class="board new" (click)="onNewBoard()">
        <p>New Board</p>
      </li>
    </ul>
  `
})
export class BoardComponent {
  constructor (route: ActivatedRoute, api: ApiService) {
    this.route = route
    this.api = api
  }
  ngOnInit () {
    const { parent } = this.route
    this.sub = parent.data
      .subscribe(data => {
        this.currentUser = data.currentUser
        this.project = data.project
        this.boards = data.project.boards || []
      })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  handleOrder (evt, items) {
    let ref = items.splice(evt.oldIndex, 1)[0]
    items.splice(evt.newIndex, 0, ref)

    return items
  }
  handleCardDrop (evt, board) {
    let card = board.cards[evt.oldIndex]

    // The target is only defined on the event of
    // an addition to a different board
    if (this.target) {
      this.api.update(`cards/sort/${card.id}`, {
        board_id: this.target.id,
        next: evt.newIndex,
        prev: evt.oldIndex
      }).catch(err => console.log(err))

      this.target = null
      return
    }

    if (evt.oldIndex !== evt.newIndex && evt.type === 'end') {
      this.api.update(`cards/sort/${card.id}`, {
        next: evt.newIndex,
        prev: evt.oldIndex
      })/* .then(card => { */
        // board.cards = this.handleOrder(evt, board.cards);
      /* }) */.catch(err => console.log(err))
    };
  }
  handleBoardDrop (evt) {
    if (evt.oldIndex !== evt.newIndex && evt.type === 'end') {
      this.api.update(`boards/sort/${this.project.id}`, {
        next: evt.newIndex,
        prev: evt.oldIndex
      }).catch(err => console.log(err))
    };
  }
  onNewBoard () {
    let newBoard = {
      title: '',
      owner_id: this.currentUser.id,
      project_id: this.project.id,
      next: this.boards.length
    }
    this.api.create(`boards`, newBoard).then(board => {
      board.editing = true
      this.boards.push(board)
    }).catch(err => console.log(err))
  }
  onNewCard (board) {
    let newCard = {
      text: '',
      owner_id: this.currentUser.id,
      board_id: board.id,
      next: board.cards ? board.cards.length : 0
    }
    this.api.create(`cards`, newCard).then(card => {
      board.cards ? board.cards.push(card) : board.cards = [ card ]
    }).catch(err => console.log(err))
  }
  handleTitle (evt, board) {
    let title = evt.target.value
    if (evt.keyCode === 13) {
      evt.preventDefault()
      this.saveBoard(title, board.next)
    }
  }
  handleText (evt, card, boardIndex) {
    if (evt.keyCode === 13) {
      evt.preventDefault()
      card.text = evt.target.value
      this.saveCard(card, boardIndex)
    }
  }
  saveBoard (title, index) {
    this.boards[index].editing = false
    this.boards[index].title = title
    this.api.update(`boards/${this.boards[index].id}`, this.boards[index])
      .then(board => {
        this.boards[board.index] = board
      }).catch(err => console.log(err))
  }
  saveCard (card, boardIndex) {
    card.editing = false
    this.api.update(`cards/${card.id}`, card)
      .then(card => {
        this.boards[boardIndex].cards[card.index] = card
      }).catch(err => console.log(err))
  }
}
