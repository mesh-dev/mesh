'use strict'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { NgIf } from '@angular/common'
import { Router, ActivatedRoute } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { MarkdownPipe } from '../../pipes/markdown.pipe'
import { RecaptchaComponent } from '../common/recaptcha.component'

@Component({
  selector: 'question-create',
  template: `
    <form name="question" class="creation" #questionForm="ngForm">
      <label>What would you like to learn about?</label>
      <input type="text" name="title"
             placeholder="Choose a strong title"
             [(ngModel)]="question.title" />
      <label>Give some detail about your objective.</label>
      <div class="form-control">
        <textarea name="description"
                  placeholder="Help other's visualize your question. Supports markdown..."
                  [(ngModel)]="question.description">
        </textarea>
      </div>
      <div class="live-preview markdown-body" [innerHTML]="question.description | markdown"></div>
      <label>Add searchable tags.</label>
      <input type="text" name="tags"
             placeholder="Space separated"
             [(ngModel)]="question.tags" />
      <recaptcha (captchaResponse)="handleCaptcha($event)"></recaptcha>
      <button type="submit" [disabled]="!question.title && !question.description && !question.recaptcha_token"
              (click)="onSubmit()">
                Create Question
      </button>
    </form>
  `
})
export class QuestionCreateComponent {
  constructor (router: Router, route: ActivatedRoute, api: ApiService) {
    this.router = router
    this.route = route
    this.api = api
  }
  ngOnInit () {
    const { parent } = this.route.parent
    this.sub = parent.data
      .subscribe(({ currentUser }) => {
        this.currentUser = currentUser
      })

    this.question = {}
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  handleCaptcha (evt) {
    this.question.recaptcha_token = evt
  }
  onSubmit () {
    this.question.owner_id = this.currentUser.id
    this.api.create('questions', this.question)
      .then(question => {
        this.router.navigate(['D/learning'])
      })
      .catch(err => console.log(err))
  }
}
