import { NgModule } from '@angular/core'
import { SharedModule } from '../common/shared.module'
import { ProjectCreateComponent } from './project.component'
import { QuestionCreateComponent } from './question.component'
import { TeamCreateComponent } from './team.component'

@NgModule({
  imports: [
    SharedModule
  ],
  declarations: [
    ProjectCreateComponent,
    QuestionCreateComponent,
    TeamCreateComponent
  ],
  exports: [
    ProjectCreateComponent,
    QuestionCreateComponent,
    TeamCreateComponent
  ]
})
export class CreateModule { }
