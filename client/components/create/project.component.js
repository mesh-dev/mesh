'use strict'

import { Component, OnInit } from '@angular/core'
import { NgIf } from '@angular/common'
import { ActivatedRoute, Router } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { MarkdownPipe } from '../../pipes/markdown.pipe'
import { RecaptchaComponent } from '../common/recaptcha.component'

@Component({
  selector: 'project-create',
  template: `
    <form name="build" class="creation" (ngSubmit)="onSubmit()" #BuildForm="ngForm">
      <label>What would you like to build?</label>
      <input type="text" name="title"
             placeholder="Choose a strong title"
             [class]="errors.name ? 'warn' : ''"
             [(ngModel)]="build.title" />
      <p *ngIf="errors.name">{{ errors.name }}</p>
      <label>Give some detail about the project.</label>
      <textarea name="description"
                placeholder="Help other's visualize your goals. Supports markdown.." [class]="errors.description ? 'warn' : ''"
                [(ngModel)]="build.description">
      </textarea>
      <div class="live-preview markdown-body" [innerHTML]="build.description | markdown"></div>
      <label>Add searchable tags.</label>
      <input type="text" name="tags"
             placeholder="Space separated"
             [(ngModel)]="build.tags" />
      <p *ngIf="errors.tags">{{ errors.tags }}</p>
      <recaptcha (captchaResponse)="handleCaptcha($event)"></recaptcha>
      <button type="submit" [disabled]="!build.title && !build.description && !build.recaptcha_token">
        Create Project
      </button>
    </form>
  `
})
export class ProjectCreateComponent {
  constructor (router: Router, route: ActivatedRoute, api: ApiService) {
    this.router = router
    this.route = route
    this.api = api
  }
  ngOnInit () {
    const { parent } = this.route.parent
    this.sub = parent.data
      .subscribe(({ currentUser }) => {
        this.currentUser = currentUser
      })

    this.build = {}
    this.errors = {}
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  handleCaptcha (evt) {
    this.build.recaptcha_token = evt
  }
  onSubmit () {
    this.build.owner = this.currentUser.id
    this.api.create('projects', this.build)
      .then(build => {
        this.router.navigate(['D/building'])
      })
      .catch(err => console.log(err))
  }
}
