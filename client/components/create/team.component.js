'use strict'

import { Component, OnInit } from '@angular/core'
import { NgIf, NgClass } from '@angular/common'
import { FormControl } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { RecaptchaComponent } from '../common/recaptcha.component'
import { Observable } from 'rxjs/Rx'

@Component({
  selector: 'team-create',
  template: `
    <form name="team" class="creation" (ngSubmit)="onSubmit()">
      <label>What team name would you like?</label>
      <input type="text" name="name"
             placeholder="Choose something unique"
             [ngClass]="errors.name ? 'warn' : 'succcess'"
             [formControl]="name" [(ngModel)]="model.name" />
      <p *ngIf="errors.name">{{ errors.name }}</p>
      <label>Describe what your team is all about.</label>
      <textarea name="description"
                placeholder="This is public"
                [class]="errors.description ? 'warn' : 'succcess'"
                [(ngModel)]="model.description">
      </textarea>
      <p *ngIf="errors.description">{{ errors.email }}</p>
      <label>Add searchable tags.</label>
      <input type="text" name="tags"
             placeholder="Space separated"
             [(ngModel)]="model.tags" />
      <p *ngIf="errors.tags">{{ errors.tags }}</p>
      <recaptcha (captchaResponse)="handleCaptcha($event)">
      </recaptcha>
      <div class="submit">
        <button type="submit" [disabled]="!errors.name && !model.name && !model.description && !model.recaptcha_token">
          Create Team
        </button>
        <p class="warning" *ngIf="currentUser.team">
          <span>Warning: </span>
          By creating a new team you will leave {{ currentUser.team.name }}
        </p>
      </div>
    </form>
  `
})
export class TeamCreateComponent {
  name = new FormControl()

  constructor (
    router: Router,
    route: ActivatedRoute,
    api: ApiService,
    socket: SocketService,
    event: EventService
  ) {
    this.router = router
    this.route = route
    this.api = api
    this.socket = socket
    this.event = event
  }
  ngOnInit () {
    const { parent } = this.route.parent
    this.sub = parent.data
      .subscribe(({ currentUser, team }) => {
        this.currentUser = currentUser
      })

    this.model = {}
    this.errors = {}

    this.name.valueChanges
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe(name => {
        if (!name) return this.errors.name = null

        this.api.fetch(`teams/validate/${name}`)
          .subscribe(() => {
            this.errors.name = null
          },
          err => {
            this.errors.name = 'That team name is taken ¯\\_(ツ)_/¯'
          })
      })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  handleCaptcha (evt) {
    this.model.recaptcha_token = evt
  }
  onSubmit () {
    this.api.create('teams', this.model)
      .then(team => {
        this.socket.join(`team:${team.id}`)
        this.router.navigate(['/D/team/', team.id])
        this.event.changeTeam(team)
      })
      .catch(err => console.log(err))
  }
}
