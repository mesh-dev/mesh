'use strict'

import { Component, Input, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgIf } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { MomentPipe } from '../../pipes/moment.pipe'
import { SvgIconComponent } from '../common/svgicon.component'
import { EditFieldComponent } from '../common/editfield.component'

@Component({
  selector: 'profile-details',
  template: `
    <div [hidden]="editing" class="details">
      <div class="detail">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/flask-purp.svg'"></svg-icon>
          <span>{{ user.reputation }}</span>
        </div>
      </div>
      <div class="detail">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/at8.svg'"></svg-icon>
          <span class="handle">{{ user.handle }}</span>
        </div>
      </div>
      <div class="detail" *ngIf="user.description">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/rocket77.svg'"></svg-icon>
          <span>Description</span>
        </div>
        <p>{{ user.description }}</p>
      </div>
      <div class="detail" *ngIf="user.location">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/pin66.svg'"></svg-icon>
          <span>Location</span>
        </div>
        <p>{{ user.location }}</p>
      </div>
      <div class="detail" *ngIf="user.birthday">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/present33.svg'"></svg-icon>
          <span>Birthday</span>
        </div>
        <p>{{ user.birthday | moment }}</p>
      </div>
      <div class="detail" *ngIf="user.website">
        <div class="label">
          <svg-icon [icon]="'/assets/icons/globe35.svg'"></svg-icon>
          <span>Website</span>
        </div>
        <a href="http://{{ user.website }}" target="_blank">{{ user.website }}</a>
      </div>
    </div>
    <form class="editor" name="profile" *ngIf="editing">
      <label>@Username</label>
      <edit #username [item]="user"
            key="handle"
            [validate]="true"
            (handler)="user.handle = $event.handle">
            (err)="onErr($event)"
      </edit>
      <label>Description</label>
      <edit [item]="user"
            key="description"
            [textarea]="true"
            (handler)="user.description = $event.description">
      </edit>
      <label>Location</label>
      <edit [item]="user"
            key="location"
            (handler)="user.location = $event.location">
      </edit>
      <label>Birthday</label>
      <edit [item]="user"
            key="birthday"
            (handler)="user.birthday = $event.birthday">
      </edit>
      <label>Website</label>
      <edit [item]="user"
            key="website"
            (handler)="user.website = $event.website">
      </edit>
      <button type="submit" [disabled]="username.error" for="profile" (click)="saveEdit()">
        <svg-icon [icon]="'/assets/icons/check-mark.svg'"></svg-icon>
        <p>Save</p>
      </button>
    </form>
    <button class="editing" *ngIf="editable && !editing" (click)="toggleEditing()">
      <svg-icon [icon]="'/assets/icons/edit.svg'"></svg-icon>
      <p>Edit Info</p>
    </button>
  `
})
export class ProfileDetailsComponent {
  @Input() user
  @Input() editable

  constructor (router: Router, api: ApiService) {
    this.router = router
    this.api = api
  }
  ngOnInit () {
    this.ref = this.user.handle
    this.editing = false
  }
  onErr(err) {
    this.error = err
  }
  saveEdit () {
    this.api.update(`users/${this.user.id}`, this.user)
      .then(user => {
        if (this.ref !== user.handle) {
          this.router.navigate(['/D', 'profile', user.handle])
            .then(() => window.location.reload())
        }
        this.editing = false
      })
      .catch(err => console.log(err))
  }
  toggleEditing () {
    this.editing = true
  }
}
