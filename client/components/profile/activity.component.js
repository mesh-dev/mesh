'use strict'

import { Component, Input } from '@angular/core'
import { ActivatedRoute, RouterLink } from '@angular/router'
import { NgIf, NgFor } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { MomentPipe } from '../../pipes/moment.pipe'
import { MarkdownPipe } from '../../pipes/markdown.pipe'
import { SvgIconComponent } from '../common/svgicon.component'
import { ReputationComponent } from '../common/reputation.component'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/zip'

@Component({
  selector: 'activity',
  template: `
    <div class="activity">
      <ul class="questions" *ngIf="user.activity.questions.length > 0; else notLearning">
        <h4>Learning</h4>
        <li class="question" *ngFor="let question of user.activity.questions">
          <reputation [rep]="question.rep" [currentUser]="currentUser"></reputation>
          <div class="question-body">
            <a routerLink="/Q/{{ question.id }}">
              <h5>{{ question.title }}</h5>
            </a>
            <div class="info">
              <span>Created on {{ question.created_at | moment }} | </span>
              <span>Updated {{ question.updated_at | moment }}</span>
            </div>
            <div class="markdown-body" [innerHTML]="question.description | markdown">
            </div>
            <div class="tags">
              <span>tagged: {{ question.tags }}</span>
            </div>
          </div>
        </li>
      </ul>
      <ng-template #notLearning>
        <p class="guide">
          <svg-icon [icon]="'/assets/icons/speech-bubble20.svg'"></svg-icon>
          <em>{{ currentUser.id === user.id ? 'You are not learning anything yet' : user.handle + ' is not learning anything yet' }}</em>
          <br />
          <strong [hidden]="currentUser.id !== user.id">Create a <a routerLink="/D/create/question">question</a> to learn something new</strong>
        </p>
      </ng-template>
      <ul class="projects" *ngIf="user.activity.projects.length > 0; else notBuilding">
        <h4>Building</h4>
        <li class="project" *ngFor="let project of user.activity.projects">
          <reputation [rep]="project.rep" [currentUser]="currentUser"></reputation>
          <div class="project-body">
            <a routerLink="/P/{{ project.id }}">
              <h5>{{ project.title }}</h5>
            </a>
            <div class="info">
              <span>Created on {{ project.created_at | moment }} | </span>
              <span>Updated {{ project.updated_at | moment }}</span>
            </div>
            <div class="markdown-body" [innerHTML]="project.description | markdown">
            </div>
            <div class="tags">
              <span>tagged: {{ project.tags }}</span>
            </div>
          </div>
        </li>
      </ul>
      <ng-template #notBuilding>
        <p class="guide">
          <svg-icon [icon]="'/assets/icons/notebook91.svg'"></svg-icon>
          <em>{{ currentUser.id === user.id ? 'You are not building anything yet' : user.handle + ' is not building anything yet' }}</em>
          <br />
          <strong [hidden]="currentUser.id !== user.id">Create a <a routerLink="/D/create/project">project</a> to build something with your team</strong>
        </p>
      </ng-template>
    </div>
  `
})
export class UserActivityComponent {
  @Input() user

  constructor (route: ActivatedRoute) {
    this.route = route
  }

  ngOnInit () {
    const { parent } = this.route
    this.sub = parent.data
      .subscribe(({ currentUser }) => {
        this.currentUser = currentUser
      })
  }
  ngOnDestroy () { this.sub.unsubscribe() }
}
