'use strict'

import { Component, Input } from '@angular/core'
import { RouterLink } from '@angular/router'
import { NgIf } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'

@Component({
  selector: 'requests',
  template: `
    <div class="requests" *ngIf="shouldDisplay(); else remove">
      <p class="type" *ngIf="!noTeams && !request.id">
        Would you like to {{ requestType }} {{ !adding ? user.team.name : currentUser.team.name }}?
      </p>
      <div class="toggles" *ngIf="!noTeams && !request.id">
        <div class="switch-container" *ngIf="!noTeams" (click)="toggleSwitches()">
          <toggle-switch [state]="adding"
                         [lock]="locked">
          </toggle-switch>
          <toggle-switch [state]="!adding"
                         [lock]="locked">
          </toggle-switch>
        </div>
        <button class="request-btn" (click)="sendRequest()">
          {{ adding ? 'Request to add @' + user.handle + ' to ' + currentUser.team.name : 'Request to join ' + user.team.name }}
        </button>
      </div>
      <div class="hints">
        <p *ngIf="!user.team && !request.id">@{{ user.handle }} has no team..</p>
        <p *ngIf="!currentUser.team && !request.id">
          You have no team..
          <br>
          <a routerLink="/D/create/team">Create one now?</a>
        </p>
      </div>
      <p class="warning" *ngIf="user.team && currentUser.team && !adding">
        <span>Warning:</span> If accepted by {{ user.team.name }} you will leave {{
        currentUser.team.name }}
      </p>
      <p class="awaiting" *ngIf="request.id && request.approved === null">
        <em>Awaiting Approval</em>
      </p>
      <button class="accept" (click)="handleApproval(true)"
              *ngIf="request.requested_id == currentUser.id && request.approved === null">
        {{ request.kind == 'join' ? 'Add @' + user.handle + ' to ' + currentUser.team.name : 'Join ' + user.team.name }}
      </button>
      <button class="decline" (click)="handleApproval(false)"
              *ngIf="request.requested_id == currentUser.id && request.approved === null">
        Decline
      </button>
    </div>
    <ng-template #remove>
      <div class="remove"
           *ngIf="user.team_id === currentUser.team_id &&
           currentUser.role === 'owner'">
        <button (click)="onRemoval()">
          Remove Teammate
        </button>
      </div>
    </ng-template>
  `
})
export class RequestsComponent {
  @Input() user;
  @Input() currentUser;
  @Input() request;

  constructor (socket: SocketService, api: ApiService, event: EventService) {
    this.socket = socket
    this.api = api
    this.event = event
  }
  toggleSwitches () {
    if (this.locked) return

    this.adding = !this.adding
    if (this.adding) {
      this.requestType = `add @${this.user.handle} to`
    } else {
      this.requestType = 'join'
    }
  }
  sendRequest () {
    const request = {
      kind: this.requestType,
      requested_id: this.user.id,
      requester_id: this.currentUser.id,
      team_id: this.requestType === 'join' ? this.user.team.id : this.currentUser.team_id
    }

    this.api.create('requests', request)
      .then(request => {
        this.request = request
      })
      .catch(err => console.log(err))
  }
  handleApproval (approved) {
    this.api.update(`requests/${this.request.id}`, { approved: approved })
      .then(request => {
        if (request.approved) {
          this.request = request
          this.event.acceptRequest(request)
        } else {
          this.request = {}
          this.event.acceptRequest(request)
        }
      })
      .catch(err => console.log(err))
  }
  onRemoval () {
    this.api.remove(`teams/teammate/${this.user.id}`)
      .subscribe(
        () => {
          this.user.team_id = null
          this.user.team = null
        },
        err => console.log(err)
      )
  }
  shouldDisplay () {
    return this.request && this.user.id !== this.currentUser.id &&
           this.currentUser.team_id !== this.user.team_id
  }
  ngOnInit () {
    switch (true) {
      case !this.currentUser.team_id && this.user.team === null:
        this.noTeams = true
        break
      case this.currentUser.team_id && this.user.team !== null:
        this.requestType = `add @${this.user.handle} to`
        this.adding = true
        this.locked = false
        break
      case this.user.team != null:
        this.requestType = 'join'
        this.adding = false
        this.locked = true
        break
      case !this.user.team :
        this.requestType = `add @${this.user.handle} to`
        this.adding = true
        this.locked = true
        break
    }

    this.socket.sync('request:declined', (request) => this.request = {})
  }
}
