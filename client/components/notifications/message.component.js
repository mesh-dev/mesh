'use strict'

import { Component, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core'
import { NgFor, NgIf } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { SvgIconComponent } from '../common/svgicon.component'
import * as _some from 'lodash/some'
import * as _remove from 'lodash/remove'

@Component({
  selector: 'message-notifications',
  template: `
    <div class="messages">
      <div class="notifications" (click)="viewing = !viewing">
        <svg-icon [icon]="'/assets/icons/dm.svg'"></svg-icon>
        <div class="count" *ngIf="messages && messages.length >= 1">
          <span class="number">
            {{ messages.length < 9 ? messages.length : '9+' }}
          </span>
        </div>
      </div>
      <ul class="dropdown" *ngIf="messages && messages.length > 0 && viewing">
        <li *ngFor="let message of messages" (click)="handleDm(message.sender)">
          <img [src]="message.sender.avatar_url" />
          <div class="message">
            <a routerLink="/D/profile/{{ message.sender.handle }}">
              <p>{{ message.sender.handle }}</p>
            </a>
            <blockquote>{{ message.text }}</blockquote>
          </div>
        </li>
      </ul>
    </div>
  `
})
export class MessageNotificationsComponent {
  @Output() notification = new EventEmitter()

  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.socket = socket
    this.event = event
  }
  ngOnInit () {
    this.openThreads = []
    this.viewing = false
    this.sub = this.route.data
      .subscribe(data => this.messages = data.messages)

    this.socket.sync('user:message', (message) => {
      if (!_some(this.openThreads, { id: message.sender.id })) {
        this.messages = [ message, ...this.messages ]
      }
      this.notification.emit(message)
    })
    this.event.threadOpened$.subscribe(user => {
      this.openThreads.push(user)
    })
    this.event.threadClosed$.subscribe(user => {
      _remove(this.openThreads, (o) => o.id === user.id)
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
    this.socket.unsync('user:message')
  }
  handleDm (user) {
    this.viewing = false
    this.event.openThread(user)
    _remove(this.messages, (m) => m.sender.id === user.id)
  }
}
