'use strict'

import { Component, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core'
import { NgFor, NgIf } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { SvgIconComponent } from '../common/svgicon.omponent'
import * as _ from 'lodash'

@Component({
  selector: 'mention-notifications',
  template: `
    <div class="mentions">
      <div class="notifications" (click)="viewing = !viewing">
        <svg-icon [icon]="'/assets/icons/at9.svg'"></svg-icon>
        <div class="count" *ngIf="mentions && mentions.length >= 1">
          <span class="number">
            {{ mentions.length < 9 ? mentions.length : '9+' }}
          </span>
        </div>
      </div>
      <ul class="dropdown" *ngIf="mentions && mentions.length > 0 && viewing">
        <li *ngFor="let mention of mentions">
          <img [src]="mention.sender.avatar_url" />
          <div class="info">
            <span>Mentioned by </span>
            <a routerLink="/D/profile/{{ mention.sender.handle }}">
              {{ mention.sender.handle }}
            </a>
            <blockquote><p>{{ mention.text }}</p></blockquote>
            <div class="view" (click)="onMarkAsRead(mention)">
              <a routerLink="{{ mention.link }}">
                View this now
              </a>
              <p>Mark as read</p>
            </div>
          </div>
        </li>
      </ul>
    </div>
  `
})
export class MentionNotificationsComponent {
  @Output() notification = new EventEmitter()

  constructor (
    route: ActivatedRoute,
    api: ApiService,
    socket: SocketService
  ) {
    this.route = route
    this.api = api
    this.socket = socket
  }
  ngOnInit () {
    this.viewing = false
    this.sub = this.route.data.subscribe(data => this.mentions = data.mentions)
    this.socket.sync('user:mentioned', (mention) => {
      this.notification.emit(mention)
      this.mentions = [ mention, ...this.mentions ]
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
    this.socket.unsync('user:mentioned')
  }
  onMarkAsRead (mention) {
    this.viewing = false
    _.pull(this.mentions, mention)
    this.api.update(`mentions/${mention.id}`, { seen: true })
      .catch(err => console.log(err))
  }
}
