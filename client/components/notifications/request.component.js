'use strict'

import { Component, Output, OnInit, OnDestroy, EventEmitter } from '@angular/core'
import { NgFor, NgIf } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { SvgIconComponent } from '../common/svgicon.component'
import * as _remove from 'lodash/remove'

@Component({
  selector: 'request-notifications',
  template: `
    <div class="requests">
      <div class="notifications" (click)="viewing = !viewing">
        <svg-icon [icon]="'/assets/icons/plug.svg'"></svg-icon>
        <div class="count" *ngIf="requests && requests.length >= 1">
          <span class="number">
            {{ requests.length < 9 ? requests.length : '9+' }}
          </span>
        </div>
      </div>
      <ul class="dropdown" *ngIf="requests && requests.length > 0 && viewing">
        <li *ngFor="let request of requests">
          <img [src]="request.requester.avatar_url" />
          <a routerLink="/D/profile/{{ request.requester.handle }}">
            <p>
              Requested to {{ request.kind }}
              {{ request.kind == 'join' ?
                 request.requested.team.name :
                 request.requester.team.name }}
              by @{{ request.requester.handle }}
            </p>
            <span>click to view</span>
          </a>
        </li>
      </ul>
    </div>
  `
})
export class RequestNotificationsComponent {
  @Output() notification = new EventEmitter()

  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.socket = socket
    this.event = event
  }
  ngOnInit () {
    this.viewing = false

    this.sub = this.route.data
      .subscribe(data => this.requests = data.requests)

    this.socket.sync('user:requested', (request) => {
      this.notification.emit(request)
      this.requests = [ request, ...this.requests ]
    })

    this.event.requestAccepted$
      .subscribe(request =>
        _remove(this.requests, (r) => r.id === request.id))
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
    this.socket.unsync('user:requested')
  }
}
