'use strict'

import { Component } from '@angular/core'
import { MessageNotificationsComponent } from './message.component'
import { RequestNotificationsComponent } from './request.component'
import { MentionNotificationsComponent } from './mention.component'
import { PushNotificationsService } from 'angular2-notifications'

@Component({
  selector: 'notifications',
  template: `
    <request-notifications (notification)="handlePushRequest($event)">
    </request-notifications>
    <mention-notifications (notification)="handlePushMention($event)">
    </mention-notifications>
    <message-notifications (notification)="handlePushMessage($event)">
    </message-notifications>
  `
})
export class NotificationsComponent {
  constructor (_pushNotifications: PushNotificationsService) {
    this._pushNotifications = _pushNotifications
    this._pushNotifications.requestPermission()
  }

  handlePushMessage (evt) {
    this._pushNotifications.create('Mesh', {
      body: evt.text,
      icon: evt.sender.avatar_url
    }).subscribe(
      res => res,
      err => console.log(err)
    )
  }
  handlePushRequest (evt) {
    this._pushNotifications.create('Mesh', {
      body: `New request from @${evt.requester.handle}`,
      icon: evt.requester.avatar_url
    }).subscribe(
      res => res,
      err => console.log(err)
    )
  }
  handlePushMention (evt) {
    this._pushNotifications.create('Mesh', {
      body: `Mentioned by @${evt.sender.handle}`,
      icon: evt.sender.avatar_url
    }).subscribe(
      res => res,
      err => console.log(err)
    )
  }
}
