import { NgModule } from '@angular/core'
import { PushNotificationsModule, PushNotificationsService } from 'angular2-notifications'
import { SharedModule } from '../common/shared.module'
import { NotificationsComponent } from './notifications.component'
import { MessageNotificationsComponent } from './message.component'
import { RequestNotificationsComponent } from './request.component'
import { MentionNotificationsComponent } from './mention.component'

@NgModule({
  imports: [ SharedModule, PushNotificationsModule ],
  providers: [ PushNotificationsService ],
  declarations: [
    MessageNotificationsComponent,
    RequestNotificationsComponent,
    MentionNotificationsComponent,
    NotificationsComponent
  ],
  exports: [
    NotificationsComponent
  ]
})
export class NotificationsModule { }
