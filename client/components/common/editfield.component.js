'use strict'

import { Component, AfterViewInit, OnInit, ViewChild, Input, Output, EventEmitter } from '@angular/core'
import { FormControl } from '@angular/forms'
import { NgClass } from '@angular/common'
import { AuthService } from '../../services/auth.service'

import { Observable } from 'rxjs/Observable'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'

@Component({
  selector: 'edit',
  template: `
    <form name="edit" class="edit" #form>
      <input *ngIf="!textarea" name="edit-input"
             [ngClass]="error ? 'warn edit-field' : 'edit-field'"
             [formControl]="validation"
             [(ngModel)]="item[key]"
             (keydown)="onEdit($event)" />
      <textarea *ngIf="textarea" rows="4" name="edit-area"
                [(ngModel)]="item[key]"
                (keydown)="onEdit($event)">
      </textarea>
    </form>
    <div class="error" *ngIf="error">
      <p class="status">
        <strong>{{ error.status }}</strong>
      </p>
      <p class="msg">
        {{ error.msg }}
      </p>
    </div>
  `
})
export class EditFieldComponent {
  @Input() item
  @Input() key

  @Input() textarea = false
  @Input() validate = false

  @Output() handler = new EventEmitter()

  @ViewChild('form') formEl

  validation = new FormControl()

  constructor (auth: AuthService) {
    this.auth = auth
  }
  ngAfterViewInit () {
    this.formEl.nativeElement.children[0].focus()
  }
  ngOnInit () {
    if (!this.validate) return
    const ref = this.item[this.key]

    this.validation.valueChanges
      .debounceTime(600)
      .distinctUntilChanged()
      .subscribe(validation => {
        if (ref === validation) {
          this.error = null
          return
        }

        if (!validation) {
          this.error = new Error()
          this.error.msg = 'An @username is required!'
          return
        }

        this.auth
          .validate(validation)
          .then(() => {
            this.error = null
          })
          .catch(err => {
            this.error = new Error()
            this.error.status = err.status
            this.error.type = validation
            this.error.msg = 'That @username is taken :/'
          })
      })
  }
  onEdit (evt) {
    this.item.e = evt
    this.handler
      .emit(this.item)
  }
}
