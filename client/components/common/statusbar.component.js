'use strict'

import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, RouterLink } from '@angular/router'
import { NgIf, NgFor, NgClass } from '@angular/common'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { MomentPipe } from '../../pipes/moment.pipe'
import { StatusPipe } from '../../pipes/status.pipe'
import { HandlePipe } from '../../pipes/handle.pipe'
import { SvgIconComponent } from './common/svgicon.component'
import * as _findIndex from 'lodash/findIndex'

@Component({
  selector: 'status-bar',
  template: `
    <div [class]="collapsed ? 'collapsed status-container' : 'status-container'">
      <figure class="collapse" *ngIf="team" (click)="collapsed = !collapsed">
        {{ !collapsed ? '&#8676;' : '&#8677;' }}
      </figure>
      <div [class]="team ? 'status-bar' : 'no-team'" *ngIf="team; else noTeam">
        <h6 [style.display]="collapsed ? 'none' : 'block'">
          Team <a [routerLink]="['/D', 'team', team.id]">{{ team.name }}</a>
        </h6>
        <span [style.display]="collapsed ? 'none' : 'block'">
          Message your teammates by selecting one below
        </span>
        <input class="term-handle" type="text"
                *ngIf="team && team.teammates.length > 0"
                [style.display]="collapsed ? 'none' : 'block'"
                placeholder="Search your teammates"
                [(ngModel)]="termHandle">
        <ul>
          <li *ngFor="let teammate of team.teammates | status | handle:termHandle">
            <div class="teammate" *ngIf="currentUser.id != teammate.id"
                 (click)="handleDm(teammate)">
              <img [class]="teammate.active ? 'active' : 'inactive'"
                   [src]="teammate.avatar_url" />
              <div [ngClass]="collapsed ? 'info collapsed' : 'info'" [style.position]="collapsed ? 'fixed' : 'relative'">
                <p [class]="teammate.active ? 'active' : 'inactive'">
                  {{ teammate.handle }}
                </p>
                <span class="last-seen">
                  last seen: {{ teammate.active ? 'active now' : teammate.updated_at | moment }}
                </span>
              </div>
            </div>
          </li>
        </ul>
      </div>
      <ng-template #noTeam>
        <h5>
          Had you a team 💭
        </h5>
        <span>
          All your teammates would be reachable right here
        </span>
        <p class="instruction">
          Search for one to join or <a routerLink="/D/create/team">create one</a> to get started
        </p>
      </ng-template>
    </div>
  `
})
export class StatusbarComponent implements OnInit {
  @Input() currentUser;
  @Input() collapsed = false

  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.socket = socket
    this.event = event
  }
  ngOnInit () {
    this.termHandle = ''
    this.sub = this.route.data
      .subscribe(({ team }) => this.team = team)

    this.event.teamChanged$
      .subscribe(team => {
        this.team = team
        this.currentUser.team = team

        this.team.teammates
          .map(({ id }) => this.socket.unsync(`user:${id}:save`))

        this.addListeners()
      })

    this.socket.sync('team:save', (team) => {
      if (this.currentUser.team.id === team.id) {
        this.team = team
      }
    })

    if (this.team) {
      this.addListeners()
    }
  }
  ngOnDestroy () {
    this.socket.unsync('team:save')
  }
  addListeners () {
    this.team.teammates
      .forEach(({ id }) => {
        this.socket.sync(`user:${id}:save`, (user) => {
          const index = _findIndex(this.team.teammates, (t) => t.id === user.id)
          this.team.teammates[index] = user
          this.team.teammates = [ ...this.team.teammates ]
        })
      })
  }
  handleDm (teammate) {
    this.event.openThread(teammate)
  }
}
