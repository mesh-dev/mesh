'use strict'

import {Component, Input} from '@angular/core'
import {Router} from '@angular/router'

@Component({
  selector: 'back-button',
  template: `
    <button (click)="onBack()">&lt;</button>
  `
})
export class BackButtonComponent {
  constructor () {
  }
  onBack () {
    window.history.back()
  }
}
