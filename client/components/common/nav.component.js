'use strict'

import { Component, OnChanges, Input } from '@angular/core'
import { NgIf, NgFor } from '@angular/common'
import { SvgIconComponent } from 'svgicon.omponent'
import { SearchComponent } from '../search/search.component'
import { NotificationsComponent } from '../notifications/notifications.component'
import { SocketService } from '../../services/socket.service'

@Component({
  selector: 'navigation',
  template: `
    <nav>
      <ul class="navigation">
        <a class="logo" routerLink="/D" routerLinkActive="home">
          <svg-icon icon="/assets/icons/mesh-logo-v1.svg"></svg-icon>
        </a>
        <li *ngFor="let link of links">
          <a routerLink="{{ link.route }}" routerLinkActive="active">
              <svg-icon *ngIf="link.icon" [icon]="link.icon"></svg-icon>
             {{ link.name }}
          </a>
        </li>
      </ul>
      <div class="search-bar">
        <search></search>
      </div>
      <notifications></notifications>
      <div class="controls" *ngIf="currentUser">
        <div class="user">
          <a routerLink="/D/profile/{{ currentUser.handle }}">
            <img [src]="[currentUser.avatar_url]" />
          </a>
          <div class="rep">
            <svg-icon [icon]="'/assets/icons/flask-purp.svg'"></svg-icon>
            <span>{{ currentUser.reputation }}</span>
          </div>
        </div>
        <a class="idea" routerLink="/D/create/question">
          <svg-icon [icon]="'/assets/icons/plus79.svg'">
          </svg-icon>
        </a>
        <div class="more">
          <div class="three-dots">
            <figure class="dots">
            </figure>
          </div>
          <ul class="menu">
            <li>
              <a class="logout" href="/logout">
                <svg-icon [icon]="'/assets/icons/logout.svg'">
                </svg-icon>
                <p>Logout</p>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  `
})
export class NavComponent {
  @Input() currentUser
  @Input() links

  constructor(socket: SocketService) {
    this.socket = socket
  }
  ngOnInit() {
    this.socket.sync(`user:${this.currentUser.id}:save`, (user) => {
      if(this.currentUser.reputation !== user.reputation) {
        this.currentUser.reputation = user.reputation
      }
    })
  }
  ngOnChanges ({ links, currentUser }) {
    if (links && links.currentValue) {
      this.links = links.currentValue
    }
  }
}
