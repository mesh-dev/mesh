'use strict'

import {
  Component,
  OnInit,
  EventEmitter,
  Output,
  Input,
  OnChanges,
  ViewChild,
  ChangeDetectorRef
} from '@angular/core'
import { Router } from '@angular/router'
import { NgFor, NgIF } from '@angular/common'
import { StatusPipe } from '../../pipes/status.pipe'
import * as _ from 'lodash'

@Component({
  selector: 'auto-complete',
  template: `
    <div #list class="list" *ngIf="matchedCandidates.length > 0">
      <button class="candidate" *ngFor="let candidate of matchedCandidates | status: 'reverse'"
              (click)="onSelect(candidate)">
        <img [class]="candidate.active ? 'active' : 'inactive'"
             [src]="candidate.avatar_url" />
        <p>{{ candidate.handle }}</p>
      </button>
    </div>
  `
})
export class AutoCompleteComponent {
  @Input() trigger
  @Input() model
  @Input() candidates

  @Output() completion = new EventEmitter()

  @ViewChild('list') el

  constructor (cdr: ChangeDetectorRef) {
    this.cdr = cdr
  }
  ngOnInit () {
    this.matchedCandidates = []
  }
  ngOnChanges ({ model }) {
    if (model.currentValue !== undefined) {
      this.handleTrigger(model.currentValue)
    }
  }
  handleTrigger (model) {
    model = model || ''

    const pattern = new RegExp(`\\${this.trigger}\\w*$`, 'g')
    const matches = model.match(pattern)

    if (matches) {
      this.onMatch(matches[0])
    } else {
      this.matchedCandidates = []
    }
  }
  onMatch (match) {
    this.matchedCandidates = [...this.candidates]

    const matchChars = match
      .replace(this.trigger, '')
      .split('')

    _.forEach(this.candidates, (candidate) => {
      if (matchChars.join('') === candidate.handle) {
        return this.matchedCandidates = []
      }

      let candidateChars = candidate.handle.split('')
      if (_.isMatch(candidateChars, matchChars) &&
        !_.includes(this.matchedCandidates, candidate)) {
        this.matchedCandidates.push(candidate)
      } else if (!_.isMatch(candidateChars, matchChars)) {
        _.pull(this.matchedCandidates, candidate)
      }
    })

    if (this.matchedCandidates.length > 0) {
      this.cdr.detectChanges()
      this.el.nativeElement.scrollTop = this.el.nativeElement.scrollHeight
    }
  }
  onSelect (completion) {
    this.completion.emit(completion)
  }
}
