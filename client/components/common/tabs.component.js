'use strict'

import {Component, Input} from '@angular/core'
import {ROUTER_DIRECTIVES} from '@angular/router'

@Component({
  selector: 'tabs',
  directives: [ROUTER_DIRECTIVES],
  template: `
    <ul>
      <li *ngFor="let link of links">
        <a routerLink="{{ link.toLowerCase() }}">{{ link }}</a>
      </li>
    </ul>
  `
})
export class TabsComponent {
  @Input() links;

  constructor () {
  }
}
