import { Component, ElementRef } from '@angular/core'

@Component({
  selector: 'logo',
  template: `
    <svg-icon style="display: none" #svgEl icon="/assets/icons/mesh-logo-v1.svg"></svg-icon>
    <canvas #canvasEl width="120" height="120"></canvas>
  `
})
export class LogoComponent {
  // line drawing constants
  dashLen = 100
  speed = 5 * 1

  constructor (el: ElementRef) { this.el = el }

  ngAfterViewInit () {
    const polylines = this.el.nativeElement.querySelectorAll('polyline')
    const polygons = this.el.nativeElement.querySelectorAll('polygon')

    this.canvasEl = this.el.nativeElement.querySelector('canvas')
    this.ctx = this.canvasEl.getContext('2d')
    this.ctx.clearRect(0, 0, 120, 120)
    this.ctx.lineCap = 'square'
    this.ctx.lineJoin = 'bevel'
    this.ctx.strokeStyle='#FFFCF9'
    this.ctx.lineWidth = 1

    this.lines = this.parseAttributes(polylines)
    this.polygons = this.parseAttributes(polygons)

    this.lines.forEach(pts => this._drawLines(pts))
    this.polygons.forEach(pts => this._drawLines(pts))
  }
  _drawLines = (pts, offset = this.dashLen, index = 0) => {
    if(!pts[index]) {
      return
    }

    this.ctx.setLineDash([this.dashLen - offset, offset - this.speed])
    this.ctx.moveTo(pts[0].x, pts[0].y)
    this.ctx.lineTo(pts[index].x, pts[index].y)
    this.ctx.stroke()

    offset -= this.speed

    if(offset > 0) {
      requestAnimationFrame(() => this._drawLines(pts, offset, index))
    } else  {
      this.ctx.moveTo(pts[0].x, pts[0].y)
      this.ctx.lineTo(pts[index].x, pts[index].y)
      this.ctx.stroke()

      index++

      this.ctx.setTransform(1, 0, 0, 1, 0, 3 * Math.random())
      this.ctx.rotate(Math.random() * 0.005)
      offset = this.dashLen
      requestAnimationFrame(() => this._drawLines(pts, offset, index))
    }
  }
  parseAttributes (elems) {
    const cords = []
    for (let elem of elems) {
      const points = elem.getAttribute('points').split(' ')
      const axis = points
        .map(p => {
          const xAndY = p.split(',')
          return { x: Math.round(xAndY[0] / 10), y: Math.round(xAndY[1] / 10) }
        })

      cords.push(axis)
    }

    return cords
  }
}
