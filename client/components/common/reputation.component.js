'use strict'

import { Component, Input, Output, EventEmitter, OnInit, OnDestroy } from '@angular/core'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'

@Component({
  selector: 'reputation',
  template: `
    <div class="up-vote">
      <button [class]="upVoted ? 'up-voted' : ''"
              (click)="handleUpvote()"
              [disabled]="upVoted">
        &#x25B2;
      </button>
    </div>
    <div class="reputation">
      <p>{{ rep.reputation }}</p>
    </div>
    <div class="down-vote">
      <button [class]="downVoted ? 'down-voted' : ''"
              (click)="handleDownvote()"
              [disabled]="downVoted || rep.reputation === 1">
        &#x25BC;
      </button>
    </div>
  `
})
export class ReputationComponent implements OnInit {
  @Input() currentUser;
  @Input() rep;

  @Output() newRep = new EventEmitter();

  constructor (api: ApiService, socket: SocketService) {
    this.api = api
    this.socket = socket
  }
  ngOnInit () {
    for (var voter of this.rep.upvoters) {
      if (voter.id === this.currentUser.id) {
        this.upVoted = true
      }
    }
    for (var voter of this.rep.downvoters) {
      if (voter.id === this.currentUser.id) {
        this.downVoted = true
      }
    }

    this.socket.sync('rep:save', (rep) => {
      if (this.rep.id === rep.id) {
        this.rep = rep
        this.newRep.emit(rep)
      }
    })
  }
  ngOnDestroy () {
    this.socket.unsync('rep:save')
  }
  handleUpvote () {
    const newRep = { reputation: this.rep.reputation + 5 }
    this.api.update(`reps/${this.rep.id}`, newRep)
      .then(rep => {
        this.rep = rep
        this.newRep.emit(rep)
        this.upVoted = true
      })
      .catch(err => console.log(err))
  }
  handleDownvote () {
    const newRep = { reputation: this.rep.reputation <= 1 ? 1 : this.rep.reputation - 2 }
    this.api.update(`reps/${this.rep.id}`, newRep)
      .then(rep => {
        this.rep = rep
        this.newRep.emit(rep)
        this.downVoted = true
      })
      .catch(err => console.log(err))
  }
}
