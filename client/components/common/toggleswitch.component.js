'use strict'

import {Component, Input} from '@angular/core'

@Component({
  selector: 'toggle-switch',
  template: `
    <div class="toggle-switch">
      <input type="checkbox"
             name="toggle" class="toggle"
             [checked]="state" disabled="lock" />
      <label class="switch" for="toggle"></label>
    </div>
  `
})
export class ToggleSwitchComponent {
  @Input() state;
  @Input() lock;

  constructor () {
  }
}
