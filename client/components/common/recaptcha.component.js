'use strict'

import { Component, OnInit, OnDestroy, Output, EventEmitter, NgZone } from '@angular/core'

@Component({
  selector: 'recaptcha',
  template: `
    <div class="g-recaptcha"
         [attr.data-sitekey]="'6LcVQAcUAAAAAB3LWG7SpMoGVEKQOdsHc1wmctu0'"
         data-callback="onVerify">
    </div>
  `
})
export class RecaptchaComponent {
  @Output() captchaResponse = new EventEmitter();

  constructor (zone: NgZone) {
    window['onVerify'] = response => zone.run(this.recaptchaCallback.bind(this, response))
  }
  ngOnInit () {
    var doc = document.body
    var script = document.createElement('script')
    script.innerHTML = ''
    script.src = 'https://www.google.com/recaptcha/api.js'
    script.async = true
    script.defer = true
    doc.appendChild(script)
  }
  ngOnDestroy () {
    var doc = document.body
    var script = doc.lastChild
    doc.removeChild(script)
  }
  recaptchaCallback (response) {
    if (!response) return

    this.captchaResponse.emit(response)
  }
}
