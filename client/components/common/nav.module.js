import { NgModule } from '@angular/core'
import { SharedModule } from './shared.module'
import { NotificationsModule } from '../notifications/notifications.module'
import { NavComponent } from './nav.component'
import { LogoComponent } from './logo.component'
import { SearchComponent } from '../search/search.component'
import { SearchResultsComponent } from '../search/results.component'

@NgModule({
  imports: [
    SharedModule,
    NotificationsModule
  ],
  declarations: [
    NavComponent,
    LogoComponent,
    SearchComponent,
    SearchResultsComponent
  ],
  exports: [
    NavComponent,
    LogoComponent
  ]
})
export class NavModule { }
