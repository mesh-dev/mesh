'use strict'

import { Component, Input, OnInit, OnDestroy } from '@angular/core'
import { SocketService } from '../../services/socket.service'
import { MomentPipe } from '../../pipes/moment.pipe'

@Component({
  selector: 'owner',
  template: `
    <div class="owner">
      <img [class]="item.owner.active ? 'active' : ''" [src]="item.owner.avatar_url" />
      <div class="info">
        <p><a routerLink="/D/profile/{{ item.owner.handle }}">{{ item.owner.handle }}</a></p>
        <span>{{ item.created_at | moment }}</span>
      </div>
    </div>
  `
})
export class OwnerComponent {
  @Input() item;

  constructor (socket: SocketService) {
    this.socket = socket
  }
  ngOnInit () {
    this.socket.sync('user:save', (user) => {
      if (this.item.owner.id === user.id) {
        this.item.owner = user
      }
    })
  }
  ngOnDestroy () {
    this.socket.unsync('user:save')
  }
}
