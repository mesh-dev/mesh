import { DomSanitizer } from '@angular/platform-browser'
import { Component, Input, OnInit, OnChanges } from '@angular/core'
import { Http, Headers, RequestOptions } from '@angular/http'
import { NgIf } from '@angular/common'
import { Observable } from 'rxjs/Observable'

import 'rxjs/add/observable/of'
import 'rxjs/add/operator/do'
import 'rxjs/add/operator/map'

@Component({
  selector: 'svg-icon',
  template: `
    <div class="icon" [innerHtml]="sanitizer.bypassSecurityTrustHtml(svg)"></div>
  `
})
export class SvgIconComponent {
  @Input() icon;

  constructor (sanitizer: DomSanitizer, http: Http) {
    this.sanitizer = sanitizer
    this.http = http
    this.svg = ''

    let headers = new Headers({ 'Content-Type': 'image/svg+xml' })
    this.options = new RequestOptions({ headers: headers })
  }
  ngOnInit () {
    this.fetchSvg()
      .subscribe(svg => this.svg = svg)
  }
  ngOnChanges () {
    this.fetchSvg()
      .subscribe(svg => this.svg = svg)
  }
  fetchSvg () {
    let svgCache = window.localStorage.getItem(this.icon)
    if (svgCache) {
      return Observable.of(svgCache)
    } else {
      return this.http.get(this.icon, this.options)
        .do(res => {
          window.localStorage.setItem(this.icon, res._body)
        }).map(res => res._body)
    }
  }
}
