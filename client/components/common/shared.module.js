import { NgModule } from '@angular/core'
import { FormsModule, ReactiveFormsModule } from '@angular/forms'
import { CommonModule } from '@angular/common'
import { RouterModule } from '@angular/router'
import { InfiniteScrollModule } from 'ngx-infinite-scroll'
import { DirectiveModule } from '../../directives/directive.module'
import { PipeModule } from '../../pipes/pipe.module'
import { SvgIconComponent } from './svgicon.component'
import { ToggleSwitchComponent } from './toggleswitch.component'
import { RecaptchaComponent } from './recaptcha.component'
import { AutoCompleteComponent } from './autocomplete.component'
import { StatusbarComponent } from './statusbar.component'
import { UploadComponent } from './upload.component'
import { EditFieldComponent } from './editfield.component'
import { OwnerComponent } from './owner.component'
import { ReputationComponent } from './reputation.component'
import { OAuthComponent } from '../account/oauth.component'

@NgModule({
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    PipeModule,
    DirectiveModule,
    InfiniteScrollModule
  ],
  declarations: [
    SvgIconComponent,
    ToggleSwitchComponent,
    RecaptchaComponent,
    StatusbarComponent,
    UploadComponent,
    EditFieldComponent,
    OwnerComponent,
    ReputationComponent,
    AutoCompleteComponent,
    OAuthComponent
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule,
    PipeModule,
    InfiniteScrollModule,
    DirectiveModule,
    SvgIconComponent,
    ToggleSwitchComponent,
    RecaptchaComponent,
    StatusbarComponent,
    UploadComponent,
    EditFieldComponent,
    OwnerComponent,
    ReputationComponent,
    AutoCompleteComponent,
    OAuthComponent
  ]
})
export class SharedModule { }
