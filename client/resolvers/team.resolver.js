import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'
import { Observable } from 'rxjs/Rx'

import { ApiService } from '../services/api.service'

@Injectable()
export class TeamResolver implements Resolve {
  constructor (api: ApiService) {
    this.api = api
  }
  resolve (route, state) {
    if (state.url === `/D/team/${route.params.id}`) {
      return this.api.fetch(`teams/${route.params.id}`)
    }

    return this.api.fetch('teams/me')
  }
}
