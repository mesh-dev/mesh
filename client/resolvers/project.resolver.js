import { Injectable } from '@angular/core'
import { Resolve } from '@angular/router'
import { Observable } from 'rxjs/Rx'
import { ApiService } from '../services/api.service'

@Injectable()
export class ProjectResolver implements Resolve {
  constructor (api: ApiService) {
    this.api = api
  }
  resolve (route, state) {
    return this.api.fetch(`projects/${route.params.id}`)
  }
}
