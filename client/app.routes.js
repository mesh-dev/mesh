import { NgModule } from '@angular/core'
import { RouterModule, Routes } from '@angular/router'
import { LandingModule } from './pages/landing/landing.module'
import { LoginModule } from './pages/login/login.module'
import { DashboardModule } from './pages/dashboard/dashboard.module'
import { ProjectModule } from './pages/project/project.module'
import { QuestionModule } from './pages/question/question.module'
import { StartModule } from './pages/start/start.module'
import { ErrorModule } from './pages/error/error.module'

// Main pages
import { LandingPage } from './pages/landing/landing.page'
import { LoginPage } from './pages/login/login.page'
import { DashboardPage } from './pages/dashboard/dashboard.page'
import { QuestionPage } from './pages/question/question.page'
import { ProjectPage } from './pages/project/project.page'
import { ErrorPage } from './pages/error/error.page'

// Child Routes
import { dashboardPageRoutes } from './routes/dashboard.routes'
import { projectPageRoutes } from './routes/project.routes'

// Resolvers
import { CurrentUserResolver } from './resolvers/currentuser.resolver'
import { UserResolver } from './resolvers/user.resolver'
import { RequestsResolver } from './resolvers/requests.resolver'
import { TeamResolver } from './resolvers/team.resolver'
import { ProjectResolver } from './resolvers/project.resolver'
import { QuestionResolver } from './resolvers/question.resolver'
import { MentionsResolver } from './resolvers/mentions.resolver'
import { DirectMessagesResolver } from './resolvers/dm.resolver'


export const routes = [
  {
    path: '',
    component: LandingPage
  },
  {
    path: 'login',
    component: LoginPage
  },
  {
    path: 'Q/:id',
    component: QuestionPage,
    resolve: {
      currentUser: CurrentUserResolver,
      team: TeamResolver,
      question: QuestionResolver,
      messages: DirectMessagesResolver,
      mentions: MentionsResolver,
      request: RequestsResolver
    }
  },
  {
    path: 'P/:id',
    component: ProjectPage,
    resolve: {
      currentUser: CurrentUserResolver,
      project: ProjectResolver,
      team: TeamResolver,
      messages: DirectMessagesResolver,
      mentions: MentionsResolver,
      request: RequestsResolver
    }
  },
  {
    path: 'D',
    pathMatch: 'prefix',
    component: DashboardPage,
    children: [...dashboardPageRoutes],
    resolve: {
      currentUser: CurrentUserResolver,
      team: TeamResolver,
      requests: RequestsResolver,
      mentions: MentionsResolver,
      messages: DirectMessagesResolver
    }
  },
  {
    path: '**',
    component: ErrorPage
  }
]

@NgModule({
  imports: [
    RouterModule.forRoot(routes),
    LandingModule,
    LoginModule,
    DashboardModule,
    ProjectModule,
    QuestionModule,
    StartModule,
    ErrorModule
  ],
  providers: [
    ProjectResolver,
    CurrentUserResolver,
    TeamResolver,
    RequestsResolver,
    MentionsResolver,
    DirectMessagesResolver,
    UserResolver,
    QuestionResolver
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
