import { Injectable, EventEmitter } from '@angular/core'
import { SocketService } from './socket.service'
import * from 'webrtc-adapter'

@Injectable()
export class WebRTCService {
  peerStream = new EventEmitter();
  peerHangup = new EventEmitter();

  constructor (socket: SocketService) {
    this.socket = socket

    this.peerConnections = {}
    this.servers = {'iceServers': [
      {'urls': 'stun:stun.l.google.com:19302'},
      {
        urls: 'turn:numb.viagenie.ca',
        credential: 'muazkh',
        username: 'webrtc@live.com'
      },
      {
        urls: 'turn:192.158.29.39:3478?transport=udp',
        credential: 'JZEOEt2V3Qb0y27GRntt2u2PAYA=',
        username: '28224511:1379330808'
      }
    ]}
  }
  init (user, project, stream) {
    this.currentUser = user
    this.project = project
    this.stream = stream

    this.socket.sync('call:join', (data) => {
      if (data.initiator) {
        this.makeOffer(data.peer)
      }
    })
    this.socket.sync('call:leave', (data) => {
      this.handleHangup(data.peer)
    })
    this.socket.sync('call:signal', (call) => {
      this.handleCall(call)
    })

    this.socket.emit('call:join', {
      project: project
    })
  }
  getPeerConnection (peer) {
    if (this.peerConnections[peer.id]) {
      return this.peerConnections[peer.id]
    }

    let pc = new RTCPeerConnection(this.servers)
    this.peerConnections[peer.id] = pc

    pc.addStream(this.stream)

    pc.onicecandidate = (evt) => {
      if (evt.candidate) {
        this.socket.emit('call:signal', {
          by: this.currentUser,
          to: peer,
          ice: evt.candidate,
          type: 'ice'
        })
      }
    }
    pc.onaddstream = (evt) => {
      if (evt.stream != null) {
        this.peerStream.emit({ stream: evt.stream, peer: peer })
      }
    }
    pc.ontrack = (evt) => {
      if (evt.stream != null) {
        this.peerStream.emit({ stream: evt.stream, peer: peer })
      }
    }

    return pc
  }
  makeOffer (peer) {
    var pc = this.getPeerConnection(peer)

    pc.createOffer(sdp => {
      pc.setLocalDescription(sdp, () => {
        this.socket.emit('call:signal', {
          by: this.currentUser,
          to: peer,
          sdp: sdp,
          type: 'sdp-offer'
        })
      }, (err) => this.handleErrors(err))
    }, (err) => this.handleErrors(err))
  }
  handleCall (call) {
    var pc = this.getPeerConnection(call.by)

    switch (call.type) {
      case 'sdp-offer':
        pc.setRemoteDescription(new RTCSessionDescription(call.sdp), () => {
          console.log('Setting remote description by offer')
          pc.createAnswer(sdp => {
            pc.setLocalDescription(sdp, () => {
              this.socket.emit('call:signal', {
                by: call.to,
                to: call.by,
                sdp: sdp,
                type: 'sdp-answer'
              })
            }, (err) => this.handleErrors(err))
          }, (err) => this.handleErrors(err))
        }, (err) => this.handleErrors(err))
        break
      case 'sdp-answer':
        pc.setRemoteDescription(new RTCSessionDescription(call.sdp), () => {
          console.log('Setting remote description by answer')
        }, (err) => this.handleErrors(err))
        break
      case 'ice':
        if (call.ice) {
          console.log('Adding ice candidates')
          pc.addIceCandidate(new RTCIceCandidate(call.ice))
        }
        break
    }
  }
  hangup () {
    this.peerConnections = {}
    this.socket.emit('call:leave', {
      project: this.project
    })
    this.socket.unsync('call:join')
    this.socket.unsync('call:leave')
    this.socket.unsync('call:signal')
  }
  handleHangup (peer) {
    this.peerHangup.emit(peer)
    let pc = this.getPeerConnection(peer)
    pc.close()
    this.peerConnections[peer.id] = null
  }
  handleErrors (err) {
    console.log(err)
  }
}
