import { Injectable } from '@angular/core'
import { Router } from '@angular/router'
import { ApiService } from './api.service'

@Injectable()
export class MentionService {
  constructor (router: Router, api: ApiService) {
    this.router = router
    this.api = api
  }
  findMentions (currentUser, text) {
    this.currentUser = currentUser
    this.text = text

    let pattern = /\@(\w*)/g
    let mentioned = text.match(pattern)
    if (mentioned) {
      let handles = []
      for (let handle of mentioned) {
        handles.push(handle.split('@')[1])
      }
      this.sendMentions(handles)
    }
  }
  fetchMentioned (handle) {
    return this.api.fetch(`users/${handle}`)
  }
  sendMentions (mentioned) {
    for (let handle of mentioned) {
      this.fetchMentioned(handle)
        .subscribe(user => {
          let mention = {
            sender_id: this.currentUser.id,
            mentioned_id: user.id,
            link: this.router.url,
            text: this.text
          }
          this.api.create('mentions', mention)
            .catch(err => console.log(err))
        })
    }
  }
}
