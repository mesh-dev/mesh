import { Injectable } from '@angular/core'
import { Subject } from 'rxjs/Subject'

@Injectable()
export class EventService {
  _changeAvatarSource = new Subject();
  _changeTeamSource = new Subject();
  _acceptRequestSource = new Subject();
  _openThreadSource = new Subject();
  _closeThreadSource = new Subject();

  avatarChanged$ = this._changeAvatarSource.asObservable();
  teamChanged$ = this._changeTeamSource.asObservable();
  requestAccepted$ = this._acceptRequestSource.asObservable();
  threadOpened$ = this._openThreadSource.asObservable();
  threadClosed$ = this._closeThreadSource.asObservable();

  acceptRequest (request) {
    this._acceptRequestSource.next(request)
  }
  changeTeam (team) {
    this._changeTeamSource.next(team)
  }
  closeThread (user) {
    this._closeThreadSource.next(user)
  }
  changeAvatar (avatar) {
    this._changeAvatarSource.next(avatar)
  }
  openThread (user) {
    this._openThreadSource.next(user)
  }
}
