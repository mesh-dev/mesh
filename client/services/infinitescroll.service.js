'use strict'

import { Inject } from '@angular/core'
import { ApiService } from './api.service'
import { Observable } from 'rxjs/Observable'

export class InfiniteScrollService {
  constructor (@Inject(ApiService) api: ApiService) { /* eslint-disable */
    this.api = api
  }

  loadFirstPage (endpoint) {
    this.endpoint = endpoint
    this.busy = false
    this.page = 1

    return this.api
      .fetch(`${this.endpoint}?page=${this.page}`)
      .do(res => this.pages = res.pages)
      .map(res => res.docs)
  }

  fetchNextPage () {
    this.page++
    if (this.page <= this.pages) {
      this.busy = true

      return this.api
        .fetch(`${this.endpoint}?page=${this.page}`)
        .map(res => res.docs)
    } else {
      return Observable.of(null)
    }
  }
}
