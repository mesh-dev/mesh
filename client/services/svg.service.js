'use strict'

import { Injectable } from '@angular/core'
import { Http } from '@angular/http'
import { Observable } from 'rxjs/Observable'

@Injectable()
export class SvgService {
  constructor (http: Http) {
    this.http = http
  }

  handleSvgCache (path) {
    if (false) {
      return Observable.of(this.svgCache[path])
    } else {
      return this.http.get(path)
    }
  }
}
