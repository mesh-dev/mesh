import { NgModule } from '@angular/core'
import { SwipeEventDirective } from './swipe.directive'
import { MasonryDirective } from './masonry.directive'
import { SortableDirective } from './sortable.directive'

@NgModule({
  declarations: [
    SwipeEventDirective,
    MasonryDirective,
    SortableDirective
  ],
  exports: [
    SwipeEventDirective,
    MasonryDirective,
    SortableDirective
  ]
})
export class DirectiveModule { }
