'use strict'

import { Input, Directive, ElementRef, AfterViewChecked, AfterViewInit } from '@angular/core'
import * as Masonry from 'masonry-layout'
import * as imagesLoaded from 'imagesloaded'

@Directive({
  selector: '[masonry]'
})
export class MasonryDirective {
  constructor (el: ElementRef) {
    this.el = el
  }
  ngAfterViewChecked () {
    this.grid = new Masonry(this.el.nativeElement, {
      itemSelector: '.item',
      percentPosition: true
    })
  }
  ngAfterViewInit () {
    const imgLoad = imagesLoaded(this.el.nativeElement)
    imgLoad.on('done', (instance) => this.grid.layout())
  }
}
