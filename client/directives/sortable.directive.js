'use strict'

import { Directive, Input, Output, EventEmitter, ElementRef, OnInit } from '@angular/core'
import { EventService } from '../services/event.service'
import * as Sortable from 'sortablejs'

@Directive({
  selector: '[sortable]'
})
export class SortableDirective {
  @Input() group;

  @Output() drop = new EventEmitter();
  @Output() add = new EventEmitter();

  constructor (event: EventService, el: ElementRef) {
    this.elem = el.nativeElement
    this.event = event
  }
  ngOnInit () {
    const sortable = Sortable.create(this.elem, {
      group: this.group,
      animation: 100,
      filter: '.new',
      chosenClass: 'chosen',
      ghostClass: 'ghost',
      onEnd: (evt) => {
        this.drop.emit(evt)
      },
      onAdd: (evt) => {
        this.add.emit(evt)
      }
    })
  }
}
