'use strict'

import { Directive, EventEmitter, Output } from '@angular/core'
import { EventService } from '../services/event.service'

@Directive({
  selector: '[swipe]',
  host: {
    '(touchstart)': 'handleTouchStart($event)',
    '(touchend)': 'handleTouchEnd($event)'
  }
})
export class SwipeEventDirective {
  @Output() touch = new EventEmitter();

  constructor (event: EventService) {
    this.event = event
  }
  ngOnInit () {
    this.threshold = 150
    this.allowedTime = 200

    this.event.threadOpened$.subscribe(user => {
      this.touch.emit({ right: false, left: true })
    })
  }
  handleTouchStart (e) {
    const touched = e.changedTouches[0]

    this.dist = 0
    this.startX = touched.pageX
    this.startY = touched.pageY
    this.startTime = new Date().getTime()
  }
  handleTouchEnd (e) {
    const touched = e.changedTouches[0]

    this.dist = touched.pageX - this.startX
    this.elapsedTime = new Date().getTime() - this.startTime

    let isSwipeRight = touched.pageX > this.startX && this.dist > this.threshold && this.elapsedTime > this.allowedTime
    let isSwipeLeft = this.startX > touched.pageX && this.dist < this.threshold && this.elapsedTime > this.allowedTime

    if (isSwipeRight || isSwipeLeft) {
      this.touch.emit({ right: isSwipeRight, left: isSwipeLeft })
    }
  }
}
