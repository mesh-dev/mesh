import { Injectable } from '@angular/core'
import { CanActivate } from '@angular/router'
import { Observable } from 'rxjs/Rx'
import { ApiService } from '../services/api.service'

@Injectable()
export class ProjectGuard implements CanActivate {
  constructor (api: ApiService) {
    this.api = api
  }
  canActivate (route, state) {
    return Observable
      .merge(this.api.fetchProject(route.params.id), Observable.of(true))
      .filter(x => x === true)
  }
}
