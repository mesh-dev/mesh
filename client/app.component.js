import { Component } from '@angular/core'
import * as reset from 'eric-meyer-reset.scss/_reset.scss'
import * as highlights from 'highlight.js/styles/github.css'
import * as markdown from 'github-markdown-css/github-markdown.css'
import * as styles from './styles/app.scss'

@Component({
  selector: 'app',
  styles: [`
    ${reset}
    ${markdown}
    ${highlights}
    ${styles}
  `],
  template: `
    <ng-progress [showSpinner]="false" color="#2196F3"
                 direction="rightToLeftIncreased"
                 positionUsing="translate">
    </ng-progress>
    <router-outlet></router-outlet>
  `
})
export class AppComponent {}
