'use strict'

import { Component } from '@angular/core'
import * as template from './error.page.html'

@Component({
  selector: 'error-page',
  template: `
    ${template}
  `
})
export class ErrorPage {}