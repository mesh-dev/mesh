import { NgModule } from '@angular/core'
import { ErrorPage } from './error.page'

@NgModule({
  declarations: [
    ErrorPage
  ],
  exports: [
    ErrorPage
  ]
})
export class ErrorModule { }