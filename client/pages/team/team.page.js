'use strict'

import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor, NgIf, NgStyle, SlicePipe } from '@angular/common'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { Observable } from 'rxjs/Observable'
import 'rxjs/add/observable/zip'
import * as template from './team.page.html'

@Component({
  selector: 'team-page',
  template: `
    ${template}
  `
})
export class TeamPage {
  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.event = event
    this.socket = socket
  }
  ngOnInit () {
    this.numShowing = 5

    const { parent } = this.route
    this.sub = Observable.zip(this.route.data, parent.data)
      .subscribe(([ child, parent ]) => {
        this.currentUser = parent.currentUser
        this.team = child.team
      })

    this.socket.sync('request:approved', (request) => {
      this.team = request.team
    })
    this.socket.sync('team:save', (team) => {
      if (team.id === this.team.id) {
        this.team = team
      }
    })
    this.socket.sync('user:save', (user) => {
      if (!this.team.teammates) return

      this.team.teammates.map((teammate, i) => {
        if (teammate.id === user.id) {
          return this.team.teammates[i] = user
        }
      })
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  shouldHide (toCompare) {
    return typeof toCompare === 'undefined'
  }
  onShowMore () {
    if (this.numShowing === 5) {
      this.numShowing = this.team.teammates.length
    } else {
      this.numShowing = 5
    }
  }
}
