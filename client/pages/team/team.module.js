import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { TeamPage } from './team.page'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    TeamPage
  ],
  exports: [
    TeamPage
  ]
})
export class TeamModule { }
