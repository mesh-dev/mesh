import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { NavModule } from '../../components/common/nav.module'
import { MessengerModule } from '../../components/messages/messenger.module'
import { ProfileModule } from '../profile/profile.module'
import { TeamModule } from '../team/team.module'
import { LearningModule } from '../learning/learning.module'
import { BuildingModule } from '../building/building.module'
import { CreateModule } from '../../components/create/create.module'
import { DashboardPage } from './dashboard.page'
import { CreatePage } from '../create/create.page'

@NgModule({
  imports: [
    SharedModule,
    ProfileModule,
    TeamModule,
    MessengerModule,
    LearningModule,
    BuildingModule,
    NavModule,
    CreateModule
  ],
  declarations: [
    DashboardPage,
    CreatePage
  ],
  exports: [
    DashboardPage,
    CreatePage
  ]
})
export class DashboardModule { }
