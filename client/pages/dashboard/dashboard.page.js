'use strict'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'

@Component({
  template: `
    <navigation [currentUser]="currentUser"
                [links]="links">
    </navigation>
    <div [class]="swipe.right ? 'tray' : 'container'"
         swipe (touch)="swipe = $event">
      <status-bar [currentUser]="currentUser">
      </status-bar>
      <router-outlet class="child-views"></router-outlet>
      <direct-messenger *ngIf="openThreads.length > 0"
                       [currentUser]="currentUser"
                       [users]="openThreads">
      </direct-messenger>
    </div>
  `
})
export class DashboardPage {
  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.event = event
    this.route = route
    this.socket = socket
  }
  ngOnInit () {
    this.swipe = {}

    this.subscription = this.route.data
      .subscribe(({ currentUser, team }) => {
        this.currentUser = currentUser
        this.team = team
      })

    this.openThreads = []
    this.links = [
      { route: '/D/learning', name: 'Learning', icon: '/assets/icons/speech-bubble20.svg' },
      { route: '/D/building', name: 'Building', icon: '/assets/icons/notebook91.svg' }
    ]

    this.event.avatarChanged$.subscribe(avatarUrl => {
      this.currentUser.avatar_url = avatarUrl
    })
    this.event.teamChanged$.subscribe(team => {
      this.currentUser.team_id = team.id
    })
    this.event.threadClosed$.subscribe(user => {
      _.remove(this.openThreads, (o) => o.id === user.id)
    })
    this.event.threadOpened$.subscribe(user => {
      if (_.some(this.openThreads, { id: user.id })) {
        return _.remove(this.openThreads, (o) => o.id === user.id)
      } else if (this.openThreads.length >= 4) {
        this.openThreads.shift()
        this.openThreads.push(user)
      } else {
        this.openThreads.push(user)
      }
    })

    this.socket.sync('request:approved', (request) => {
      this.socket.join(`team:${request.team.id}`)
      this.event.changeTeam(request.team)
    })
  }
  ngOnDestroy () {
    this.socket.unsyncAll()
    this.subscription.unsubscribe()
  }
}
