'use strict'

import {Component} from '@angular/core'

@Component({
  selector: 'settingsPage',
  template: `
    ${require('./settingsPage.html')}
  `
})
export class SettingsPage {
  constructor () {
  }
}
