'use strict'

import { Component, OnInit } from '@angular/core'
import { Router } from '@angular/router'
import { NgIf } from '@angular/common'
import * as template from './create.page.html'

@Component({
  selector: 'create-page',
  template: `
    ${template}
  `
})
export class CreatePage {
  constructor (router: Router) {
    this.router = router
  }
  ngOnInit () {
    this.creating = {
      team: false,
      learn: true,
      build: false
    }
  }
  handleCreate (type) {
    switch (type) {
      case 'team' :
        this.creating.team = true
        this.creating.learn = false
        this.creating.build = false

        this.router.navigate(['D/create'])
        break
      case 'learn' :
        this.creating.team = false
        this.creating.learn = true
        this.creating.build = false

        this.router.navigate(['D/create/learn'])
        break
      case 'build' :
        this.creating.team = false
        this.creating.learn = false
        this.creating.build = true

        this.router.navigate(['D/create/build'])
        break
    }
  }
}
