import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { NavModule } from '../../components/common/nav.module'
import { MessengerModule } from '../../components/messages/messenger.module'
import { WebRTCService } from '../../services/webrtc.service'
import { MentionService } from '../../services/mention.service'
import { VoiceChatComponent } from '../../components/project/voice.component'
import { PresentUsersComponent } from '../../components/project/presences.component'
import { ProjectPage } from './project.page'

@NgModule({
  imports: [
    SharedModule,
    MessengerModule,
    NavModule
  ],
  providers: [ WebRTCService, MentionService ],
  declarations: [
    ProjectPage,
    VoiceChatComponent,
    PresentUsersComponent,
  ],
  exports: [
    ProjectPage
  ]
})
export class ProjectModule { }
