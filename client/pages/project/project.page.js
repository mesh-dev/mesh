'use strict'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor, NgIf, NgStyle } from '@angular/common'
import { AuthService } from '../../services/auth.service'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import * as template from './project.page.html'

@Component({
  selector: 'project-page',
  template: `
    ${template}
  `
})
export class ProjectPage {
  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.event = event
    this.socket = socket
  }
  ngOnInit () {
    this.openThreads = []

    this.sub = this.route.data.subscribe(data => {
      this.currentUser = data.currentUser
      this.project = data.project
      this.team = data.team
    })

    this.links = []

    this.socket.join(`project:${this.project.id}`)

    this.event.threadClosed$.subscribe(user => {
      _.remove(this.openThreads, (o) => o.id === user.id)
    })
    this.event.threadOpened$.subscribe(user => {
      if (_.some(this.openThreads, { id: user.id })) {
        return _.remove(this.openThreads, (o) => o.id === user.id)
      } else if (this.openThreads.length >= 4) {
        this.openThreads.shift()
        this.openThreads.push(user)
      } else {
        this.openThreads.push(user)
      }
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
    this.socket.leave(`project:${this.project.id}`)
  }
}
