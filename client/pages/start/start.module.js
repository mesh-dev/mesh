import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { StartPage } from './start.page'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    StartPage
  ],
  exports: [
    StartPage
  ]
})
export class StartModule { }
