import { Component } from '@angular/core'
import { RouterLink } from '@angular/router'
import * as template from './start.page.html'

@Component({
  selector: 'start-page',
  template: `
    ${template}
  `
})
export class StartPage {
  overview = `## What is Mesh? \n **Mesh is a social collaboration platform.** A place to build and learn with others that you know, or haven't met yet. \n ### What does that mean? \n Mesh aims to put three different tools at your disposal real-time communication, task management, and q&a forum. In combining these features with a social aspect, you can expand  your circle of collaborators further than ever before. \n ### Who is it for? \n Mesh is for anyone that is over vanity likes and comments to help pass the time. Someone that wants to replace them with actually getting something done.`
  goals = `## What We Are Trying to Accomplish. \n **In many ways social networks have failed us.** They could be so much more than fleeting entertainment to pass the time. With Mesh, we are looking to offer a platform that provides incentive to actively better yourself through learning and doing in a social atmosphere. \n ### How are we trying to do this? \n By combining or "meshing" together certain aspects of great collaboration tools and integrating them in a social network. \n ### Why would I want to use mesh? \n Access to people in the same pursuit of knowledge as you can be difficult, but the benefits great. Mesh is going to provide that access and give you the tools to harness each other's knowledge in attaining your goals.`
}
