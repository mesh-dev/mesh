import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { BuildingPage } from './building.page'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    BuildingPage
  ],
  exports: [
    BuildingPage
  ]
})
export class BuildingModule { }
