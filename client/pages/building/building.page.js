import { Component, OnInit } from '@angular/core'
import { NgFor } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import { InfiniteScrollService } from '../../services/infinitescroll.service'
import * as template from './building.page.html'

@Component({
  selector: 'building-page',
  providers: [ InfiniteScrollService ],
  template: `
    ${template}
  `
})
export class BuildingPage {
  constructor (route: ActivatedRoute, infiniteScroll: InfiniteScrollService) {
    this.route = route
    this.infiniteScroll = infiniteScroll
  }
  ngOnInit () {
    this.projects = []

    const { parent } = this.route
    this.sub = parent.data
      .subscribe(({ currentUser }) => {
        this.currentUser = currentUser
      })

    this.infiniteScroll
      .loadFirstPage(`projects/paginate`)
      .subscribe(projects => this.projects = projects)
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  onScrolled () {
    this.infiniteScroll
      .fetchNextPage()
      .subscribe(projects => {
        if (!projects) {
          this.done = true
          return
        }

        this.projects = [...this.projects, ...projects]
        this.infiniteScroll.busy = false
      })
  }
}
