import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { LandingPage } from './landing.page'
import { SignupComponent } from '../../components/account/signup.component'
import { RegisterComponent } from '../../components/account/register.component'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    LandingPage,
    SignupComponent,
    RegisterComponent
  ],
  exports: [
    LandingPage
  ]
})
export class LandingModule { }
