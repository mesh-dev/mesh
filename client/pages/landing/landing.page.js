import { Component } from '@angular/core'
import { RouterLink } from '@angular/router'
import * as template from './landing.page.html'

@Component({
  selector: 'landing-page',
  template: `
    ${template}
  `
})
export class LandingPage {}
