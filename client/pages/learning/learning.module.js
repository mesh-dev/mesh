import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { LearningPage } from './learning.page'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    LearningPage
  ],
  exports: [
    LearningPage
  ]
})
export class LearningModule { }
