'use strict'

import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor, NgIf, NgStyle } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { InfiniteScrollService } from '../../services/infinitescroll.service'
import { EventService } from '../../services/event.service'
import * as template from './learning.page.html'

@Component({
  selector: 'learning-page',
  providers: [ InfiniteScrollService ],
  template: `
    ${template}
  `
})
export class LearningPage {
  constructor (
    route: ActivatedRoute,
    infiniteScroll: InfiniteScrollService
  ) {
    this.route = route
    this.infiniteScroll = infiniteScroll
  }
  ngOnInit () {
    this.questions = []

    const { parent } = this.route
    this.sub = parent.data
      .subscribe(({ currentUser }) => {
        this.currentUser = currentUser
      })

    this.infiniteScroll
      .loadFirstPage(`questions/paginate`)
      .subscribe(questions => this.questions = questions)
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  onScrolled () {
    this.infiniteScroll
      .fetchNextPage()
      .subscribe(questions => {
        if (!questions) {
          this.done = true
          return
        }

        this.questions = [...this.questions, ...questions]
        this.infiniteScroll.busy = false
      })
  }
}
