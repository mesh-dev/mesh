'use strict'

import { Component } from '@angular/core'
import { RouterLink } from '@angular/router'
import * as template from './login.page.html'

@Component({
  selector: 'loginPage',
  template: `
    ${template}
  `
})
export class LoginPage {}
