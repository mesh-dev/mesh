'use strict'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute, RouterLink } from '@angular/router'
import { HTTP_PROVIDERS } from '@angular/http'
import { NgIf, NgStyle } from '@angular/common'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import * as template from './profile.page.html'

@Component({
  selector: 'profile-page',
  template: `
    ${template}
  `
})
export class ProfilePage {
  constructor (
    route: ActivatedRoute,
    api: ApiService,
    socket: SocketService,
    event: EventService
  ) {
    this.route = route
    this.api = api
    this.event = event
    this.socket = socket
  }
  ngOnInit () {
    this.sub = this.route.params
      .subscribe(() => {
        this.user = this.route.snapshot.data.user
        this.currentUser = this.route.parent.snapshot.data.currentUser

        this.api
          .fetch(`requests/pending/${this.user.id}`)
          .subscribe(request => {
            this.request = request || {}
          })

        this.socket.sync(`user:${this.user.id}:save`, (user) => {
          if (user.id != this.currentUser.id) {
            this.user = { ...this.user, ...user }
          }
        })
      })

    this.socket.sync('user:requested', (request) => {
      this.request = request
    })
    this.socket.sync('request:approved', (request) => {
      this.request = request
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
  }
  onChangeAvatar (path) {
    this.event.changeAvatar(path)
    this.user.avatar_url = path
  }
  shouldHide (toCompare) {
    return typeof toCompare === 'undefined'
  }
}
