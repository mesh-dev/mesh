import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { ProfilePage } from './profile.page'
import { RequestsComponent } from '../../components/profile/requests.component'
import { ProfileDetailsComponent } from '../../components/profile/details.component'
import { UserActivityComponent } from '../../components/profile/activity.component'

@NgModule({
  imports: [ SharedModule ],
  declarations: [
    ProfilePage,
    RequestsComponent,
    ProfileDetailsComponent,
    UserActivityComponent
  ],
  exports: [
    ProfilePage
  ]
})
export class ProfileModule { }
