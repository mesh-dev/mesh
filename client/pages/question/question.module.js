import { NgModule } from '@angular/core'
import { SharedModule } from '../../components/common/shared.module'
import { NavModule } from '../../components/common/nav.module'
import { MessengerModule } from '../../components/messages/messenger.module'
import { MentionService } from '../../services/mention.service'
import { AnswersComponent } from '../../components/answers/answers.component'
import { AnswerComponent } from '../../components/answers/answer.component'
import { CommentsComponent } from '../../components/answers/comments.component'
import { QuestionPage } from './question.page'

@NgModule({
  imports: [
    SharedModule,
    MessengerModule,
    NavModule
  ],
  providers: [ MentionService ],
  declarations: [
    QuestionPage,
    AnswersComponent,
    AnswerComponent,
    CommentsComponent
  ],
  exports: [
    QuestionPage
  ]
})
export class QuestionModule { }
