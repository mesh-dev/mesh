'use strict'

import { Component, OnInit, OnDestroy } from '@angular/core'
import { ActivatedRoute } from '@angular/router'
import { NgFor, NgIf, NgStyle } from '@angular/common'
import { AuthService } from '../../services/auth.service'
import { ApiService } from '../../services/api.service'
import { SocketService } from '../../services/socket.service'
import { EventService } from '../../services/event.service'
import * as template from './question.page.html'

@Component({
  selector: 'question-page',
  template: `
    ${template}
  `
})
export class QuestionPage {
  constructor (
    route: ActivatedRoute,
    socket: SocketService,
    event: EventService,
    api: ApiService
  ) {
    this.route = route
    this.event = event
    this.socket = socket
    this.api = api
  }
  ngOnInit () {
    const particpants = []
    this.openThreads = []
    this.links = [
      { route: '/D', name: 'Back to Dashboard', icon: '/assets/icons/dashboard.svg' }
    ]

    this.sub = this.route.data.subscribe(data => {
      this.currentUser = data.currentUser
      this.team = data.team
      this.question = data.question

      particpants.push(this.question.owner)
      for (let answer of this.question.answers) {
        if (!_.some(particpants, { id: answer.owner.id })) {
          particpants.push(answer.owner)
        }
      }
      this.question.particpants = particpants
    })

    this.socket.join(`question:${this.question.id}`)
    this.socket.sync('question:save', (question) => {
      if (this.currentUser.id === question.owner_id) return

      this.question.title = question.title
      this.question.description = question.description
    })
    this.socket.sync('answer:save', (answer) => {
      if (_.some(this.question.answers, { id: answer.id })) {
        const index = _.findIndex(this.question.answers, (a) => a.id == answer.id)
        this.question.answers[index] = answer
        return
      }

      this.question.answers.push(answer)
    })

    this.event.threadClosed$.subscribe(user => {
      _.remove(this.openThreads, (o) => o.id === user.id)
    })
    this.event.threadOpened$.subscribe(user => {
      if (_.some(this.openThreads, { id: user.id })) {
        return _.remove(this.openThreads, (o) => o.id === user.id)
      } else if (this.openThreads.length >= 4) {
        this.openThreads.shift()
        this.openThreads.push(user)
      } else {
        this.openThreads.push(user)
      }
    })
  }
  ngOnDestroy () {
    this.sub.unsubscribe()
    this.socket.unsync('question:save')
    this.socket.unsync('answer:save')
    this.socket.leave(`question:${this.question.id}`)
  }
  handleEdit () {
    this.editing = false
    this.api.update(`questions/${this.question.id}`, this.question)
      .then(question => {
        this.question.title = question.title
        this.question.description = question.description
      })
      .catch(err => console.log(err))
  }
}
