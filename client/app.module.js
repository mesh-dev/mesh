import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { FormsModule } from '@angular/forms'
import { HttpModule, BrowserXhr } from '@angular/http'
import { NgProgressModule, NgProgressCustomBrowserXhr } from 'ng2-progressbar'
import { AppRoutingModule } from './app.routes'
import { PipeModule } from './pipes/pipe.module'
import { ApiService } from './services/api.service'
import { AuthService } from './services/auth.service'
import { EventService } from './services/event.service'
import { SocketService } from './services/socket.service'
import { AppComponent } from './app.component'

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    NgProgressModule,
    AppRoutingModule
  ],
  providers: [
    EventService,
    AuthService,
    ApiService,
    SocketService,
    { provide: BrowserXhr, useClass: NgProgressCustomBrowserXhr }
  ],
  declarations: [
    AppComponent
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
